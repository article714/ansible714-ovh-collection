# Ansible Collection for managing OVH-cloud products & resources

[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)

This collection contains a set of modules and roles to manage hosted services at [OVH-Cloud](https://www.ovhcloud.com/).

The [official documentation](https://ansible714.article714.org/collections/ansible714.ovh/index.html) is published together
with [Ansible714 docs](https://ansible714.article714.org).

## Dependencies

This collection makes use of :

- [ovh-python](https://github.com/ovh/python-ovh) module to connect to [OVH Api](https://api.ovh.com/) which it relies on
  to manage products & resources;
- [Openstack Ansible collections](https://github.com/openstack/ansible-collections-openstack), as
  [OVH-Cloud](https://www.ovhcloud.com/) cloud infrastructure relies on [OpenStack](https://www.openstack.org/).

### Ansible compatibility

**This collection is only compatible with ansible versions > 2.8**.

To identify the versions against which this collection is tested/validated, you have to look at `.gitlab-ci.yml` file where
versions matrix are defined.

### Python compatibility

This tooling is compatible with python 3.9+.
_Efforts are being made to test it against earlier version of Python, but there is no warranty that everything works_

Python 2 has been deprecated [1st January 2020](https://www.python.org/doc/sunset-python-2/), we should all consider
switching to Python 3.

_As of version 0.0.8 and later of the collection, we dropped python any Python 2 support as some required packages are_
_not available for older python_

## Contribute

\_NOTE:\_ We maintain a list of authors, see `AUTHORS.md` file.

Project source code is hosted at [Gitlab.com](https://gitlab.com):
[official repository](https://gitlab.com/article714/ansible714-ovh-collection).

For more details on how to configure a development environment to work on this collection,
please have a look at [Development docs](development.md)

### pre-commit

### Issues lifecycle

- `ILC::Todo`, issue with that tag is ready to be worked on, a target milestone should have been set.
- `ILC::Doing`, work in progress, a dedicated branch (and probably a merge request) should exist.
- `ILC::Done`, issue with that tag is "closeable", work is merged on `main` branch, and issue will be closed when
  integrated to an official release (milestone).

### Branching policy & releases

We tend to use a branch per issue and a tag per official release.

Releases are available for download from [package registry](https://gitlab.com/article714/ansible714-ovh-collection/-/packages)
, and manually uploaded to [Galaxy](https://galaxy.ansible.com/).
