# -*- coding: utf-8 -*-


# Copyright: ©Tekfor - 2022

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

# Common Options for all OvhClientModule

from __future__ import absolute_import, division, print_function

__metaclass__ = type


class ModuleDocFragment(object):
    DOCUMENTATION = r"""
options:
    ovh_api_endpoint:
        required: true
        description:
          - Which OVH Api endpoint to use
        type: str

    ovh_api_application_key:
        required: true
        description:
          - Application Key obtained from OVH
        type: str

    ovh_api_application_secret:
        required: true
        description:
          - Application Secret obtained from OVH
        type: str

    ovh_api_consumer_key:
        required: true
        description:
          - |
            Consumer key, created via OVH-Api and configured with the rights
            to use all the needed endoints (the one that you will be using)
        type: str

"""
