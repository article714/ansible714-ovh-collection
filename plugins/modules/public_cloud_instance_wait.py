#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2023

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

from __future__ import absolute_import, division, print_function


__metaclass__ = type


ANSIBLE_METADATA = {"metadata_version": "1.1", "status": ["preview"], "supported_by": "community"}


DOCUMENTATION = r"""
---
module: public_cloud_instance_wait
short_description: Wait for an Public Cloud Instance to become active
description:
  - Wait until the public cloud instance installation is done (status == active)
  - Can be used to wait before running next task in your playbook
author:
  - Christophe Guychard (@xtof-osd)
  - Mathieu Piriou (@PiMath22)
requirements:
  - python >= 3.6
  - ovh >= 1.0.0
options:
  project_id:
    required: true
    type: str
    description:
      - The ID of public cloud project
  instance_id:
    required: true
    type: str
    description: The instance uuid
  target_status:
    required: false
    type: str
    description: target status to wait for
    default: ACTIVE
  max_retry:
    required: false
    type: int
    description: Number of retries
    default: 30
  sleep:
    required: false
    type: int
    description: Time to sleep between retries
    default: 20

extends_documentation_fragment:
  - ansible714.ovh.ovh_client_module

"""

EXAMPLES = """
ansible714.ovh.public_cloud_instance_wait:
  instance_id: "{{ instance_id }}"
  project_id: "{{ project_id }}"
  ovh_api_endpoint: "{{ ovh_api_endpoint }}"
  ovh_api_application_key: "{{ ovh_api_application_key }}"
  ovh_api_application_secret: "{{ ovh_api_application_secret }}"
  ovh_api_consumer_key: "{{ ovh_api_consumer_key }}"
delegate_to: localhost
"""

RETURN = """ # """


from ansible_collections.ansible714.ovh.plugins.module_utils.ovh_client_module import (  # noqa: E402,E501
    OvhClientModule,
)
import time  # noqa: E402,E501


def run_module():
    module = OvhClientModule(
        argument_spec=dict(
            project_id=dict(type="str", required=True),
            instance_id=dict(type="str", required=True),
            max_retry=dict(required=False, default=30, type="int"),
            sleep=dict(required=False, default=20, type="int"),
            target_status=dict(required=False, default="ACTIVE", type="str"),
        ),
        supports_check_mode=True,
        required_by={},
    )

    # Get Parameters
    instance_id = module.params["instance_id"]
    project_id = module.params["project_id"]
    max_retry = module.params["max_retry"]
    sleep = module.params["sleep"]
    target_status = module.params["target_status"]

    is_target_status = False

    # Import exceptions (inside module to avoid earlier errors if ovh module is not present)
    from ovh.exceptions import (
        APIError,
    )

    if module.check_mode:
        module.exit_json(msg="done - (dry run mode)", changed=False)

    for i in range(int(max_retry)):
        time.sleep(float(sleep))

        result = None
        try:
            result = module.client.get(
                "/cloud/project/{0}/instance/{1}".format(project_id, instance_id)
            )

            if result and "status" in result:
                is_target_status = result["status"] == target_status

        except APIError as error:
            module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

        if is_target_status:
            module.exit_json(changed=True, **result)

    module.fail_json(msg="Max wait time reached, about %i x %i seconds" % (i + 1, int(sleep)))


def main():
    run_module()


if __name__ == "__main__":
    main()
