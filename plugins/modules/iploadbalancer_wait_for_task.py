#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2022

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

from __future__ import absolute_import, division, print_function


__metaclass__ = type


ANSIBLE_METADATA = {"metadata_version": "1.1", "status": ["preview"], "supported_by": "community"}


DOCUMENTATION = r"""
---
module: iploadbalancer_wait_for_task
short_description: Wait for a IP-Load-Balancer Task to finish
description:
  - Wait until the provided IP-Load-Balancer task has ended
author:
  - Christophe Guychard (@xtof-osd)
requirements:
  - "ovh >=  1.0.0"
options:
  name:
    required: true
    description:
      - Name of the ip-loadbalancer
    type: str
  task_id:
    required: true
    description:
      - Id of the task
    type: int
  timewait:
    required: false
    description:
      - Time (seconds)to wait between two checks of task status
    default: 10
    type: int
  retries:
    required: false
    description:
      - Number of retries
    default: 12
    type: int

extends_documentation_fragment:
  - ansible714.ovh.ovh_client_module

"""

EXAMPLES = """
- name: Wait for task completion
  ansible714.ovh.iploadbalancer_wait_for_task:
    name: my-iploadbalancer
    task_id: 18
    ovh_api_endpoint: '{{ ovh_api_endpoint }}'
    ovh_api_application_key: '{{ ovh_api_application_key }}'
    ovh_api_application_secret: '{{ ovh_api_application_secret }}'
    ovh_api_consumer_key: '{{ ovh_api_consumer_key }}'

"""

RETURN = """
# JSON description of the IP-Load-Balancer task, according to OVH-Api schema.
# (https://eu.api.ovh.com/console/#/ipLoadbalancing/{serviceName}/task/{taskId}#GET)
"""


from ansible_collections.ansible714.ovh.plugins.module_utils.ovh_client_module import (  # noqa: E402,E501
    OvhClientModule,
)
from time import sleep  # noqa: E402


def run_module():
    module = OvhClientModule(
        argument_spec=dict(
            name=dict(required=True),
            task_id=dict(type="int", required=True),
            retries=dict(required=False, default=12, type="int"),
            timewait=dict(required=False, default=10, type="int"),
        ),
        required_by={},
    )

    if module.client is None:
        module.fail_json(msg="Unable to init OVH api Client")

    # Get parameters
    name = module.params.get(
        "name",
    )
    task_id = module.params.get("task_id")
    max_retries = int(module.params.get("retries"))
    timewait = float(module.params.get("timewait"))

    retries = 1

    from ovh.exceptions import (
        APIError,
    )

    task_status = "unknown"
    task_progress = "0"
    while retries < max_retries:
        # Check that status of the task
        try:
            task_info = module.client.get("/ipLoadbalancing/{0}/task/{1}".format(name, task_id))
        except APIError as error:
            module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))
        task_status = task_info.get("status")
        task_progress = task_info.get("progress")
        if task_status in ("init", "todo", "doing"):
            sleep(timewait)
        elif task_status in ("cancelled", "done", "error"):
            module.exit_json(
                changed=True, msg="Task ended with status {0}".format(task_status), **task_info
            )

        retries += 1

    module.fail_json(
        msg="Wait for IP-Load-Balancer Task timed out, status is {0}, and progress: {1}".format(
            task_status, task_progress
        )
    )


def main():
    run_module()


if __name__ == "__main__":
    main()
