#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2022

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

from __future__ import absolute_import, division, print_function


__metaclass__ = type


ANSIBLE_METADATA = {"metadata_version": "1.1", "status": ["preview"], "supported_by": "community"}

DOCUMENTATION = r"""
---
module: iploadbalancer_delete_vrack
short_description: Remove an OVH IP Load-Balancer from a Vrack
description:
  - When possible (ig Ip Load-Balancer is elegible), remove an OVH IP Load-Balancer from a Vrack
author:
  - Christophe Guychard (@xtof-osd)
requirements:
  - "ovh >=  1.0.0"
options:
  name:
    required: true
    description:
      - Name of the IpLoadBalancer
    type: str
  vrack:
    required: true
    description:
      - Name of the Vrack to attach IpLoadBalancer to
    type: str
  skip_validation:
    required: false
    description:
      - Do we need to skip verification about IP Load Balancer existance
    type: bool
    default: false

extends_documentation_fragment:
  - ansible714.ovh.ovh_client_module

"""

EXAMPLES = """
- name: Sets vrack for Load-Balancer
  ansible714.ovh.iploadbalancer_delete_vrack:
    name: my-load-balancer
    iplb_vrack: my-vrack
    ovh_api_endpoint: '{{ ovh_api_endpoint }}'
    ovh_api_application_key: '{{ ovh_api_application_key }}'
    ovh_api_application_secret: '{{ ovh_api_application_secret }}'
    ovh_api_consumer_key: '{{ ovh_api_consumer_key }}'


"""

RETURN = """
# JSON description of the Vrack task, according to OVH-Api schema.
# (https://eu.api.ovh.com/console/#/vrack/{serviceName}/task/{taskId}#GET)
"""


from ansible_collections.ansible714.ovh.plugins.module_utils.ovh_client_module import (  # noqa: E402,E501
    OvhClientModule,
)
from ansible_collections.ansible714.ovh.plugins.module_utils.iploadbalancer import (  # noqa: E402,E501
    get_iplb_info,
)


def run_module():
    module = OvhClientModule(
        argument_spec=dict(
            name=dict(type="str", required=True),
            vrack=dict(type="str", required=True),
            skip_validation=dict(type="bool", default=False, required=False),
        ),
        required_by={},
    )

    if module.client is None:
        module.fail_json(msg="Unable to init OVH api Client")

    # Get parameters
    name = module.params.get("name", None)
    vrack = module.params.get("vrack", None)
    skip_validation = module.params.get("skip_validation", False)

    from ovh.exceptions import (
        APIError,
    )

    # Check that the ip-load-balancer exists
    if not skip_validation:
        loadBalancerData = get_iplb_info(module)
        if not loadBalancerData.get("vrackEligibility", False):
            module.fail_json(msg="Load-Balancer is not eligible to Vrack")

    # Check if Load-Balancer is already in Vrack
    lb_in_vrack = False
    try:
        call_result = module.client.get("/vrack/{0}/ipLoadbalancing/{1}".format(vrack, name))
        lb_in_vrack = (
            call_result.get("ipLoadbalancing") == name,
            call_result.get("vrack") == vrack,
        )
        if not lb_in_vrack:
            module.fail_json(msg="Failed to check that Load-Balancer is in Vrack")
    except APIError as error:
        if error.response is not None and error.response.status_code == 404:
            module.exit_json(changed=False, msg="Load-Balancer is not in Vrack")
        else:
            module.fail_json(msg="Failed to execute OVH-Api call : {0} ".format(error))

    task_info = {}
    if lb_in_vrack:
        # removes ipLB from Vrack
        try:
            task_info = module.client.delete("/vrack/{0}/ipLoadbalancing/{1}".format(vrack, name))
        except APIError as error:
            module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

    module.exit_json(changed=True, msg="Load-Balancer is being detacehd from Vrack", **task_info)


def main():
    run_module()


if __name__ == "__main__":
    main()
