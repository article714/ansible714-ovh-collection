#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2022

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

from __future__ import absolute_import, division, print_function


__metaclass__ = type


ANSIBLE_METADATA = {"metadata_version": "1.1", "status": ["preview"], "supported_by": "community"}


DOCUMENTATION = r"""
---
module: public_cloud_flavor_info
short_description: Flavor data from name
description: Provides information about instance flavor in a given region
author:
  - Christophe Guychard (@xtof-osd)

requirements:
  - "python >= 3.6"
  - "ovh >=  1.0.0"
options:
  name:
    required: true
    description: "The human readable name of the flavor: d2-2, c2-7, b2-7,..."
    type: str
  region:
    required: true
    description: |
      Region from which you will use given flavor.

      Region is mandatory as flavor id is different for each region.
    type: str
  project_id:
    required: true
    description:
      - The ID of project
    type: str

extends_documentation_fragment:
  - ansible714.ovh.ovh_client_module

"""

EXAMPLES = """
- name: Get info about some flavor
  ansible714.ovh.public_cloud_flavor_info:
    project_id: "{{ project_id }}"
    name: "d2-2"
    ovh_api_endpoint: '{{ ovh_api_endpoint }}'
    ovh_api_application_key: '{{ ovh_api_application_key }}'
    ovh_api_application_secret: '{{ ovh_api_application_secret }}'
    ovh_api_consumer_key: '{{ ovh_api_consumer_key }}'
  delegate_to: localhost
"""

RETURN = """
FlavorDescription:
  type: dict
  returned: success
  description: detailed information about instance flavor
  contains:
    available:
      type: bool
      description: Available in stock
    capabilities:
      type: dict
      description: Capabilities of the flavor
    disk:
      type: int
      description: Number of disks
    id:
      type: str
      description: Flavor id
    inboundBandwidth:
      type: int
      description: Max capacity of inbound traffic in Mbit/s
    name:
      type: str
      description: Flavor name
    osType:
      type: str
      description: OS to install on
    outboundBandwidth:
      type: int
      description: Max capacity of outbound traffic in Mbit/s
    planCodes:
      type: dict
      description: Plan codes to order instances
    quota:
      type: int
      description: Number instance you can spawn with your actual quota
    ram:
      type: int
      description: Ram quantity (Gio)
    region:
      type: str
      description: Flavor region
    type:
      type: str
      description: Flavor type
    vcpus:
      type: int
      description: Number of VCPUs

"""


from ansible_collections.ansible714.ovh.plugins.module_utils.ovh_client_module import (  # noqa: E402,E501
    OvhClientModule,
)


def run_module():
    module = OvhClientModule(
        argument_spec=dict(
            dict(
                project_id=dict(type="str", required=True),
                name=dict(type="str", required=True),
                region=dict(type="str", required=True),
            )
        ),
        supports_check_mode=True,
        required_by={},
    )

    # Get Parameters
    project_id = module.params["project_id"]
    name = module.params["name"]
    region = module.params["region"]

    # Import exceptions (inside module to avoid earlier errors if ovh module is not present)
    from ovh.exceptions import (
        APIError,
    )

    try:
        flavors = []
        if region:
            flavors = module.client.get(
                "/cloud/project/{0}/flavor".format(project_id), region=region
            )
        else:
            flavors = module.client.get("/cloud/project/{0}/flavor".format(project_id))
        for flavor_data in flavors:
            if flavor_data["name"] == name:
                module.exit_json(changed=False, **flavor_data)

        module.fail_json(msg="Flavor {0} not found in {1}".format(name, region), changed=False)

    except APIError as error:
        module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))


def main():
    run_module()


if __name__ == "__main__":
    main()
