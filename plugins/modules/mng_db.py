#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2023

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

from __future__ import absolute_import, division, print_function


__metaclass__ = type


ANSIBLE_METADATA = {"metadata_version": "1.1", "status": ["preview"], "supported_by": "community"}


DOCUMENTATION = r"""
---
module: mng_db
short_description: Create, update or delete OVH database instance
description:
  - |
    This module allows you to delete, update or create a database instance in an OVH public
    cloud project.

    To simply delete an OVH database instance, just use the "project_id", set
    state to present and "db_instance_name" variable.
    The module will retrieve the instance ID (cluster_id) and call the API that allows it to
    be deleted.

    To update an instance, just use the variables: "project_id" and "db_instance_name".
    As well as the optional variables: "flavor", "backup_time", "maintenance_time", "plan",
    "version" and "new_db_instance_name".
    These variables are not mandatory. However, you must have the "project_id",
    the "db_instance_name" and the variables with the new parameters that you want
    to modify. The "db_instance_name" matches to the current name of the instance,
    while "new_db_instance_name" corresponds to the new name of the instance.

    To create an instance, just use all the variables except:
    new_db_instance_name.
author:
  - Mathieu Piriou (@PiMath22)
requirements:
  - "ovh >= 1.0.0"
options:
  project_id:
    required: true
    description: The ID of the project
    type: str
  db_instance_name:
    required: true
    description: The name of the database to create
    type: str
  backup_time:
    required: false
    description: The backup time of the database
    type: str
  maintenance_time:
    required: false
    description: The maintenance time of the database
    type: str
  plan:
    required: false
    description: The plan of the database
    type: str
  subnet_id:
    required: false
    description: The ID of the subnet to use for the database
    type: str
  network_id:
    required: false
    description: The ID of the private network
    type: str
  version:
    required: false
    description: The version of database to use
    type: str
  new_db_instance_name:
    required: false
    description: The new database name
    type: str
  ip_restrictions:
    required: false
    description: List of ip authorized
    type: list
    elements: dict
  state:
    required: false
    description: Indicate the desired state of the database
    choices:
      - present
      - absent
    default: present
    type: str
  node_pattern_flavor:
    required: false
    description: The database flavor type
    type: str
  node_pattern_region:
    required: false
    description: The region of the database
    type: str
  db_engine:
    required: true
    description:
        - |
          The name of the database engine :
          - cassandra
          - m3db
          - mongodb
          - opensearch
          - postgresql
          - grafana
          - kafka
          - mysql
          - redis
    type: str

extends_documentation_fragment:
  - ansible714.ovh.ovh_client_module

"""

EXAMPLES = """
- name: Create OVH database instance
  ansible714.ovh.mng_db:
    project_id: "{{ project_id }}"
    db_instance_name: "{{ db_instance_name }}"
    node_pattern_flavor: "{{ node_pattern_flavor }}"
    node_pattern_region: "{{ node_pattern_region }}"
    plan: "{{ plan }}"
    private_id: "{{ private_id }}"
    subnet_id: "{{ subnet_id }}"
    version: "{{ version }}"
    db_engine: "{{ db_engine }}"
    ip_restrictions: "{{ ip_restrictions }}"
    ovh_api_endpoint: '{{ ovh_api_endpoint }}'
    ovh_api_application_key: '{{ ovh_api_application_key }}'
    ovh_api_application_secret: '{{ ovh_api_application_secret }}'
    ovh_api_consumer_key: '{{ ovh_api_consumer_key }}'
  delegate: localhost
  become: no
"""

RETURN = """
# All information related to the created/updated/deleted database instance
"""

from ansible_collections.ansible714.ovh.plugins.module_utils.ovh_client_module import (  # noqa: E402,E501
    OvhClientModule,
)

from ansible_collections.ansible714.ovh.plugins.module_utils.mng_database import (  # noqa: E402,E501
    get_cluster_id_from_module,
    get_node_number_from_module,
    get_ip_restrictions_from_module,
    get_db_engine_from_module,
)


def delete_db(module, cluster_id):
    from ovh.exceptions import (
        APIError,
    )

    try:
        module.client.delete(
            "/cloud/project/{0}/database/{1}/{2}".format(
                module.params["project_id"], get_db_engine_from_module(module), cluster_id
            )
        )
    except APIError as e:
        error_msg = "Failed to delete database instance: {0}".format(e)
        module.fail_json(msg=error_msg, rc=1)

    module.exit_json(changed=True, result="Database instance deleted successfully")


def update_db(module, cluster_id):
    from ovh.exceptions import (
        APIError,
    )

    try:
        module.client.put(
            "/cloud/project/{0}/database/{1}/{2}".format(
                module.params["project_id"], get_db_engine_from_module(module), cluster_id
            ),
            **get_data_for_update_from_module(module),
        )
    except APIError as e:
        module.fail_json(msg="Failed update Databases instance: {0}".format(e))

    module.exit_json(changed=True, result="Database instance updated successfully")


def create_db(module):
    from ovh.exceptions import (
        APIError,
    )

    try:
        response = module.client.post(
            "/cloud/project/{0}/database/{1}".format(
                module.params["project_id"], get_db_engine_from_module(module)
            ),
            **get_data_for_create_from_module(module),
        )
    except APIError as api_error:
        module.fail_json(msg="Error creating database instance: {0}".format(api_error))

    if "id" not in response:
        module.fail_json(msg="Missing database instance ID in API response")

    module.exit_json(changed=True, database_id=response["id"])


def get_data_for_create_from_module(module):
    data = get_common_data_from_module(module)

    data["nodesPattern"] = {
        "flavor": module.params["node_pattern_flavor"],
        "number": get_node_number_from_module(module),
        "region": module.params["node_pattern_region"],
    }

    if module.params["network_id"]:
        data["networkId"] = module.params["network_id"]
        data["subnetId"] = module.params["subnet_id"]

    return data


def get_data_for_update_from_module(module):
    data = get_common_data_from_module(module)

    if module.params["node_pattern_flavor"]:
        data["flavor"] = module.params["node_pattern_flavor"]

    data["nodeNumber"] = get_node_number_from_module(module)

    return data


def get_common_data_from_module(module):
    data = {
        "plan": module.params["plan"],
        "version": module.params["version"],
    }

    if module.params["db_instance_name"]:
        data["description"] = module.params["db_instance_name"]

    if module.params["backup_time"]:
        data["backup_time"] = module.params["backup_time"]

    if module.params["maintenance_time"]:
        data["maintenance_time"] = module.params["maintenance_time"]

    ip_restrictions = get_ip_restrictions_from_module(module)
    if ip_restrictions:
        data["ipRestrictions"] = ip_restrictions

    return data


def run_module():
    module = OvhClientModule(
        argument_spec=dict(
            project_id=dict(type="str", required=True),
            db_instance_name=dict(type="str", required=True),
            backup_time=dict(type="str", required=False),
            maintenance_time=dict(type="str", required=False),
            plan=dict(type="str", required=False),
            network_id=dict(type="str", required=False),
            subnet_id=dict(type="str", required=False),
            version=dict(type="str", required=False),
            new_db_instance_name=dict(type="str", required=False),
            ip_restrictions=dict(type="list", elements="dict", required=False),
            state=dict(type="str", choices=["present", "absent"], default="present"),
            node_pattern_flavor=dict(type="str", required=False),
            node_pattern_region=dict(type="str", required=False),
            db_engine=dict(type="str", required=True),
        ),
        required_by={},
    )

    if module.client is None:
        module.fail_json(msg="Unable to init OVH api Client")

    cluster_id = get_cluster_id_from_module(module)

    if cluster_id:
        state = module.params["state"]
        if state == "present":
            try:
                update_db(module, cluster_id)
            except Exception as e:
                module.fail_json(msg="Failed to update database instance: {0}".format(str(e)))
        elif state == "absent":
            try:
                delete_db(module, cluster_id)
            except Exception as e:
                module.fail_json(msg="Failed to delete database instance: {0}".format(str(e)))
    else:
        try:
            create_db(module)
        except Exception as e:
            node_pattern_flavor = module.params["node_pattern_flavor"]
            node_pattern_region = module.params["node_pattern_region"]
            module.fail_json(
                msg="Failed to create database instance with {0} | {1} | {2} : {3}".format(
                    node_pattern_flavor,
                    get_node_number_from_module(module),
                    node_pattern_region,
                    str(e),
                )
            )


def main():
    run_module()


if __name__ == "__main__":
    main()
