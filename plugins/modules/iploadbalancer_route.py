#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2022

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

from __future__ import absolute_import, division, print_function


__metaclass__ = type


ANSIBLE_METADATA = {"metadata_version": "1.1", "status": ["preview"], "supported_by": "community"}


DOCUMENTATION = r"""
---
module: iploadbalancer_route
short_description: Manage Routes for OVH IpLoadBalancer
description:
  - Manage Routes for OVH IpLoadBalancer
author:
  - Christophe Guychard (@xtof-osd)
requirements:
  - "ovh >=  1.0.0"
options:
  lb_name:
    required: true
    description:
      - Name of the IpLoadBalancer
    type: str
  frontend_id:
    required: true
    description:
      - Id Of the Frontend for this Route
    type: int
  frontend_type:
    required: true
    description:
      - Kind of Frontend
    choices:
      - 'http'
      - 'tcp'
    type: str
  name:
    required: true
    description:
      - |
        Human readable name for your server.
        Should be unique for this module to work properly
    type: str
  action_status:
    required: false
    description:
      - Semantic depends on route action (see OVH documentation)
    type: int
  action_target:
    required: false
    description:
      - Semantic depends on route action (see OVH documentation)
    type: str
  action_type:
    required: false
    description:
      - Semantic depends on route action (see OVH documentation)
    type: str
  weight:
    required: false
    description:
      - |
        Set weight on that server [1..256].
        0 if not used in load balancing.
        1 if left null.
        Servers with higher weight get more requests.
    type: int
    default: 0
  rules:
    required: false
    description: List of rules to apply
    type: list
    elements: dict
  state:
    required: false
    description: Indicate the desired state of the Server
    choices:
      - present
      - absent
    default: present
    type: str

extends_documentation_fragment:
  - ansible714.ovh.ovh_client_module

"""

EXAMPLES = """
- name: Manage Routes
  ansible714.ovh.iploadbalancer_route:
    lb_name: 'my-lb'
    action_target: 18
    action_type: 'farm'
    name: 'vhost: www.example.com'
    frontend_id: 19
    frontend_type: http
    weight: 120
    ovh_api_endpoint: '{{ ovh_api_endpoint }}'
    ovh_api_application_key: '{{ ovh_api_application_key }}'
    ovh_api_application_secret: '{{ ovh_api_application_secret }}'
    ovh_api_consumer_key: '{{ ovh_api_consumer_key }}'
  delegate_to: localhost
  become: no

"""

RETURN = """
# TODO
"""


from ansible_collections.ansible714.ovh.plugins.module_utils.ovh_client_module import (  # noqa: E402,E501
    OvhClientModule,
)


def manage_rules(
    module,
    rules,
    lb_name,
    frontend_type,
    route_id,
):
    """Manage Rules for current Route"""

    # Import exceptions (inside module to avoid earlier errors if ovh module is not present)
    from ovh.exceptions import (
        APIError,
    )

    for aRule in rules:
        # --- "validate" parameters
        if "state" not in aRule:
            aRule["state"] = "present"
        if "name" not in aRule:
            module.fail_json(msg="Missing name on Rule : {0}".format(aRule))
        if "field" not in aRule:
            module.fail_json(msg="Missing field on Rule : {0}".format(aRule))
        if "match" not in aRule:
            module.fail_json(msg="Missing match on Rule : {0}".format(aRule))
        request_args = {
            "displayName": aRule["name"],
            "field": aRule["field"],
            "match": aRule["match"],
        }
        if "negate" in aRule:
            try:
                request_args["negate"] = bool(aRule["negate"])
            except KeyError:  # noqa: E722
                pass
        if "pattern" in aRule:
            request_args["pattern"] = aRule["pattern"]
        if "subField" in aRule:
            request_args["subField"] = aRule["subField"]

        # ---
        # look for the Rule
        existing_rule = None
        rules_list = []
        try:
            rules_list = module.client.get(
                "/ipLoadbalancing/{0}/{1}/route/{2}/rule".format(lb_name, frontend_type, route_id)
            )
        except APIError as error:
            module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

        for aruleid in rules_list:
            try:
                result = module.client.get(
                    "/ipLoadbalancing/{0}/{1}/route/{2}/rule/{3}".format(
                        lb_name, frontend_type, route_id, aruleid
                    ),
                )
                if result["displayName"] == aRule["name"]:
                    if existing_rule is None:
                        existing_rule = result
                    else:
                        module.fail_json(
                            msg="We found several rules with same name : {0}".format(aRule["name"])
                        )
            except APIError as error:
                module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

        # ------------------
        # Depending on state value, process data
        if aRule["state"] == "present":
            if existing_rule is None:
                # create Rule
                try:
                    existing_rule = module.client.post(
                        "/ipLoadbalancing/{0}/{1}/route/{2}/rule".format(
                            lb_name, frontend_type, route_id
                        ),
                        **request_args,
                    )
                    module.exit_json(changed=True, msg="Route has been created", **existing_rule)
                except APIError as error:
                    module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))
            else:
                try:
                    # should update Route
                    module.client.put(
                        "/ipLoadbalancing/{0}/{1}/route/{2}/rule/{3}".format(
                            lb_name, frontend_type, route_id, existing_rule["ruleId"]
                        ),
                        **request_args,
                    )
                except APIError as error:
                    module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

        else:
            if existing_rule is not None:
                # delete Route
                try:
                    module.client.delete(
                        "/ipLoadbalancing/{0}/{1}/route/{2}/rule/{3}".format(
                            lb_name, frontend_type, route_id, existing_rule["ruleId"]
                        )
                    )
                except APIError as error:
                    module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))


def run_module():
    module = OvhClientModule(
        argument_spec=dict(
            lb_name=dict(type="str", required=True),
            action_status=dict(type="int", required=False),
            action_target=dict(type="str", required=False),
            action_type=dict(type="str", required=False),
            name=dict(type="str", required=True),
            frontend_id=dict(type="int", required=True),
            frontend_type=dict(type="str", required=True, choices=("http", "tcp")),
            rules=dict(
                type="list",
                elements="dict",
                required=False,
            ),
            state=dict(type="str", choices=["present", "absent"], default="present"),
            weight=dict(type="int", required=False, default=0),
        ),
        required_by={},
    )

    # Get parameters
    lb_name = module.params.get("lb_name")
    action = None
    action_status = module.params.get("action_status")
    action_target = module.params.get("action_target")
    action_type = module.params.get("action_type")
    name = module.params.get("name")
    frontend_id = module.params.get("frontend_id")
    frontend_type = module.params.get("frontend_type")
    state = module.params.get("state")
    rules = module.params.get("rules")
    weight = module.params.get("weight")
    if action_status or action_target or action_type:
        action = {}
    if action_status:
        action["status"] = action_status
    if action_target:
        action["target"] = action_target
    if action_type:
        action["type"] = action_type

    request_args = {
        "displayName": name,
        "frontendId": frontend_id,
        "weight": weight,
    }
    if action is not None:
        request_args["action"] = action

    # Import exceptions (inside module to avoid earlier errors if ovh module is not present)
    from ovh.exceptions import (
        APIError,
    )

    existing_route = None
    # ------------------
    # Look for an existing Route
    #  using route displayName to enforce uniqueness
    routes_list = []
    try:
        routes_list = module.client.get(
            "/ipLoadbalancing/{0}/{1}/route".format(lb_name, frontend_type),
            frontendId=frontend_id,
        )
    except APIError as error:
        module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

    for arouteid in routes_list:
        try:
            result = module.client.get(
                "/ipLoadbalancing/{0}/{1}/route/{2}".format(lb_name, frontend_type, arouteid),
            )
            if result["displayName"] == name:
                if existing_route is None:
                    existing_route = result
                else:
                    module.fail_json(
                        msg="We found several routes with same name : {0}".format(name)
                    )
        except APIError as error:
            module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

    # ------------------
    # Depending on state value, process data
    if state == "present":
        if existing_route is None:
            # create Route
            try:
                existing_route = module.client.post(
                    "/ipLoadbalancing/{0}/{1}/route".format(lb_name, frontend_type), **request_args
                )
                # ----
                # Manage Rules
                manage_rules(module, rules, lb_name, frontend_type, existing_route["routeId"])
                module.exit_json(changed=True, msg="Route has been created", **existing_route)
            except APIError as error:
                module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))
        else:
            try:
                # should update Route
                module.client.put(
                    "/ipLoadbalancing/{0}/{1}/route/{2}".format(
                        lb_name, frontend_type, existing_route["routeId"]
                    ),
                    **request_args,
                )
                # ----
                # Manage Rules
                manage_rules(module, rules, lb_name, frontend_type, existing_route["routeId"])
                module.exit_json(changed=True, msg="Route has been updated")
            except APIError as error:
                module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))
    else:
        if existing_route is None:
            module.exit_json(changed=False, msg="Route already deleted {0}".format(name))
        else:
            # delete Route
            try:
                module.client.delete(
                    "/ipLoadbalancing/{0}/{1}/route/{2}".format(
                        lb_name, frontend_type, existing_route["routeId"]
                    )
                )
                module.exit_json(changed=True, msg="Route has been deleted")
            except APIError as error:
                module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

    module.exit_json(changed=False, msg="Nothing has been done")


def main():
    run_module()


if __name__ == "__main__":
    main()
