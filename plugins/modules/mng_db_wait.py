#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2023

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

from __future__ import absolute_import, division, print_function


__metaclass__ = type


ANSIBLE_METADATA = {"metadata_version": "1.1", "status": ["preview"], "supported_by": "community"}


DOCUMENTATION = r"""
---
module: mng_db_wait
short_description: Wait for an Public Cloud Database to become active
description:
  - Wait until the public cloud database installation is done (status == active)
  - Can be used to wait before running next task in your playbook
author:
  - Mathieu Piriou (@PiMath22)
requirements:
  - python >= 3.6
  - ovh >= 1.0.0
options:
  project_id:
    required: true
    type: str
    description:
      - The ID of public cloud project
  db_instance_name:
    required: true
    description: The name of the database to create
    type: str
  target_status:
    required: false
    type: str
    description: target status to wait for
    default: READY
  max_retry:
    required: false
    type: int
    description: Number of retries
    default: 30
  sleep:
    required: false
    type: int
    description: Time to sleep between retries
    default: 20
  db_engine:
    required: true
    description:
        - |
          The name of the database engine :
          - cassandra
          - m3db
          - mongodb
          - opensearch
          - postgresql
          - grafana
          - kafka
          - mysql
          - redis
    type: str

extends_documentation_fragment:
  - ansible714.ovh.ovh_client_module

"""

EXAMPLES = """
ansible714.ovh.public_cloud_instance_wait:
  db_instance_name: "{{ db_instance_name }}"
  project_id: "{{ project_id }}"
  db_engine: "{{ db_engine }}"
  ovh_api_endpoint: "{{ ovh_api_endpoint }}"
  ovh_api_application_key: "{{ ovh_api_application_key }}"
  ovh_api_application_secret: "{{ ovh_api_application_secret }}"
  ovh_api_consumer_key: "{{ ovh_api_consumer_key }}"
delegate_to: localhost
"""

RETURN = """ # """


from ansible_collections.ansible714.ovh.plugins.module_utils.ovh_client_module import (  # noqa: E402,E501
    OvhClientModule,
)

from ansible_collections.ansible714.ovh.plugins.module_utils.mng_database import (  # noqa: E402,E501
    get_cluster_id_from_module,
    get_db_engine_from_module,
)

import time  # noqa: E402,E501


def run_module():
    module = OvhClientModule(
        argument_spec=dict(
            project_id=dict(type="str", required=True),
            db_instance_name=dict(type="str", required=True),
            max_retry=dict(required=False, default=30, type="int"),
            sleep=dict(required=False, default=20, type="int"),
            target_status=dict(required=False, default="READY", type="str"),
            db_engine=dict(type="str", required=True),
        ),
        supports_check_mode=True,
        required_by={},
    )

    from ovh.exceptions import (
        APIError,
    )

    sleep = module.params["sleep"]
    target_status = module.params["target_status"]

    is_target_status = False

    for i in range(int(module.params["max_retry"])):
        time.sleep(float(sleep))

        result = None

        try:
            result = module.client.get(
                "/cloud/project/{0}/database/{1}/{2}".format(
                    module.params["project_id"],
                    get_db_engine_from_module(module),
                    get_cluster_id_from_module(module),
                )
            )

            if result and "status" in result:
                is_target_status = result["status"] == target_status

        except APIError as error:
            module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

        if is_target_status:
            module.exit_json(changed=True, **result)

    module.fail_json(msg="Max wait time reached, about %i x %i seconds" % (i + 1, int(sleep)))


def main():
    run_module()


if __name__ == "__main__":
    main()
