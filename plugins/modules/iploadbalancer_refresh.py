#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2022

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

from __future__ import absolute_import, division, print_function


__metaclass__ = type


ANSIBLE_METADATA = {"metadata_version": "1.1", "status": ["preview"], "supported_by": "community"}


DOCUMENTATION = r"""
---
module: iploadbalancer_refresh
short_description: Refresh configuration of IP Load-Balancer
description:
  - Apply the configuration defined for the Load-Balancer
author:
  - Christophe Guychard (@xtof-osd)
requirements:
  - "ovh >=  1.0.0"
options:
  name:
    required: true
    description:
      - Name of the IpLoadBalancer
    type: str
  timewait:
    required: false
    description:
      - Time (seconds)to wait between two checks of task status
    default: 10
    type: int
  retries:
    required: false
    description:
      - Number of retries
    default: 12
    type: int

extends_documentation_fragment:
  - ansible714.ovh.ovh_client_module

"""

EXAMPLES = """
- name: Apply the configuration
  ansible714.ovh.iploadbalancer_refresh:
    name: my-load-balancer
    ovh_api_endpoint: '{{ ovh_api_endpoint }}'
    ovh_api_application_key: '{{ ovh_api_application_key }}'
    ovh_api_application_secret: '{{ ovh_api_application_secret }}'
    ovh_api_consumer_key: '{{ ovh_api_consumer_key }}'

"""

RETURN = """
# JSON description of the IP Load-Balancer task, according to OVH-Api schema.
# (https://api.ovh.com/console/#/ipLoadbalancing/{serviceName}/refresh#POST)
"""


from ansible_collections.ansible714.ovh.plugins.module_utils.ovh_client_module import (  # noqa: E402,E501
    OvhClientModule,
)
from time import sleep  # noqa: E402


def run_module():
    module = OvhClientModule(
        argument_spec=dict(
            name=dict(type="str", required=True),
            retries=dict(required=False, default=12, type="int"),
            timewait=dict(required=False, default=10, type="int"),
        ),
        required_by={},
    )

    if module.client is None:
        module.fail_json(msg="Unable to init OVH api Client")

    # Get parameters
    name = module.params.get("name", None)
    max_retries = int(module.params.get("retries", 12))
    timewait = float(module.params.get("timewait", 10))

    retries = 0

    from ovh.exceptions import (
        APIError,
    )

    # looking for an existing refresh task
    existing_task = None
    try:
        existing_tasks = module.client.get(f"/ipLoadbalancing/{name}/task", action="refreshIplb")
        existing_task = existing_tasks[0] if existing_tasks else None
    except APIError as error:
        module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

    # waiting for existing task to finish
    task_status = "unknown"
    while retries < max_retries and existing_task is not None:
        # Check that status of the task
        try:
            task_info = module.client.get(f"/ipLoadbalancing/{name}/task/{existing_task}")
        except APIError as error:
            if error.response is not None and error.response.status_code == 404:
                if retries > 0:
                    # task ended between two iterations
                    module.exit_json(
                        changed=True,
                        msg=f"Task ended and disappeard, last known status was: {task_status}",
                    )

            module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

        task_status = task_info.get("status")
        if task_status in ("todo", "doing"):
            sleep(timewait)
        elif task_status in ("cancelled", "done", "error"):
            existing_task = None
        elif task_status in ("blocked"):
            module.fail_json(
                changed=False, msg="Existing task is blocked! Cannot procced ", **task_info
            )

        retries += 1

    task_info = {}
    # start the refresh task
    try:
        task_info = module.client.post("/ipLoadbalancing/{0}/refresh".format(name))
    except APIError as error:
        module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

    module.exit_json(changed=True, msg="Load-Balancer is being reconfigured", **task_info)


def main():
    run_module()


if __name__ == "__main__":
    main()
