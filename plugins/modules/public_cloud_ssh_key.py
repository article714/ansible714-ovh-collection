#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2023

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

from __future__ import absolute_import, division, print_function

__metaclass__ = type


DOCUMENTATION = r"""
---
module: public_cloud_ssh_key
short_description: Manage ssh_keys for a given OVH Public cloud project
description:
  - |
    This module manage lifecycle (create/delete) of SSH Key(s) information for Public Cloud
    Project.

    Note that updating a key is not supported
author:
  - Christophe Guychard (@xtof-osd)
requirements:
  - ovh >= 1.0.0
options:
  project_id:
    required: true
    description:
      - The ID of project
    type: str
  name:
    required: true
    description:
      - Human readable name for the key
    type: str
  publicKey:
    required: true
    description:
      - The full public ssh public key
    type: str
  region:
    required: false
    description:
      - Region for which the key is usable
    type: str
  state:
    required: false
    description: Indicate the desired state of the Key
    choices:
      - present
      - absent
    default: present
    type: str

extends_documentation_fragment:
  - ansible714.ovh.ovh_client_module

"""

EXAMPLES = """
- name: "Deletes OVH public cloud Omnipotent SSH key "
  ansible714.ovh.public_cloud_ssh_key:
    project_id: "{{ id_project }}"
    name: "My omnipotent key"
    publicKey: >-
      ssh-dss AAAAB3NzaC1kc3MAAACBAI1rN4e6LNwj/g+l4Dn8lAAMSU/Y4NC0UEUY1TFurs/
      TRpxRShxHUXGzTYGUJQDxzA+2rZqC1Q/LMIQbNxFuU1Kcmq99mixYsCA4hmSWy5mDhkcP6D
      uE6uPIjMZHC5RsGEK4z9g1KZuabulnEglUkx/aTQm0jjpRwV9Cf4DrjiobAAAAFQDN4EDX5
      PbiepM2k3XOQRWCnQbCcQAAAIBuG6C5EiOukbNeBC/DcP3Zqd3U8uRsfy6UeSVnjPTpvC8i
      JWNq1pFgaLQ4FDqHr5LBgBIbXQmjF6TYgilX5WTBoXKMBKrYPCdsIu+NdLV8JWFFaI7b2TJ
      cwsc2a2k4iSjDGBwzhw7pX6JtoxjDOcA3UBYgBpKB4kAeOCTtgbXacAAAAIA+NIj5dgTu70
      iv+aroSkkmKy4IFE1ZJCHrbM20QnriapVtvcaU/4UDwoi+ospCDNn32diEFcSDIHZsD3iEc
      +CHrISKOwNylQO2tBi+PcLk8HlhaPOlwd+LFMHsN2XwYdSxHw8PKpR2tcwOaMS8Qz8S+Ycj
      f4aA9FpDP5snSLe4Ag==
    state: absent
"""

RETURN = """
sshkey:
  description: Dictionary holding properties of ssh key.
  returned: success
  type: dict
  contains:
    id:
      type: str
      description: Description of your project
    name:
      type: str
      description: Human readable name for the key
    publicKey:
      type: str
      description: The full public ssh public key
    regions:
      type: list
      elements: str
      description: List of regions for which key is valid

"""


from ansible_collections.ansible714.ovh.plugins.module_utils.ovh_client_module import (  # noqa: E402,E501
    OvhClientModule,
)

try:
    from ovh.exceptions import APIError
except ImportError:
    pass


def run_module():
    module = OvhClientModule(
        argument_spec=dict(
            project_id=dict(type="str", required=True),
            name=dict(type="str", required=True),
            publicKey=dict(type="str", required=True, no_log=False),
            region=dict(type="str", required=False),
            state=dict(type="str", choices=["present", "absent"], default="present"),
        ),
        required_by={},
    )

    project_id = module.params["project_id"]
    name = module.params["name"]
    publicKey = module.params["publicKey"].replace("\n", "")
    region = module.params["region"]
    state = module.params["state"]

    # First, try to find if keys exist
    key_id = None
    try:
        key_ssh_list = module.client.get("/cloud/project/{0}/sshkey".format(project_id))
    except APIError as error:
        module.fail_json(msg="Error getting ssh key list: {0}".format(error))
    for ssh_data in key_ssh_list:
        if ssh_data["name"] == name:
            key_id = ssh_data["id"]
            break

    if state == "present":
        if key_id is None:
            try:
                request_args = {
                    "name": name,
                    "publicKey": publicKey,
                }
                if region:
                    request_args["region"] = region
                ssh_data = module.client.post(
                    "/cloud/project/{0}/sshkey".format(project_id), **request_args
                )
                module.exit_json(changed=True, msg="Ssh Key has been created", **ssh_data)
            except APIError as error:
                module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

        else:
            module.exit_json(changed=False, msg="Ssh key already exists")
    else:
        if key_id is None:
            module.exit_json(changed=False, msg="Ssh key already deleted {0}".format(name), **{})
        else:
            # delete key
            try:
                module.client.delete("/cloud/project/{0}/sshkey/{1}".format(project_id, key_id))
                module.exit_json(
                    changed=True,
                    msg="Ssh Key has been deleted",
                )
            except APIError as error:
                module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

    module.exit_json(changed=False, msg="Nothing has been done", **ssh_data)


def main():
    run_module()


if __name__ == "__main__":
    main()
