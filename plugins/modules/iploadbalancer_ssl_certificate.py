#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2022

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

from __future__ import absolute_import, division, print_function


__metaclass__ = type


ANSIBLE_METADATA = {"metadata_version": "1.1", "status": ["preview"], "supported_by": "community"}


DOCUMENTATION = r"""
---
module: iploadbalancer_ssl_certificate
short_description: Manage ssl/tls certificates for IpLoadBalancer
description:
  - |
    Manage ssl/tls certificates for IpLoadBalancer.
    Certificate can be provided as raw textual data or in
    a pem file containing the certificate and its related key.


    This module can manage both Custome & Free certificates for IpLoadBalancer.
    Free Certificates are Let's Encrypt certificates managed and renewed by Ovh

    Free Certificates have the type "built", where custom one's
    (see iploadbalancer_ssl_certificate) have "custom" type.

author:
  - Christophe Guychard (@xtof-osd)
requirements:
  - "python >= 3.6"
  - "cryptography>=36.0"
  - "ovh >=  1.0.0"
  - "pem"

options:
  lb_name:
    required: true
    description:
      - Name of the IpLoadBalancer
    type: str
  name:
    required: true
    description:
      - Human readable name for your ssl certificate
    type: str
  fqdn:
    required: true
    description:
      - |
        Certificate Full Qualified Domain Name for this certificate.
        FQDN will be unique on a given load-balancer, certificate with same FQDN but different
        serial number or fingerprint will be replaced.
        This enable certificate renewal without duplication of FQDN on load-balancer
    type: str
  certificate:
    required: false
    description:
      - Certificate (should be unique)
    type: str
  chain:
    required: false
    description:
      - Certificate chain
    type: str
  key:
    required: false
    description:
      - Certificate key
    type: str
  pem_file:
    required: false
    description:
      - |
        PEM file containing Certificate, must contain certificate and key.
        Can also contain full-chain (as in let's encrypt)
    type: path
  free_certificate:
    required: false
    description:
      - |
        This module can manage both Custome & Free certificates for IpLoadBalancer.
        Free Certificates are Let's Encrypt certificates managed and renewed by Ovh

        *A certificate cannot be both free and custom...*
    type: bool
    default: false
  state:
    required: false
    description: Indicate the desired state of the Frontend
    choices:
      - present
      - absent
    default: present
    type: str

extends_documentation_fragment:
  - ansible714.ovh.ovh_client_module

"""

EXAMPLES = """
- name: Manage example.com certificate
  ansible714.ovh.iploadbalancer_ssl_certificate:
    lb_name: my-load-balancer
    name: my-example-com-certificate
    fqdn: my-example-com-certificate
    pem_file: /etc/letsencrypt/live/example.com.pem
    state: present
    ovh_api_endpoint: '{{ ovh_api_endpoint }}'
    ovh_api_application_key: '{{ ovh_api_application_key }}'
    ovh_api_application_secret: '{{ ovh_api_application_secret }}'
    ovh_api_consumer_key: '{{ ovh_api_consumer_key }}'
  delegate: localhost
  become: no

- name: Manage example.com certificate
  ansible714.ovh.iploadbalancer_ssl_certificate:
    lb_name: my-load-balancer
    name: my-example-com-certificate
    fqdn: my-example-com-certificate
    free_certificate: true
    state: present
  delegate: localhost
  become: no

"""


RETURN = """
# Json information about certificate follwing OVH-Api format (for existing certs)
# see [doc](https://api.ovh.com/console/#/ipLoadbalancing/{serviceName}/ssl#POST)
"""


from ansible_collections.ansible714.ovh.plugins.module_utils.ovh_client_module import (  # noqa: E402,E501
    OvhClientModule,
)
from ansible_collections.ansible714.ovh.plugins.module_utils.iploadbalancer import (  # noqa: E402,E501
    find_ssl_certificate,
)


def compute_cert_data(module, pem_file=None, certificate=None):
    """
    Computes certificate fingerprint and serial number from a pem_fle or
    plain text certificate
    and extract fqnd, key and certificate chain from pem_file

    Args:
        module (OvhClientModule): AnsibleModule with OVH-Api support
        pem_file (str, optional): name of the pem file. Defaults to None.
        certificate (str, optional): certificate data (plain text). Defaults to None.
    """

    # Import pem and fail if not present
    try:
        import pem
    except ImportError:
        module.fail_json(msg="pem module must be installed to use this collection")
    try:
        from cryptography import x509
        from cryptography.hazmat.primitives.serialization import Encoding
        from cryptography.hazmat.primitives import hashes
    except ImportError:
        module.fail_json(msg="cryptography module must be installed to use this collection")

    cert_fingerprint, cert_serial_number, cert_fqdn, pem_key, pem_certificate, pem_chain = (
        None,
        None,
        None,
        None,
        None,
        None,
    )

    if pem_file:
        cert_data = pem.parse_file(pem_file)
        certs = []
        pem_chain = b""
        for cert_obj in cert_data:
            if isinstance(cert_obj, pem.PrivateKey):
                pem_key = cert_obj.as_text()
            if isinstance(cert_obj, pem.Certificate):
                certs.append(x509.load_pem_x509_certificate(cert_obj.as_bytes()))
        # rebuild pem_chain and certificate
        all_subjects = {c.subject: c for c in certs}
        all_issuers = {c.issuer: c for c in certs}
        for cert in certs:
            if cert.issuer in all_subjects:
                if cert.subject in all_issuers:
                    pem_chain = pem_chain + cert.public_bytes(Encoding.PEM)
                else:
                    pem_certificate = cert.public_bytes(Encoding.PEM).decode("utf-8")
                    cert_serial_number = ("0" + hex(cert.serial_number)[2:]).upper()
                    cert_fingerprint = cert.fingerprint(hashes.SHA1()).hex().upper()
                    cert_fingerprint = ":".join(
                        cert_fingerprint[i : i + 2]  # noqa: E203
                        for i in range(0, len(cert_fingerprint), 2)
                    )
                    cert_fqdn = cert.subject.rfc4514_string().replace("CN=", "")
        if pem_certificate is None or pem_key is None:
            module.fail_json(msg="Wrong Pem file provided, missing some information")
        pem_chain = pem_chain.decode("utf-8")
    elif certificate:
        cert = x509.load_pem_x509_certificate(certificate.encode("utf-8"))
        cert_fingerprint = cert.fingerprint(hashes.SHA1()).hex().upper()
        cert_fingerprint = ":".join(
            cert_fingerprint[i : i + 2] for i in range(0, len(cert_fingerprint), 2)  # noqa: E203
        )
        cert_serial_number = ("0" + hex(cert.serial_number)[2:]).upper()
        cert_fqdn = cert.subject.rfc4514_string().replace("CN=", "")

    return (cert_fingerprint, cert_serial_number, cert_fqdn, pem_key, pem_certificate, pem_chain)


def run_module():
    """Module main function"""

    module = OvhClientModule(
        argument_spec=dict(
            lb_name=dict(type="str", required=True),
            name=dict(type="str", required=True),
            fqdn=dict(type="str", required=True),
            certificate=dict(type="str", required=False),
            chain=dict(type="str", required=False),
            free_certificate=dict(type="bool", required=False, default=False),
            key=dict(type="str", required=False, no_log=True),
            pem_file=dict(type="path", required=False),
            state=dict(type="str", choices=["present", "absent"], default="present"),
        ),
        mutually_exclusive=[
            ("pem_file", "key"),
            ("pem_file", "chain"),
            ("pem_file", "certificate"),
            ("pem_file", "free_certificate"),
            ("free_certificate", "key"),
            ("free_certificate", "chain"),
            ("free_certificate", "certificate"),
        ],
        required_by={},
    )

    # Get parameters
    lb_name = module.params.get("lb_name")
    name = module.params.get("name")
    fqdn = module.params.get("fqdn")
    certificate = module.params.get("certificate")
    chain = module.params.get("chain")
    free_certificate = module.params.get("free_certificate")
    key = module.params.get("key")
    pem_file = module.params.get("pem_file")
    state = module.params.get("state")

    # Import exceptions (inside module to avoid earlier errors if ovh module is not present)
    from ovh.exceptions import (
        APIError,
    )

    # Compute certificate finger_print and serial_numner

    if pem_file or certificate:
        (
            cert_fingerprint,
            cert_serial_number,
            cert_fqdn,
            pem_key,
            pem_certificate,
            pem_chain,
        ) = compute_cert_data(module, pem_file, certificate)
        if pem_key is not None:
            key = pem_key
        if pem_certificate is not None:
            certificate = pem_certificate
        if pem_chain is not None:
            chain = pem_chain
    elif not free_certificate:
        module.fail_json(msg="No Certificate information provided")
    else:
        cert_fingerprint, cert_serial_number, cert_fqdn = None, None, None
        if fqdn is None:
            module.fail_json(msg="No FQDN provided for free Certificate")
    if cert_fingerprint or cert_serial_number:
        if fqdn != cert_fqdn:
            module.fail_json(
                msg="PEM file FQDN ({0}) inconsistent with required fqdn: {1} ".format(
                    fqdn, cert_fqdn
                )
            )
    if cert_fqdn:
        fqdn = cert_fqdn

    # ------------------
    # Looking for an existing certificate
    existing_certid, existing_certinfo = find_ssl_certificate(
        module,
        lb_name,
        cert_fingerprint=cert_fingerprint,
        cert_serial_number=cert_serial_number,
        fqdn=fqdn,
        cert_name=name,
        is_free_cert=free_certificate,
    )

    # ------------------
    # Depending on state value, process data

    request_args = {"certificate": certificate, "displayName": name, "key": key}
    if chain:
        request_args["chain"] = chain

    if state == "present":
        if existing_certid is None:
            if free_certificate:
                # create Free  Cert
                try:
                    result = module.client.post(
                        "/ipLoadbalancing/{0}/freeCertificate".format(lb_name), fqdn=[fqdn]
                    )
                    module.exit_json(
                        changed=True, msg="SSL Certificate (Free) has been created", **result
                    )
                except APIError as error:
                    module.fail_json(
                        msg="Failed to execute OVH-Api call : {0}  => {1}".format(
                            error, request_args
                        )
                    )
            else:
                # create Custom Cert
                try:
                    result = module.client.post(
                        "/ipLoadbalancing/{0}/ssl".format(lb_name), **request_args
                    )
                    module.exit_json(
                        changed=True, msg="SSL Certificate has been created", **result
                    )
                except APIError as error:
                    module.fail_json(
                        msg="Failed to execute OVH-Api call : {0}  => {1}".format(
                            error, request_args
                        )
                    )

        elif not free_certificate:
            # update Custom Cert (in fact needs deletion and re-creation)

            if (
                existing_certinfo["fingerprint"] != cert_fingerprint
                or existing_certinfo["serial"] != cert_serial_number
            ):
                # update only if needed
                try:
                    module.client.delete(
                        "/ipLoadbalancing/{0}/ssl/{1}".format(lb_name, existing_certid)
                    )
                    result = module.client.post(
                        "/ipLoadbalancing/{0}/ssl".format(lb_name), **request_args
                    )
                    module.exit_json(
                        changed=True,
                        msg="SSL Certificate has been updated (re-created )",
                        **result,
                    )
                except APIError as error:
                    module.fail_json(
                        msg="Failed to execute OVH-Api call : {0}  => {1}".format(
                            error, request_args
                        )
                    )

        else:
            # update Free Cert (display name for free certificate)

            if existing_certinfo["displayName"] != name:
                # update name only if needed
                try:
                    result = module.client.put(
                        "/ipLoadbalancing/{0}/ssl/{1}".format(lb_name, existing_certid),
                        displayName=name,
                    )
                    certinfo = module.client.get(
                        "/ipLoadbalancing/{0}/ssl/{1}".format(lb_name, existing_certid)
                    )
                    module.exit_json(
                        changed=True, msg="SSL Certificate has been updated", **certinfo
                    )
                except APIError as error:
                    module.fail_json(
                        msg="Failed to execute OVH-Api call : {0}  => {1}".format(
                            error, request_args
                        )
                    )

    else:
        if existing_certid is not None:
            # delete Certificate
            try:
                module.client.delete(
                    "/ipLoadbalancing/{0}/ssl/{1}".format(lb_name, existing_certid)
                )
                module.exit_json(changed=True, msg="Certificate has been deleted")
            except APIError as error:
                module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

    if existing_certinfo:
        module.exit_json(
            changed=False, msg="Nothing has been done, certificate found", **existing_certinfo
        )
    module.exit_json(changed=False, msg="Nothing has been done, certificate not found")


def main():
    run_module()


if __name__ == "__main__":
    main()
