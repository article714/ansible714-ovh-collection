#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2023

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

from __future__ import absolute_import, division, print_function

__metaclass__ = type


DOCUMENTATION = r"""
---
module: public_cloud_ssh_keys_facts
short_description: Get OVH Public cloud project SSH Key(s) information
description:
    - This module retrieves SSH Key(s) information from Public Cloud Project and
      provide them as facts
author:
  - Christophe Guychard (@xtof-osd)
requirements:
    - ovh >= 1.0.0
options:
    project_id:
        required: true
        description:
            - The ID of project
        type: str

extends_documentation_fragment:
  - ansible714.ovh.ovh_client_module
"""

EXAMPLES = """
- name: "Get info on OVH public cloud SSH key {{ ssh_key_name }} "
  ansible714.ovh.public_cloud_ssh_keys_facts:
    project_id: "{{ id_project }}"
"""

RETURN = """
ovhcloud:
  description: Dictionary for Ovh-Cloud related facts.
  returned: success
  type: dict
  contains:
    projects:
      description: Dictionary containing all projects info, using project_id as key
      returned: success
      type: dict
      contains:
        ssh_keys:
          returned: success
          description: List of existing keys on the project
          type: list
          elements: dict
          sample:
            - id: 45dsdsf5sdf6sdf
              name: my_key
              publicKey: "sdfqdsf...qs566"
              regions:
                - GRA11
                - SBG5

"""


from ansible_collections.ansible714.ovh.plugins.module_utils.ovh_client_module import (  # noqa: E402,E501
    OvhClientModule,
)

try:
    from ovh.exceptions import APIError
except ImportError:
    pass


def run_module():
    module = OvhClientModule(
        argument_spec=dict(
            project_id=dict(type="str", required=True),
        ),
        supports_check_mode=True,
        required_by={},
    )

    project_id = module.params["project_id"]

    try:
        key_ssh_list = module.client.get("/cloud/project/{0}/sshkey".format(project_id))
    except APIError as error:
        module.fail_json(msg="Error getting ssh key list: {0}".format(error))

    module.exit_json(
        changed=False,
        ansible_facts={"ovhcloud": {"projects": {project_id: {"ssh_keys": key_ssh_list}}}},
    )


def main():
    run_module()


if __name__ == "__main__":
    main()
