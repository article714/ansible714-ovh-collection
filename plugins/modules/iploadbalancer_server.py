#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2022

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

from __future__ import absolute_import, division, print_function


__metaclass__ = type


ANSIBLE_METADATA = {"metadata_version": "1.1", "status": ["preview"], "supported_by": "community"}


DOCUMENTATION = r"""
---
module: iploadbalancer_server
short_description: Manage Server (in a Farm) for OVH IpLoadBalancer
description:
  - Manage Server (in a Farm) for OVH IpLoadBalancer
author:
  - Christophe Guychard (@xtof-osd)
requirements:
  - "ovh >=  1.0.0"
options:
  lb_name:
    required: true
    description:
      - Name of the IpLoadBalancer
    type: str
  farm_id:
    required: true
    description:
      - Id Of the farm to which server is attached
    type: int
  farm_type:
    required: true
    description:
      - Kind of Farm
    choices:
      - 'http'
      - 'tcp'
      - 'udp'
    type: str
  address:
    required: true
    description:
      - |
        IP Address of the server
        For this module to work, this address must me unique for a given Farm
    type: str
  backup:
    required: false
    description:
      - Set server as backup
    type: bool
    default: false
  chain:
    required: False
    description:
      - SSL/TLS Certificate chain
    type: str
  cookie:
    required: False
    description:
      - Set the cookie value used when 'cookie' stickiness is set in the farm
    type: str
  name:
    required: true
    description:
      - Human readable name for your server.
    type: str
  port:
    required: false
    description:
      - Port attached to your server
    type: int
  probe:
    required: false
    description:
      - Enable/disable probe
    type: bool
    default: true
  proxyProtocolVersion:
    required: false
    description:
      - Send PROXY protocol header
    type: str
    default: null
  ssl:
    required: false
    description:
      - SSL ciphering. Probes will also be sent ciphered.
    type: bool
    default: false
  status:
    required: false
    description:
      - Enable or disable your server
    type: str
    choices:
      - active
      - inactive
    default: active
  weight:
    required: false
    description:
      - |
        Set weight on that server [1..256].
        0 if not used in load balancing.
        1 if left null.
        Servers with higher weight get more requests.
    type: int
    default: 0
  state:
    required: false
    description: Indicate the desired state of the Server
    choices:
      - present
      - absent
    type: str
    default: present

extends_documentation_fragment:
  - ansible714.ovh.ovh_client_module

"""

EXAMPLES = """
# Example extracted from ip_load_balancer role
- name: Manage Servers farms
  ansible714.ovh.iploadbalancer_server:
    lb_name: '{{ ansible_host }}'
    farm_id: '{{ item.farm_id }}'
    farm_type: '{{ item.farm_type }}'
    address: '{{ item.address }}'
    backup: '{{ item.backup }}'
    chain: '{{ item.chain }}'
    cookie: '{{ item.cookie }}'
    displayName: '{{ item.displayName }}'
    port: '{{ item.port }}'
    probe: '{{ item.probe }}'
    proxyProtocolVersion: '{{ item.proxyProtocolVersion }}'
    ssl: '{{ item.ssl }}'
    status: '{{ item.status }}'
    weight: '{{ item.weight }}'
    ovh_api_endpoint: '{{ ovh_api_endpoint }}'
    ovh_api_application_key: '{{ ovh_api_application_key }}'
    ovh_api_application_secret: '{{ ovh_api_application_secret }}'
    ovh_api_consumer_key: '{{ ovh_api_consumer_key }}'
  with_items: '{{ servers }}'
  delegate_to: localhost
  become: no

"""

RETURN = """
# TODO
"""


from ansible_collections.ansible714.ovh.plugins.module_utils.ovh_client_module import (  # noqa: E402,E501
    OvhClientModule,
)


def run_module():
    module = OvhClientModule(
        argument_spec=dict(
            lb_name=dict(type="str", required=True),
            address=dict(type="str", required=True),
            backup=dict(type="bool", required=False, default=False),
            chain=dict(type="str", required=False),
            cookie=dict(type="str", required=False, default=None),
            farm_id=dict(type="int", required=True),
            farm_type=dict(required=True, type="str", choices=("http", "tcp", "udp")),
            name=dict(type="str", required=True),
            port=dict(type="int", required=False),
            probe=dict(type="bool", required=False, default=True),
            proxyProtocolVersion=dict(type="str", required=False),
            ssl=dict(type="bool", required=False, default=False),
            state=dict(type="str", choices=["present", "absent"], default="present"),
            status=dict(
                type="str", required=False, choices=("active", "inactive"), default="active"
            ),
            weight=dict(type="int", required=False, default=0),
        ),
        required_by={},
    )

    # Get parameters
    address = module.params.get("address")
    backup = module.params.get("backup")
    chain = module.params.get("chain")
    cookie = module.params.get("cookie")
    name = module.params.get("name")
    farm_id = module.params.get("farm_id")
    farm_type = module.params.get("farm_type")
    lb_name = module.params.get("lb_name")
    port = module.params.get("port")
    probe = module.params.get("probe")
    proxyProtocolVersion = module.params.get("proxyProtocolVersion")
    ssl = module.params.get("ssl")
    state = module.params.get("state")
    status = module.params.get("status")
    weight = module.params.get("weight")

    request_args = {
        "address": address,
        "backup": backup,
        "chain": chain,
        "cookie": cookie,
        "displayName": name,
        "port": port,
        "probe": probe,
        "proxyProtocolVersion": proxyProtocolVersion,
        "ssl": ssl,
        "status": status,
        "weight": weight,
    }

    # ------------------
    # Adapt arguments to farm_type
    if farm_type == "tcp":
        request_args.pop("cookie")
    elif farm_type == "udp":
        request_args.pop("backup")
        request_args.pop("chain")
        request_args.pop("cookie")
        request_args.pop("probe")
        request_args.pop("proxyProtocolVersion")
        request_args.pop("ssl")
        request_args.pop("weight")

    # Import exceptions (inside module to avoid earlier errors if ovh module is not present)
    from ovh.exceptions import (
        APIError,
    )

    existing_serverid = None
    # ------------------
    # Look for an existing server (using farm_id and server's address)
    try:
        result = module.client.get(
            "/ipLoadbalancing/{0}/{1}/farm/{2}/server".format(lb_name, farm_type, farm_id),
            address=address,
        )
        if result:
            if len(result) == 1:
                existing_serverid = result[0]
            elif len(result) == 0:
                existing_serverid = None
            else:
                module.fail_json(msg="Too many servers for given address : {0}".format(address))
    except APIError as error:
        if error.response is not None and error.response.status_code == 404:
            existing_serverid = None
        module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

    # ------------------
    # Depending on state value, process data
    if state == "present":
        if existing_serverid is None:
            # create Farm
            try:
                result = module.client.post(
                    "/ipLoadbalancing/{0}/{1}/farm/{2}/server".format(lb_name, farm_type, farm_id),
                    **request_args,
                )
                module.exit_json(changed=True, msg="Server has been created", **result)
            except APIError as error:
                module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))
        else:
            try:
                # should update Server
                module.client.put(
                    "/ipLoadbalancing/{0}/{1}/farm/{2}/server/{3}".format(
                        lb_name, farm_type, farm_id, existing_serverid
                    ),
                    **request_args,
                )
                module.exit_json(changed=True, msg="Server has been updated")
            except APIError as error:
                module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

    else:
        if existing_serverid is None:
            module.exit_json(changed=False, msg="Server already deleted {0}".format(name))
        else:
            # delete Server
            try:
                module.client.delete(
                    "/ipLoadbalancing/{0}/{1}/farm/{2}/server/{3}".format(
                        lb_name, farm_type, farm_id, existing_serverid
                    )
                )
                module.exit_json(changed=True, msg="Server has been deleted")
            except APIError as error:
                module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

    module.exit_json(changed=False, msg="Nothing has been done")


def main():
    run_module()


if __name__ == "__main__":
    main()
