#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2022

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

from __future__ import absolute_import, division, print_function


__metaclass__ = type


ANSIBLE_METADATA = {"metadata_version": "1.1", "status": ["preview"], "supported_by": "community"}


DOCUMENTATION = r"""
---
module: iploadbalancer_info
short_description: Provide information about OVH IP Load-Balancer
description:
  - Provide information about OVHCloud IpLoadBalancer instances
author:
  - Christophe Guychard (@xtof-osd)
requirements:
  - "ovh >=  1.0.0"
options:
  name:
    required: true
    description:
      - Name of the IpLoadBalancer
    type: str

extends_documentation_fragment:
  - ansible714.ovh.ovh_client_module


"""

EXAMPLES = """
- name: Gets info about my-load-balancer
  ansible714.ovh.iploadbalancer_info:
    name: my-load-balancer
    ovh_api_endpoint: '{{ ovh_api_endpoint }}'
    ovh_api_application_key: '{{ ovh_api_application_key }}'
    ovh_api_application_secret: '{{ ovh_api_application_secret }}'
    ovh_api_consumer_key: '{{ ovh_api_consumer_key }}'

"""

RETURN = """
# JSON description of the IP Load-Balancer, according to OVH-Api schema.
# (https://eu.api.ovh.com/console/#/ipLoadbalancing/{serviceName}#GET)
"""


from ansible_collections.ansible714.ovh.plugins.module_utils.ovh_client_module import (  # noqa: E402,E501
    OvhClientModule,
)
from ansible_collections.ansible714.ovh.plugins.module_utils.iploadbalancer import (  # noqa: E402,E501
    get_iplb_info,
)


def run_module():
    module = OvhClientModule(
        argument_spec=dict(
            name=dict(required=True),
        ),
        supports_check_mode=True,
        required_by={},
    )

    loadBalancerData = get_iplb_info(module)

    if loadBalancerData is None:
        # This should not happen
        module.fail_json(msg="Load-Balancer does not exist")
    else:
        module.exit_json(changed=False, **loadBalancerData)


def main():
    run_module()


if __name__ == "__main__":
    main()
