#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2023

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

from __future__ import absolute_import, division, print_function


__metaclass__ = type


ANSIBLE_METADATA = {"metadata_version": "1.1", "status": ["preview"], "supported_by": "community"}


DOCUMENTATION = r"""
---
module: public_cloud_failover_ip_info
short_description: Retrieve all info for a OVH failover IP
description:
    - This module retrieves all info from a OVH failover IP
author:
    - Christophe Guychard (@xtof-osd)
    - Mathieu Piriou (@PiMath22)
requirements:
    - python >= 3.6
    - ovh >= 1.0.0
options:
    project_id:
        required: true
        type: str
        description: The OVH Public cloud project Id
    fo_ip:
        type: str
        required: true
        description:
            - The fail-over IP

extends_documentation_fragment:
    - ansible714.ovh.ovh_client_module


"""

EXAMPLES = """
- name: Manages an OVH Public Cloud Instance
  ansible714.ovh.public_cloud_failover_ip_info:
      ovh_api_endpoint: "{{ ovh_api_endpoint }}"
      ovh_api_application_key: "{{ ovh_api_application_key }}"
      ovh_api_application_secret: "{{ ovh_api_application_secret }}"
      ovh_api_consumer_key: "{{ ovh_api_consumer_key }}"
      fo_ip: "{{ fo_ip }}"
      project_id: "{{ project_id }}"
  delegate_to: localhost

"""

RETURN = """
FailoverIp:
  type: dict
  returned: success
  description: Detailed information about IP Fail Over
  contains:
    block:
      description: IP block
      type: str
    continentCode:
      description: Ip continent
      type: str
    geoloc:
      description: Ip location
      type: str
    id:
      description: Ip id
      type: str
    ip:
      description: Ip
      type: str
    progress:
      description: Current operation progress in percent
      type: int
    routedTo:
      description: Instance where ip is routed to
      type: str
    status:
      description: Ip status
      type: str
    subType:
      description: IP sub type
      type: str

"""


from ansible_collections.ansible714.ovh.plugins.module_utils.ovh_client_module import (  # noqa: E402,E501
    OvhClientModule,
)


def run_module():
    module = OvhClientModule(
        argument_spec=dict(
            project_id=dict(type="str", required=True), fo_ip=dict(type="str", required=True)
        ),
        supports_check_mode=True,
        required_by={},
    )

    # Get Parameters
    fo_ip = module.params["fo_ip"]
    project_id = module.params["project_id"]

    # Import exceptions (inside module to avoid earlier errors if ovh module is not present)
    from ovh.exceptions import (
        APIError,
    )

    try:
        fo_ips_list = module.client.get("/cloud/project/{0}/ip/failover".format(project_id))
    except APIError as error:
        module.fail_json(msg="Error getting failover ips list: {0}".format(error))

    for ip_data in fo_ips_list:
        if ip_data["ip"] == fo_ip:
            module.exit_json(changed=False, **ip_data)

    module.fail_json(
        msg="Error: could not find given fail-over IP {0} in {1}".format(fo_ip, fo_ips_list)
    )


def main():
    run_module()


if __name__ == "__main__":
    main()
