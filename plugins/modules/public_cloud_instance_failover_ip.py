#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2022-2023

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

from __future__ import absolute_import, division, print_function


__metaclass__ = type


ANSIBLE_METADATA = {"metadata_version": "1.1", "status": ["preview"], "supported_by": "community"}


DOCUMENTATION = r"""
---
module: public_cloud_instance_failover_ip
short_description: "Manage OVH API for public cloud: attach fail-over IP to instance"
description:
    - This module manage the attachment of  fail-over IP to an OVH public Cloud instance
author:
    - Christophe Guychard (@xtof-osd)
    - Mathieu Piriou (@PiMath22)
requirements:
    - python >= 3.6
    - ovh >= 1.0.0
options:
    project_id:
        required: true
        description:
            - |
              The named/id of the project, that can be obtained
              using public_cloud_project_info module
        type: str
    instance_id:
        required: true
        description:
            - The instance name
        type: str
    fo_ip_id:
        required: true
        description:
            - The id of the fail-over IP
        type: str
    state:
        required: false
        default: present
        choices: ['present','absent']
        description: Indicate the desired state of vrack
        type: str

extends_documentation_fragment:
  - ansible714.ovh.ovh_client_module

"""

EXAMPLES = """
- name: "Attach an fail-over IP to {{ instance_id }} on public cloud OVH"
  synthesio.ovh.public_cloud_instance_failover_ip:
      ovh_api_endpoint: "{{ ovh_api_endpoint }}"
      ovh_api_application_key: "{{ ovh_api_application_key }}"
      ovh_api_application_secret: "{{ ovh_api_application_secret }}"
      ovh_api_consumer_key: "{{ ovh_api_consumer_key }}"
      instance_id: "{{ instance_id }}"
      project_id: "{{ project_id }}"
      fo_ip_id: "{{ fo_ip_id }}"
"""

RETURN = """ # """


from ansible_collections.ansible714.ovh.plugins.module_utils.ovh_client_module import (  # noqa: E402,E501
    OvhClientModule,
)


def run_module():
    module = OvhClientModule(
        argument_spec=dict(
            project_id=dict(type="str", required=True),
            fo_ip_id=dict(type="str", required=True),
            instance_id=dict(type="str", required=True),
            state=dict(type="str", choices=["present", "absent"], default="present"),
        ),
        required_by={},
    )

    # Get parameters
    project_id = module.params["project_id"]
    fo_ip_id = module.params["fo_ip_id"]
    instance_id = module.params["instance_id"]
    state = module.params["state"]

    if module.check_mode:
        module.exit_json(
            msg="success: at/de/tached ({0}) IP {1} to/from instance {2}/{3} (dry run)".format(
                fo_ip_id, state, instance_id, project_id
            ),
            changed=True,
        )

    # Import exceptions (inside module to avoid earlier errors if ovh module is not present)
    from ovh.exceptions import (
        APIError,
    )

    # get data about ip
    ip_data = None
    try:
        ip_data = module.client.get(
            "/cloud/project/{0}/ip/failover/{1}".format(project_id, fo_ip_id)
        )
    except APIError as error:
        module.fail_json(msg="Error getting failover ip data list: {0}".format(error))

    is_already_attached = False
    if ip_data and ip_data["routedTo"] == instance_id:
        is_already_attached = True

    # Attach or detach
    if state == "present":
        if not is_already_attached:
            try:
                attach_result = module.client.post(
                    "/cloud/project/{0}/ip/failover/{1}/attach".format(project_id, fo_ip_id),
                    instanceId=instance_id,
                )

                module.exit_json(
                    msg="Fail-over IP {0} has been attached to instance {1}/{2}".format(
                        fo_ip_id, instance_id, project_id
                    ),
                    result=attach_result,
                    changed=True,
                )

            except APIError as error:
                module.fail_json(msg="Failed to call OVH API: {0}".format(error))

        module.exit_json(
            msg="Fail-over IP {0} interface already exists on instance {1}".format(
                fo_ip_id, instance_id
            ),
            changed=False,
        )

    else:
        if is_already_attached:
            module.fail_json(
                msg="NOT SUPPORTED BY API YET: Do no know how to remove fo_ip_id from instance... "
            )

        module.exit_json(
            msg="Fail-over IP {0} interface does not exist on instance {1}".format(
                fo_ip_id, instance_id
            ),
            changed=False,
        )


def main():
    run_module()


if __name__ == "__main__":
    main()
