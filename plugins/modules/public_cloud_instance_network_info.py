#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2023

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

from __future__ import absolute_import, division, print_function


__metaclass__ = type


ANSIBLE_METADATA = {"metadata_version": "1.1", "status": ["preview"], "supported_by": "community"}


DOCUMENTATION = r"""
---
module: public_cloud_instance_network_info
short_description: Retrieve network interface(s) info for a OVH public cloud instance
description:
  - |
    This module queries OVH API to retrieve information about the netwwork
    interfaces attached to a Public Cloud Instance
author:
  - Christophe Guychard (@xtof-osd)
requirements:
  - python >= 3.6
  - ovh >= 1.0.0
options:
  project_id:
    required: true
    description: The OVH API project_id is the Public cloud project Id
    type: str
  instance_id:
    required: true
    description: The instance uuid
    type: str

extends_documentation_fragment:
  - ansible714.ovh.ovh_client_module

"""

EXAMPLES = """
- name: Get data about networks
  ansible714.ovh.public_cloud_instance_network_info:
    ovh_api_endpoint: "{{ ovh_api_endpoint }}"
    ovh_api_application_key: "{{ ovh_api_application_key }}"
    ovh_api_application_secret: "{{ ovh_api_application_secret }}"
    ovh_api_consumer_key: "{{ ovh_api_consumer_key }}"
    instance_id: "{{ instance_id }}"
    project_id: "{{ project_id }}"
  delegate_to: localhost
"""

RETURN = """
InterfaceDetails:
  description: Data about interfaces available for the given Public cloud instance
  returned: success
  type: dict
  contains:
    interfaces:
      description: List of interfaces available for the given Public cloud instance
      returned: success
      type: list
      elements: dict
      contains:
        fixedIps:
           description: List of ips of the interface
           type: list
           elements: dict
           contains:
             ip:
              type: str
              description: Ip
             subnetId:
              description: Subnetwork Id
              type: str
        id:
           description: Interface unique identifier
           type: str
        macAddress:
           description: Mac address
           type: str
        networkId:
           description: Network id
           type: str
        state:
           description: Openstack state
           type: str
        type:
           description: Network type
           type: str

"""


from ansible_collections.ansible714.ovh.plugins.module_utils.ovh_client_module import (  # noqa: E402,E501
    OvhClientModule,
)


def run_module():
    module = OvhClientModule(
        argument_spec=dict(
            project_id=dict(type="str", required=True), instance_id=dict(type="str", required=True)
        ),
        supports_check_mode=True,
        required_by={},
    )

    # Get Parameters
    instance_id = module.params["instance_id"]
    project_id = module.params["project_id"]

    # Import exceptions (inside module to avoid earlier errors if ovh module is not present)
    from ovh.exceptions import (
        APIError,
    )

    try:
        result = module.client.get(
            "/cloud/project/{0}/instance/{1}/interface".format(project_id, instance_id)
        )
        if result:
            module.exit_json(changed=False, **{"interfaces": result})
        else:
            module.exit_json(changed=False, **{"interfaces": []})
    except APIError as error:
        module.fail_json(msg="Failed to call OVH API: {0}".format(error))


def main():
    run_module()


if __name__ == "__main__":
    main()
