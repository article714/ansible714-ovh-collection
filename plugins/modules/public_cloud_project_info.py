#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2022

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

from __future__ import absolute_import, division, print_function


__metaclass__ = type


ANSIBLE_METADATA = {"metadata_version": "1.1", "status": ["preview"], "supported_by": "community"}


DOCUMENTATION = r"""
---
module: public_cloud_project_info
short_description: Provides information on an OVH Public Cloud project
description:
  - |
    This module retrieves information from OVH Public Cloud project
    using human-readble project name (description)
author:
  - Christophe Guychard (@xtof-osd)
requirements:
  - ovh >= 1.0.0
options:
  name:
    required: true
    description:
      - The project human-readable name
    type: str

extends_documentation_fragment:
  - ansible714.ovh.ovh_client_module

"""

EXAMPLES = """
- name: "Provide data on OVH Public Cloud project "
  ansible714.ovh.public_cloud_project_info:
    name: "my-project"
    ovh_api_endpoint: '{{ ovh_api_endpoint }}'
    ovh_api_application_key: '{{ ovh_api_application_key }}'
    ovh_api_application_secret: '{{ ovh_api_application_secret }}'
    ovh_api_consumer_key: '{{ ovh_api_consumer_key }}'
"""

RETURN = """
project_info:
    description: Dictionary containing all projects info
    returned: success
    type: dict
    contains:
        access:
            description: Project access
            type: str
        creationDate:
            type: str
            description: Project creation date
        description:
            type: str
            description: Description of your project
        expiration:
            type: str
            description: |
                            Expiration date of your project. After this date,
                            your project will be deleted
        manualQuota:
            type: bool
            description: Manual quota prevent automatic quota upgrade
        orderId:
            type: int
            description: Project order id
        planCode:
            type: str
            description: Order plan code
        projectName:
            type: str
            description: Project name
        status:
            type: str
            description: Current status
        unleash:
            type: bool
            description: Project unleashed
"""


from ansible_collections.ansible714.ovh.plugins.module_utils.ovh_client_module import (  # noqa: E402,E501
    OvhClientModule,
)


def run_module():
    module = OvhClientModule(
        argument_spec=dict(
            name=dict(type="str", required=True),
        ),
        supports_check_mode=True,
        required_by={},
    )

    project_name = module.params.get("name")

    from ovh.exceptions import (
        APIError,
    )

    try:
        projects = module.client.get("/cloud/project")
    except APIError as error:
        module.fail_json(msg="Error getting projects list: {0}".format(error))

    for projectid in projects:
        try:
            result = module.client.get("/cloud/project/%s" % projectid)
        except APIError as error:
            module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

        if result["description"] == project_name:
            break
        else:
            result = None

    if result:
        module.exit_json(changed=False, **result)
    else:
        module.fail_json(msg="OVH Public Cloud project does not exist: %s" % project_name)


def main():
    run_module()


if __name__ == "__main__":
    main()
