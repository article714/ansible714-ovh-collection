#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2022

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

from __future__ import absolute_import, division, print_function


__metaclass__ = type


ANSIBLE_METADATA = {"metadata_version": "1.1", "status": ["preview"], "supported_by": "community"}


DOCUMENTATION = r"""
---
module: public_cloud_projects_facts
short_description: Provides facts about all available Ovhcloud projects
description:
  - |
    This module retrieves information on all available Ovhcloud projects and
    provide them as facts usable by other modules
author:
  - Christophe Guychard (@xtof-osd)
requirements:
  - ovh >= 1.0.0

extends_documentation_fragment:
  - ansible714.ovh.ovh_client_module

"""

EXAMPLES = """
- name: "Provide data on OVH Public Cloud projects "
  ansible714.ovh.public_cloud_projects_facts:
    ovh_api_endpoint: '{{ ovh_api_endpoint }}'
    ovh_api_application_key: '{{ ovh_api_application_key }}'
    ovh_api_application_secret: '{{ ovh_api_application_secret }}'
    ovh_api_consumer_key: '{{ ovh_api_consumer_key }}'
"""

RETURN = """
# Add some facts under ovhcloud key
ovhcloud:
    description: Dictionary for Ovh-Cloud related facts.
    returned: success
    type: dict
    contains:
        projects:
            description: Dictionary containing all projects info, using project_id as key
            returned: success
            type: dict
            contains:
                access:
                    description: Project access
                    type: str
                creationDate:
                    type: str
                    description: Project creation date
                description:
                    type: str
                    description: Description of your project
                expiration:
                    type: str
                    description: |
                                   Expiration date of your project. After this date,
                                   your project will be deleted
                manualQuota:
                    type: bool
                    description: Manual quota prevent automatic quota upgrade
                orderId:
                    type: int
                    description: Project order id
                planCode:
                    type: str
                    description: Order plan code
                projectName:
                    type: str
                    description: Project name
                status:
                    type: str
                    description: Current status
                unleash:
                    type: bool
                    description: Project unleashed
"""


from ansible_collections.ansible714.ovh.plugins.module_utils.ovh_client_module import (  # noqa: E402,E501
    OvhClientModule,
)


def run_module():
    module = OvhClientModule(argument_spec=dict(), supports_check_mode=True, required_by={})

    from ovh.exceptions import (
        APIError,
    )

    projects_facts = {}

    try:
        projects = module.client.get("/cloud/project")
    except APIError as error:
        module.fail_json(msg="Error getting projects list: {0}".format(error))

    for projectid in projects:
        try:
            projects_facts[projectid] = module.client.get("/cloud/project/%s" % projectid)
        except APIError as error:
            module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

    module.exit_json(changed=False, ansible_facts={"ovhcloud": {"projects": projects_facts}})


def main():
    run_module()


if __name__ == "__main__":
    main()
