#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2022

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

from __future__ import absolute_import, division, print_function


__metaclass__ = type


ANSIBLE_METADATA = {"metadata_version": "1.1", "status": ["preview"], "supported_by": "community"}


DOCUMENTATION = r"""
---
module: iploadbalancer_farm
short_description: Manage Server Farm for OVH IpLoadBalancer
description:
  - Manage Server Farm for OVH IpLoadBalancer
author:
  - Christophe Guychard (@xtof-osd)
requirements:
  - "ovh >=  1.0.0"
options:
  lb_name:
    required: true
    description:
      - Name of the IpLoadBalancer
    type: str
  farm_type:
    required: true
    description:
      - Kind of Farm to create.
    choices:
      - "http"
      - "tcp"
      - "udp"
    type: str
  balancing_algorithm:
    required: false
    description:
      - Load balancing algorithm.
    type: str
    choices:
      - "first"
      - "uri"
      - "leastconn"
      - "roundrobin"
      - "source"
    default: "roundrobin"
  name:
    required: false
    description:
      - |
        Human readable name for your backend.
        For this module to work, this name must me unique
    type: str
  port:
    required: false
    description:
      - Port attached to your farm
    type: int
  probe:
    required: false
    description:
      - |
        Specification of Healthcheck probe, according
        to https://docs.ovh.com/fr/load-balancer/probes/
    type: dict
  stickiness:
    required: false
    description:
      - http sessions stickiness type. No stickiness if null.
    choices:
      - "cookie"
      - "sourceIp"
    type: str
  vrackNetworkId:
    required: false
    description:
      - Internal Load Balancer identifier of the vRack private network to attach to your farm
    type: str
  zone:
    required: true
    description:
      - Zone of your farm
    type: str
  farm_id:
    required: false
    description:
      - Id Of the farm if the farm already exists
    type: int
  state:
    required: false
    description: Indicate the desired state of the Farm
    choices:
      - present
      - absent
    default: present
    type: str

extends_documentation_fragment:
  - ansible714.ovh.ovh_client_module

"""

EXAMPLES = """
# Example extracted from ip_load_balancer role
- name: Manage Servers farms
  ansible714.ovh.iploadbalancer_farm:
    lb_name: '{{ ansible_host }}'
    balancing_algorithm: '{{ item.balancing_algorithm | default(omit) }}'
    farm_id: '{{ item.farm_id | default(omit) }}'
    farm_type: '{{ item.farm_type }}'
    name: '{{ item.name }}'
    port: '{{ item.port | default(omit) }}'
    probe: '{{ item.probe | default(omit) }}'
    state: '{{ item.state | default("present") }}'
    stickiness: '{{ item.stickiness | default(omit) }}'
    vrackNetworkId: '{{ item.vrackNetworkId | default(omit) }}'
    zone: '{{ item.zone }}'
    ovh_api_endpoint: '{{ ovh_api_endpoint }}'
    ovh_api_application_key: '{{ ovh_api_application_key }}'
    ovh_api_application_secret: '{{ ovh_api_application_secret }}'
    ovh_api_consumer_key: '{{ ovh_api_consumer_key }}'
  with_items: '{{ farms }}'
  delegate_to: localhost
  become: no
  register: iplbfarms

"""

RETURN = """
# TODO
"""


from ansible_collections.ansible714.ovh.plugins.module_utils.ovh_client_module import (  # noqa: E402,E501
    OvhClientModule,
)
from ansible_collections.ansible714.ovh.plugins.module_utils.iploadbalancer import (  # noqa: E402,E501
    get_iplb_info,
    get_farm_byid,
)


def run_module():
    module = OvhClientModule(
        argument_spec=dict(
            lb_name=dict(required=True, type="str"),
            balancing_algorithm=dict(
                required=False,
                type="str",
                choices=("first", "uri", "leastconn", "roundrobin", "source"),
                default="roundrobin",
            ),
            farm_id=dict(required=False, type="int"),
            farm_type=dict(required=True, type="str", choices=("http", "tcp", "udp")),
            name=dict(type="str", required=False, default=None),
            port=dict(type="int", required=False, default=None),
            probe=dict(type="dict", required=False, default=None),
            state=dict(type="str", choices=["present", "absent"], default="present"),
            stickiness=dict(
                type="str", required=False, choices=("cookie", "sourceIp"), default=None
            ),
            vrackNetworkId=dict(type="str", required=False, default=None),
            zone=dict(type="str", required=True),
        ),
        required_by={},
    )

    # Get parameters
    balancing_algorithm = module.params.get("balancing_algorithm")
    farm_id = module.params.get("farm_id")
    farm_type = module.params.get("farm_type")
    lb_name = module.params.get("lb_name")
    name = module.params.get("name")
    port = module.params.get("port")
    probe = module.params.get("probe")
    state = module.params.get("state")
    stickiness = module.params.get("stickiness")
    zone = module.params.get("zone")

    request_args = {
        "balance": balancing_algorithm,
        "displayName": name,
        "port": port,
        "probe": probe,
        "stickiness": stickiness,
        "zone": zone,
    }
    # ------------------
    # Adapt arguments to farm_type
    if farm_type == "udp":
        request_args.pop("balancing_algorithm")
        request_args.pop("probe")
        request_args.pop("stickiness")

    vracknetwork = None
    # get Load-Balancer info, to know if attached to Vrack
    loadBalancerData = get_iplb_info(module)
    if loadBalancerData and loadBalancerData["vrackEligibility"]:
        vracknetwork = module.params.get("vrackNetworkId")
        request_args["vrackNetworkId"] = vracknetwork
        if vracknetwork is None:
            module.fail_json(
                msg="Error, missing parameter: vrackNetworkId "
                "is mandatory for Load Balancer attached to Vrack"
            )

    # Import exceptions (inside module to avoid earlier errors if ovh module is not present)
    from ovh.exceptions import (
        APIError,
    )

    # ------------------
    # Look for an existing farm (using farm_id if provided or name)
    existing_farm = None
    if farm_id:
        # getting farm data from farm_id
        existing_farm = get_farm_byid(module, lb_name, farm_id, farm_type)
    else:
        # getting farm data by name to enforce name uniqueness
        farms_list = []
        try:
            farms_list = module.client.get(
                "/ipLoadbalancing/{0}/{1}/farm".format(lb_name, farm_type),
                vrackNetworkId=vracknetwork,
                zone=zone,
            )
        except APIError as error:
            module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))
        for afarmid in farms_list:
            try:
                existing_farm = module.client.get(
                    "/ipLoadbalancing/{0}/{1}/farm/{2}".format(lb_name, farm_type, afarmid),
                    vrackNetworkId=vracknetwork,
                    zone=zone,
                )
            except APIError as error:
                module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))
            if existing_farm["displayName"] == name:
                break
            existing_farm = None

    # ------------------
    # Depending on state value, process data
    if state == "present":
        if existing_farm is None:
            # create Farm
            try:
                result = module.client.post(
                    "/ipLoadbalancing/{0}/{1}/farm".format(lb_name, farm_type), **request_args
                )
                module.exit_json(changed=True, msg="Servers farm has been created", **result)
            except APIError as error:
                module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))
        else:
            # should update Farm
            module.client.put(
                "/ipLoadbalancing/{0}/{1}/farm/{2}".format(
                    lb_name, farm_type, existing_farm["farmId"]
                ),
                **request_args,
            )
            module.exit_json(changed=True, msg="Servers farm has been updated")
    else:
        if existing_farm is None:
            module.exit_json(changed=False, msg="Farm already deleted {0}".format(name))
        else:
            # delete Farm
            try:
                module.client.delete(
                    "/ipLoadbalancing/{0}/{1}/farm/{2}".format(
                        lb_name, farm_type, existing_farm["farmId"]
                    ),
                )
                module.exit_json(changed=True, msg="Servers farm has been deleted")
            except APIError as error:
                module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

    module.exit_json(changed=False, msg="Nothing has been done")


def main():
    run_module()


if __name__ == "__main__":
    main()
