#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2022

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

from __future__ import absolute_import, division, print_function


__metaclass__ = type


ANSIBLE_METADATA = {"metadata_version": "1.1", "status": ["preview"], "supported_by": "community"}


DOCUMENTATION = r"""
---
module: vrack_info
short_description: Provide information about OVH Vrack
description:
  - Provide information about OVHCloud vrack instances
author:
  - Christophe Guychard (@xtof-osd)
requirements:
  - "ovh >=  1.0.0"
options:
  name:
    required: true
    description:
      - Name of the vrack
    type: str

extends_documentation_fragment:
  - ansible714.ovh.ovh_client_module

"""

EXAMPLES = """
- name: Gets info about my-vrack
  ansible714.ovh.vrack_info:
    name: my-vrack
    ovh_api_endpoint: '{{ ovh_api_endpoint }}'
    ovh_api_application_key: '{{ ovh_api_application_key }}'
    ovh_api_application_secret: '{{ ovh_api_application_secret }}'
    ovh_api_consumer_key: '{{ ovh_api_consumer_key }}'

"""

RETURN = """
# JSON description of the Vrack, according to OVH-Api schema.
# (https://eu.api.ovh.com/console/#/vrack/{serviceName}#GET)
"""


from ansible_collections.ansible714.ovh.plugins.module_utils.ovh_client_module import (  # noqa: E402,E501
    OvhClientModule,
)


def run_module():
    module = OvhClientModule(
        argument_spec=dict(
            name=dict(required=True),
        ),
        supports_check_mode=True,
        required_by={},
    )

    if module.client is None:
        module.fail_json(msg="Unable to init OVH api Client")

    # Get parameters
    name = module.params.get(
        "name",
    )

    from ovh.exceptions import (
        APIError,
    )

    # Check that the ip-load-balancer exists
    try:
        vracks = module.client.get("/vrack")
    except APIError as error:
        module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

    if name not in vracks:
        module.fail_json(msg="Vrack {0} does not exist".format(name))
    else:
        vrackData = module.client.get("/vrack/{0}".format(name))
        module.exit_json(changed=False, **vrackData)

    module.exit_json(changed=False)


def main():
    run_module()


if __name__ == "__main__":
    main()
