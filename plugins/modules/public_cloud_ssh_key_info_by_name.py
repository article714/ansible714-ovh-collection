#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2023

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

from __future__ import absolute_import, division, print_function

__metaclass__ = type


DOCUMENTATION = r"""
---
module: public_cloud_ssh_key_info_by_name
short_description: Get OVH Public cloud project SSH Key information, using its name
description:
    - This module retrieves SSH Key information from Public Cloud Project using a human
      readable ssh name (name)
author:
  - Christophe Guychard (@xtof-osd)
requirements:
    - ovh >= 1.0.0
options:
    project_id:
        required: true
        description:
            - The ID of project
        type: str
    name:
        required: true
        description:
            - The SSH Key humane-readable name
        type: str

extends_documentation_fragment:
  - ansible714.ovh.ovh_client_module
"""

EXAMPLES = """
- name: "Get info on OVH public cloud SSH key"
  ansible714.ovh.public_cloud_ssh_key_info_by_name:
    project_id: "{{ id_project }}"
    name: "{{ ssh_key_name }}"
"""

RETURN = """
sshkey:
  description: Dictionary holding properties of ssh key.
  returned: success
  type: dict
  contains:
    id:
      type: str
      description: Description of your project
    name:
      type: str
      description: Human readable name for the key
    publicKey:
      type: str
      description: The full public ssh public key
    regions:
      type: list
      elements: str
      description: List of regions for which key is valid
"""


from ansible_collections.ansible714.ovh.plugins.module_utils.ovh_client_module import (  # noqa: E402,E501
    OvhClientModule,
)

try:
    from ovh.exceptions import APIError

except ImportError:
    pass


def run_module():
    module = OvhClientModule(
        argument_spec=dict(
            name=dict(type="str", required=True),
            project_id=dict(type="str", required=True),
        ),
        required_by={},
    )

    name = module.params["name"]
    project_id = module.params["project_id"]

    key_ssh_list = []
    try:
        key_ssh_list = module.client.get("/cloud/project/{0}/sshkey".format(project_id))
    except APIError as error:
        module.fail_json(msg="Error getting ssh key list: {0}".format(error))

    for ssh_data in key_ssh_list:
        if ssh_data["name"] == name:
            module.exit_json(changed=False, **ssh_data)

    module.fail_json(
        msg="Error: could not find given SSH Key name {0} in {1}".format(name, key_ssh_list)
    )


def main():
    run_module()


if __name__ == "__main__":
    main()
