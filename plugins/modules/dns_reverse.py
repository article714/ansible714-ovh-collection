#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2023

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

from __future__ import absolute_import, division, print_function


__metaclass__ = type


ANSIBLE_METADATA = {"metadata_version": "1.1", "status": ["preview"], "supported_by": "community"}


DOCUMENTATION = r"""
---
module: dns_reverse
short_description: Manage DNS reverse records.
description: Manage DNS reverse records.
author:
  - Christophe Guychard (@xtof-osd)
requirements:
  - ovh >= 1.0.0
options:
  ip_block:
    required: false
    description: The ip block containing the reverse-ip to set
    type: str
  ip_reverse:
    required: true
    description: The ip  for which to setup reverse record
    type: str
  reverse_name:
    required: true
    description: The DNS record to be assigned
    type: str
  state:
    required: false
    description: Indicate the desired state of the record
    choices:
      - present
      - absent
    default: present
    type: str

extends_documentation_fragment:
  - ansible714.ovh.ovh_client_module

"""

EXAMPLES = """
ansible714.ovh.dns_reverse:
  ip_reverse: 10.0.0.2
  reverse_name: my.private.name.com.
  state: present
delegate_to: localhost

"""

RETURN = """ # Nothing to return except operation result """

from ansible_collections.ansible714.ovh.plugins.module_utils.ovh_client_module import (  # noqa: E402,E501
    OvhClientModule,
)


def run_module():
    module = OvhClientModule(
        dict(
            ip_block=dict(required=False, type="str"),
            ip_reverse=dict(required=True, type="str"),
            reverse_name=dict(required=True, type="str"),
            state=dict(type="str", choices=["present", "absent"], default="present"),
        ),
        supports_check_mode=True,
        required_by={},
    )

    # Get Parameters
    ip_block = module.params["ip_block"]
    ip_reverse = module.params["ip_reverse"]
    ip_block = ip_reverse if not ip_block else ip_block
    reverse_name = module.params["reverse_name"]
    state = module.params["state"]

    if module.check_mode:
        module.exit_json(
            msg=f"DNS Reverse record: {ip_reverse} ({ip_block}) to "
            f"{reverse_name} set - (dry run mode)",
            changed=True,
        )

    # Import exceptions (inside module to avoid earlier errors if ovh module is not present)
    from ovh.exceptions import APIError, ResourceNotFoundError

    try:
        existing_reverse = module.client.get(f"/ip/{ip_block}/reverse/{ip_reverse}")
    except ResourceNotFoundError:
        existing_reverse = {}
    except APIError as error:
        module.fail_json(msg=f"Failed to execute OVH-Api call : {error}")

    if existing_reverse:
        if state == "present":
            # update (delete + re-create) if needed
            if existing_reverse.get("reverse") == reverse_name:
                try:
                    module.client.delete(
                        f"/ip/{ip_block}/reverse{ip_reverse}",
                        ipReverse=ip_reverse,
                        reverse=reverse_name,
                    )
                    module.client.post(
                        f"/ip/{ip_block}/reverse", ipReverse=ip_reverse, reverse=reverse_name
                    )
                    module.exit_json(
                        msg=f"DNS Reverse record {ip_reverse} ({ip_block}) "
                        f"to {reverse_name} succesfully updated.",
                        changed=True,
                    )
                except APIError as error:
                    module.fail_json(msg=f"Failed to execute OVH-Api call : {error}")

            else:
                module.exit_json(
                    msg=f"No need to change reverse record {reverse_name} -> {ip_reverse}",
                    changed=False,
                )
        else:
            # delete reverse record
            try:
                module.client.delete(
                    f"/ip/{ip_block}/reverse{ip_reverse}",
                    ipReverse=ip_reverse,
                    reverse=reverse_name,
                )
                module.exit_json(
                    msg=f"DNS Reverse record {ip_reverse} ({ip_block}) "
                    f"to {reverse_name} succesfully deleted.",
                    changed=True,
                )
            except APIError as error:
                module.fail_json(msg=f"Failed to execute OVH-Api call : {error}")
    else:
        if state == "present":
            # create record
            try:
                module.client.post(
                    f"/ip/{ip_block}/reverse", ipReverse=ip_reverse, reverse=reverse_name
                )
                module.exit_json(
                    msg=f"DNS Reverse record {ip_reverse} ({ip_block}) "
                    f"to {reverse_name} succesfully set.",
                    changed=True,
                )
            except APIError as error:
                module.fail_json(msg=f"Failed to execute OVH-Api call : {error}")

        else:
            # Do nothing
            module.exit_json(
                msg=f"DNS Reverse record {ip_reverse} ({ip_block}) "
                f"to {reverse_name} already deleted.",
                changed=False,
            )


def main():
    run_module()


if __name__ == "__main__":
    main()
