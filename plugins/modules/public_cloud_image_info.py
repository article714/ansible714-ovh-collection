#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2022

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

from __future__ import absolute_import, division, print_function


__metaclass__ = type


ANSIBLE_METADATA = {"metadata_version": "1.1", "status": ["preview"], "supported_by": "community"}


DOCUMENTATION = r"""
---
module: public_cloud_image_info
short_description: Image data from name
description:
  - Get information about an image or a snapshot in a particular region.
  - Module provides detailed info about image/snapshot.
author:
  - Christophe Guychard (@xtof-osd)

requirements:
  - "python >= 3.6"
  - "ovh >=  1.0.0"
options:
  name:
    required: true
    description: The human readable image name, e.g. "Ubuntu 22.04"
    type: str
  region:
    required: true
    description: |
      Region from which you will use given image

      Region is mandatory as flavor id is different for each region.
    type: str
  project_id:
    required: true
    description:
      - The ID of project
    type: str

extends_documentation_fragment:
  - ansible714.ovh.ovh_client_module


"""

EXAMPLES = """
- name: Get Information about Ubuntu 22.10 Image
  ansible714.ovh.public_cloud_image_info:
    project_id: "{{ project_id }}"
    region: "GRA7"
    name: "Ubuntu 22.10"
    ovh_api_endpoint: '{{ ovh_api_endpoint }}'
    ovh_api_application_key: '{{ ovh_api_application_key }}'
    ovh_api_application_secret: '{{ ovh_api_application_secret }}'
    ovh_api_consumer_key: '{{ ovh_api_consumer_key }}'
  delegate_to: localhost
  register: image_data
"""

RETURN = """
ImageDescription:
  type: dict
  returned: success
  description: Detailed information about image or snapshot
  contains:
    creationDate:
      type: str
      description: Image creation date
    flavorType:
      type: str
      description: Image usable only for this type of flavor if not null
    id:
      type: str
      description: Image id
    minDisk:
      type: int
      description: Minimum disks required to use image
    minRam:
      type: int
      description: Minimum RAM required to use image
    name:
      type: str
      description: Image name
    planCode:
      type: str
      description: Order plan code
    region:
      type: str
      description: Image region
    size:
      type: int
      description: Image size (in GiB)
    status:
      type: str
      description: Image status
    tags:
      type: list
      elements: str
      description: Tags about the image
    type:
      type: str
      description: Image type
    user:
      type: str
      description: User to connect with
    visibility:
      type: str
      description: Image visibility

        """


from ansible_collections.ansible714.ovh.plugins.module_utils.ovh_client_module import (  # noqa: E402,E501
    OvhClientModule,
)


def run_module():
    module = OvhClientModule(
        argument_spec=dict(
            project_id=dict(type="str", required=True),
            name=dict(type="str", required=True),
            region=dict(type="str", required=True),
        ),
        supports_check_mode=True,
        required_by={},
    )

    # Get parameters
    project_id = module.params["project_id"]
    name = module.params["name"]
    region = module.params["region"]

    # Import exceptions (inside module to avoid earlier errors if ovh module is not present)
    from ovh.exceptions import (
        APIError,
    )

    try:
        images = []
        if region:
            images = module.client.get(
                "/cloud/project/{0}/image".format(project_id), region=region
            )
        else:
            images = module.client.get("/cloud/project/{0}/image".format(project_id))

    except APIError as error:
        module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

    try:
        snapshots = []
        if region:
            snapshots = module.client.get(
                "/cloud/project/{0}/snapshot".format(project_id), region=region
            )
        else:
            snapshots = module.client.get("/cloud/project/{0}/snapshot".format(project_id))
    except APIError as error:
        module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

    # returns first result found in images or snapshots
    for image_data in images + snapshots:
        if image_data["name"] == name:
            module.exit_json(changed=False, **image_data)

    module.fail_json(msg="Image/snapshot {0} not found in {1}".format(name, region), changed=False)


def main():
    run_module()


if __name__ == "__main__":
    main()
