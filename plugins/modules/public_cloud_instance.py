#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2023

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

from __future__ import absolute_import, division, print_function


__metaclass__ = type


ANSIBLE_METADATA = {"metadata_version": "1.1", "status": ["preview"], "supported_by": "community"}


DOCUMENTATION = r"""
---
module: public_cloud_instance
short_description: Manages OVH Public Cloud instance lifecycle
description:
  - This module enables to create or delete PCIs on OVH-Cloud infrastructure
  - Update is currently limited by OVH API
author:
  - Christophe Guychard (@xtof-osd)
  - Mathieu Piriou (@PiMath22)
requirements:
  - python >= 3.6
  - ovh >= 1.0.0
options:
  project_id:
    required: true
    type: str
    description:
      - The ID of public cloud project
  name:
    required: true
    type: str
    description:
      - Name of The instance
  flavor_id:
    required: true
    type: str
    description:
      - Id of the kind (flavor) of instance to create (e.g. id of b2-15)
  region:
    required: true
    type: str
    description:
      - The region where the instance is/will be hosted
  group_id:
    required: false
    type: str
    description:
      - Id of the kind (flavor) of instance to create (e.g. id of b2-15)
  image_id:
    required: true
    type: str
    description:
      - Id of the image to install on the instance
  monthly_billing:
    required: false
    default: false
    type: bool
    description:
      - Enable or not the monthly billing
  networks:
    required: false
    type: list
    description:
      - "The network(s) configuration: one entry per interface to configure."
    elements: dict
  ssh_key_id:
    required: false
    type: str
    description:
      - If of the ssh key to be added to instance (used to connect)
  user_data:
    required: false
    type: str
    description:
      - Additional data to document Instance
  state:
    required: false
    description: Indicate the desired state of the Farm
    choices:
      - present
      - absent
    default: present
    type: str

extends_documentation_fragment:
  - ansible714.ovh.ovh_client_module

"""

EXAMPLES = """
- name: Manages an OVH Public Cloud Instance
  ansible714.ovh.public_cloud_instance:
    ovh_api_endpoint: "{{ ovh_api_endpoint }}"
    ovh_api_application_key: "{{ ovh_api_application_key }}"
    ovh_api_application_secret: "{{ ovh_api_application_secret }}"
    ovh_api_consumer_key: "{{ ovh_api_consumer_key }}"
    name: MyInstance
    region: GRA-11
    flavor_id: "{{ d2_7_flavor_id }}"
    state: present
    group_id: "{{ my_group_id }}"
    ssh_key_id: "{{ sshKeyId }}"
    project_id: "{{ project_id }}"
    image_id: "{{ image_id }}"
    user_data: "Some data that I wan't to keep here"
  delegate_to: localhost
"""

RETURN = """
InstanceDetail:
  description: Dictionary for Ovh-Cloud related facts.
  returned: success
  type: dict
  contains:
    created:
      description: Instance creation date
      type: str
    currentMonthOutgoingTraffic:
      description: Instance outgoing network traffic for the current month (in bytes)
      type: int
    flavor:
      description: Instance flavor
      type: dict
    id:
      description: Instance id
      type: str
    image:
      description: Instance image
      type: dict
    ipAddresses:
      description: Instance IP addresses
      type: list
      elements: str
    monthlyBilling:
      description: Instance monthly billing status
      type: dict
    name:
      description: Instance name
      type: str
    operationIds:
      description: Ids of pending public cloud operations
      type: list
      elements: str
    planCode:
      description: Order plan code
      type: str
    region:
      description: Instance id
      type: str
    rescuePassword:
      description: Rescue password if instance is in RESCUE status
      type: str
    sshKey:
      description: Instance SSH key
      type: dict
    status:
      description: Instance status
      type: str
"""

from ansible_collections.ansible714.ovh.plugins.module_utils.ovh_client_module import (  # noqa: E402,E501
    OvhClientModule,
)
from ansible_collections.ansible714.ovh.plugins.module_utils.public_cloud_instance import (  # noqa: E402,E501
    find_pci,
)


def run_module():
    module = OvhClientModule(
        argument_spec=dict(
            dict(
                project_id=dict(type="str", required=True),
                name=dict(type="str", required=True),
                flavor_id=dict(type="str", required=True),
                region=dict(type="str", required=True),
                group_id=dict(type="str", required=False),
                image_id=dict(type="str", required=True),
                monthly_billing=dict(required=False, default=False, type="bool"),
                networks=dict(required=False, default=[], type="list", elements="dict"),
                ssh_key_id=dict(type="str", required=False, default=None),
                user_data=dict(type="str", required=False, default=None),
                state=dict(type="str", choices=["present", "absent"], default="present"),
            )
        ),
        required_by={},
    )

    # Get parameters
    project_id = module.params["project_id"]
    name = module.params["name"]
    flavor_id = module.params["flavor_id"]
    region = module.params["region"]
    group_id = module.params["group_id"]
    image_id = module.params["image_id"]
    monthly_billing = module.params["monthly_billing"]
    networks = module.params["networks"]
    ssh_key_id = module.params["ssh_key_id"]
    state = module.params["state"]
    user_data = module.params["user_data"]

    # Look for existing instance:
    instancedata = find_pci(module, bynameonly=True)

    # Import exceptions (inside module to avoid earlier errors if ovh module is not present)
    from ovh.exceptions import (
        APIError,
    )

    if state == "present":
        if instancedata is None:
            # creates instance if it does not exist
            try:
                request_args = {
                    "flavorId": flavor_id,
                    "name": name,
                    "region": region,
                    "monthlyBilling": monthly_billing,
                }
                if group_id:
                    request_args["groupId"] = group_id
                if image_id:
                    request_args["imageId"] = image_id
                if networks:
                    request_args["networks"] = networks
                if ssh_key_id:
                    request_args["sshKeyId"] = ssh_key_id
                if user_data:
                    request_args["userData"] = user_data

                result = module.client.post(
                    "/cloud/project/{0}/instance".format(project_id), **request_args
                )

                module.exit_json(changed=True, **result)
            except APIError as error:
                module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

        module.exit_json(changed=True, **instancedata)

    else:
        if instancedata:
            # deletes instance as it exists
            try:
                result = module.client.delete(
                    "/cloud/project/{0}/instance/{1}".format(project_id, instancedata["id"])
                )

                module.exit_json(changed=True, **result)
            except APIError as error:
                module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

    module.exit_json(changed=False, msg="Did nothing")


def main():
    run_module()


if __name__ == "__main__":
    main()
