#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2023

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)


from __future__ import absolute_import, division, print_function


__metaclass__ = type


ANSIBLE_METADATA = {"metadata_version": "1.1", "status": ["preview"], "supported_by": "community"}


DOCUMENTATION = r"""
---
module: public_cloud_instance_facts
short_description: Retrieve all info for a OVH public cloud instance
description:
  - This module register facts about an OVH public cloud instance.
  - You should provide either name or instance_id
author:
  - Christophe Guychard (@xtof-osd)

requirements:
  - "python >= 3.6"
  - "ovh >=  1.0.0"
options:
  project_id:
    required: true
    description:
      - The ID of project
    type: str
  instance_id:
    required: false
    description: The instance uuid
    type: str
  name:
    required: false
    description: The instance human-readable name
    type: str
  region:
    required: false
    description: The region in which to look for instance
    type: str

extends_documentation_fragment:
  - ansible714.ovh.ovh_client_module

"""

EXAMPLES = """
ansible714.ovh.public_cloud_instance_facts:
  instance_id: "{{ instance_id }}"
  project_id: "{{ project_id }}"
  ovh_api_endpoint: "{{ ovh_api_endpoint }}"
  ovh_api_application_key: "{{ ovh_api_application_key }}"
  ovh_api_application_secret: "{{ ovh_api_application_secret }}"
  ovh_api_consumer_key: "{{ ovh_api_consumer_key }}"
delegate_to: localhost
"""


RETURN = """
# Add some facts inside each PCI space
# InstanceDetail stored under facts[name][ovhcloud]:
InstanceDetail:
  description: Dictionary for Ovh-Cloud related facts.
  returned: success
  type: dict
  contains:
    created:
      description: Instance creation date
      type: str
    currentMonthOutgoingTraffic:
      description: Instance outgoing network traffic for the current month (in bytes)
      type: int
    flavor:
      description: Instance flavor
      type: dict
    id:
      description: Instance id
      type: str
    image:
      description: Instance image
      type: dict
    ipAddresses:
      description: Instance IP addresses
      type: list
      elements: str
    monthlyBilling:
      description: Instance monthly billing status
      type: dict
    name:
      description: Instance name
      type: str
    operationIds:
      description: Ids of pending public cloud operations
      type: list
      elements: str
    planCode:
      description: Order plan code
      type: str
    region:
      description: Instance id
      type: str
    rescuePassword:
      description: Rescue password if instance is in RESCUE status
      type: str
    sshKey:
      description: Instance SSH key
      type: dict
    status:
      description: Instance status
      type: str
"""

from ansible_collections.ansible714.ovh.plugins.module_utils.ovh_client_module import (  # noqa: E402,E501
    OvhClientModule,
)
from ansible_collections.ansible714.ovh.plugins.module_utils.public_cloud_instance import (  # noqa: E402,E501
    find_pci,
)


def run_module():
    module = OvhClientModule(
        argument_spec=dict(
            instance_id=dict(type="str", required=False),
            name=dict(type="str", required=False),
            project_id=dict(type="str", required=True),
            region=dict(type="str", required=False),
        ),
        mutually_exclusive=[
            ("instance_id", "name"),
        ],
        supports_check_mode=True,
        required_by={},
    )

    instancedata = find_pci(module)

    if instancedata is not None:
        module.exit_json(
            changed=False,
            ansible_facts={instancedata["name"]: {"ovhcloud": instancedata}},
        )

    module.fail_json(msg="Could not find Instance.")


def main():
    run_module()


if __name__ == "__main__":
    main()
