#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2022

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

from __future__ import absolute_import, division, print_function


__metaclass__ = type


ANSIBLE_METADATA = {"metadata_version": "1.1", "status": ["preview"], "supported_by": "community"}


DOCUMENTATION = r"""
---
module: iploadbalancer_vracknetwork
short_description: Manage Vrack Network for OVH IpLoadBalancer
description:
  - Manage Vrack Network for OVH IpLoadBalancer
author:
  - Christophe Guychard (@xtof-osd)
requirements:
  - "ovh >=  1.0.0"
options:
  lb_name:
    required: true
    description:
      - Name of the IpLoadBalancer
    type: str
  name:
    required: false
    description:
      - Human readable name for your backend
    type: str
  natIp:
    required: true
    description:
      - |
        An IP block used as a pool of IPs by this Load Balancer to connect to the servers in
        this private network. The block must be in the private network and reserved for the
        Load Balancer
    type: str
  subnet:
    required: true
    description:
      - IP Block of the private network in the vRack
    type: str
  vlan:
    required: false
    description:
      - VLAN of the private network in the vRack. 0 if the private network is not in a VLAN
    type: int
  balancing_algorithm:
    required: false
    description:
      - Load balancing algorithm.
    type: str
    choices:
      - "first"
      - "uri"
      - "leastconn"
      - "roundrobin"
      - "source"
    default: "roundrobin"
  state:
    required: false
    description: Indicate the desired state of the vrack network
    choices:
      - present
      - absent
    default: present
    type: str

extends_documentation_fragment:
  - ansible714.ovh.ovh_client_module

"""

EXAMPLES = """
# Example extracted from ip_load_balancer role
- name: Manage networks for vrack
  ansible714.ovh.iploadbalancer_vracknetwork:
    lb_name: '{{ ansible_host }}'
    natIp: '{{ item.natIp }}'
    subnet: '{{ item.subnet }}'
    vlan: '{{ item.vlan | default(0) }}'
    name: '{{ item.name }}'
    state: '{{ item.state }}'
    ovh_api_endpoint: '{{ ovh_api_endpoint }}'
    ovh_api_application_key: '{{ ovh_api_application_key }}'
    ovh_api_application_secret: '{{ ovh_api_application_secret }}'
    ovh_api_consumer_key: '{{ ovh_api_consumer_key }}'
  with_items: '{{ networks }}'
  when: iplb_vrack | default(false) and vrackEligibility
  delegate_to: localhost
  become: no
  register: vracknetworks
"""

RETURN = """
# TODO
"""


from ansible_collections.ansible714.ovh.plugins.module_utils.ovh_client_module import (  # noqa: E402,E501
    OvhClientModule,
)
from ansible_collections.ansible714.ovh.plugins.module_utils.iploadbalancer import (  # noqa: E402,E501
    get_vracknetwork_info,
)


def run_module():
    module = OvhClientModule(
        argument_spec=dict(
            lb_name=dict(required=True, type="str"),
            balancing_algorithm=dict(
                required=False,
                type="str",
                choices=("first", "uri", "leastconn", "roundrobin", "source"),
                default="roundrobin",
            ),
            name=dict(type="str", required=False, default=None),
            natIp=dict(type="str", required=True),
            subnet=dict(type="str", required=True),
            vlan=dict(type="int", required=False, default=0),
            state=dict(
                type="str", required=False, choices=["present", "absent"], default="present"
            ),
        ),
        required_by={},
    )

    # Get parameters
    lb_name = module.params.get("lb_name")
    name = module.params.get("name")
    subnet = module.params.get("subnet")
    natIp = module.params.get("natIp")
    vlan = int(module.params.get("vlan"))
    state = module.params.get("state")

    # Import exceptions (inside module to avoid earlier errors if ovh module is not present)
    from ovh.exceptions import (
        APIError,
    )

    # Check if network already exists:

    result = get_vracknetwork_info(module)

    if result is not None:
        if state == "present":
            # update network
            try:
                module.client.put(
                    "/ipLoadbalancing/{0}/vrack/network/{1}".format(
                        lb_name, result["vrackNetworkId"]
                    ),
                    displayName=name,
                    natIp=natIp,
                    vlan=vlan,
                )
                result["displayName"] = (name,)
                result["natIp"] = (natIp,)
                result["vlan"] = (vlan,)
                module.exit_json(changed=True, msg="Vrack network has been updated", **result)

            except APIError as error:
                module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

        else:
            try:
                # should delete Network, as state is "absent" and network exists
                result = module.client.delete(
                    "/ipLoadbalancing/{0}/vrack/network/{1}".format(
                        lb_name, result["vrackNetworkId"]
                    )
                )

                module.exit_json(changed=True, msg="Vrack network has been deleted ")

            except APIError as error:
                module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

    if state == "present":
        # creating network
        try:
            result = module.client.post(
                "/ipLoadbalancing/{0}/vrack/network".format(lb_name),
                displayName=name,
                farmId=None,
                natIp=natIp,
                subnet=subnet,
                vlan=vlan,
            )

            module.exit_json(changed=True, msg="Vrack network has been created", **result)

        except APIError as error:
            module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

    module.exit_json(changed=False, msg="Vrack network: nothing done")


def main():
    run_module()


if __name__ == "__main__":
    main()
