#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2023

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

from __future__ import absolute_import, division, print_function


__metaclass__ = type


ANSIBLE_METADATA = {"metadata_version": "1.1", "status": ["preview"], "supported_by": "community"}

DOCUMENTATION = r"""
---
module: private_network_subnet_ids
short_description: Obtain the private network and subnet ID of the OVH public cloud
description:
    - |
      This module allows you to retrieve the identifier of a private network
      as well as the id of a subnet
author:
    - Mathieu Piriou (@PiMath22)
requirements:
    - ovh >= 1.0.0
options:
    project_id:
        required: true
        description: The ID of project
        type: str
    private_subnet_cidr:
        required: true
        description: Global network with cidr
        type: str
    region:
        required: true
        description: The region where to deploy the instance
        type: str
    private_network_name:
        required: true
        description: The name of the private network
        type: str

extends_documentation_fragment:
  - ansible714.ovh.ovh_client_module

"""

EXAMPLES = """
- name: "Get ID on OVH public cloud subnet or private network"
  ansible714.ovh.private_network_subnet_ids:
    project_id: "{{ id_service }}"
    private_subnet_cidr: "{{  private_subnet_cidr }}"
    region: "{{ region }}"
    private_network_name: "{{ private_network_name }}"
    ovh_api_endpoint: '{{ ovh_api_endpoint }}'
    ovh_api_application_key: '{{ ovh_api_application_key }}'
    ovh_api_application_secret: '{{ ovh_api_application_secret }}'
    ovh_api_consumer_key: '{{ ovh_api_consumer_key }}'
"""

RETURN = """
# All private subnet information
"""

from ansible_collections.ansible714.ovh.plugins.module_utils.ovh_client_module import (  # noqa: E402,E501
    OvhClientModule,
)


def id_private_network(module, project_id, private_network_name, region):
    if module.client is None:
        module.fail_json(msg="Unable to init OVH api Client")

    from ovh.exceptions import (
        APIError,
    )

    private_network_list = []
    try:
        private_network_list = module.client.get(
            "/cloud/project/{0}/network/private".format(project_id)
        )
    except APIError as api_error:
        module.fail_json(msg="Error retrieving cluster list: {0}".format(api_error))

    if private_network_list:
        id_openstack_value = None
        id_private_network_value = None
        for private_network_data in private_network_list:
            if private_network_data["name"] == private_network_name:
                for region_data in private_network_data["regions"]:
                    if region_data["region"] == region:
                        id_openstack_value = region_data["openstackId"]
                        id_private_network_value = private_network_data["id"]
                        break
                else:
                    module.fail_json(
                        msg="Error: could not find region {0} in private network {1}".format(
                            region, private_network_name
                        )
                    )
                break
        else:
            module.fail_json(
                msg="Error: could not find given private network name {0}".format(
                    private_network_name
                )
            )
        if not id_private_network_value:
            module.fail_json(
                msg="Could not retrieve ID for private network {0}".format(private_network_name)
            )

        if not id_openstack_value:
            module.fail_json(
                msg="Could not retrieve OpenStack ID for private network {0} in region {1}".format(
                    private_network_name, region
                )
            )

        return id_private_network_value, id_openstack_value


def id_subnet_network(module, project_id, id_private_network_value, private_subnet_cidr, region):
    if module.client is None:
        module.fail_json(msg="Unable to init OVH api Client")

    from ovh.exceptions import (
        APIError,
    )

    private_subnet_list = []
    try:
        private_subnet_list = module.client.get(
            "/cloud/project/{0}/network/private/{1}/subnet".format(
                project_id, id_private_network_value
            )
        )
    except APIError as api_error:
        module.fail_json(msg="Error getting private subnets list : {0}".format(api_error))

    if private_subnet_list:
        id_private_subnet = None
        for private_subnet_data in private_subnet_list:
            for ip_pool in private_subnet_data["ipPools"]:
                if ip_pool["network"] == private_subnet_cidr and ip_pool["region"] == region:
                    id_private_subnet = private_subnet_data["id"]
                    break
            else:
                continue
            break
        else:
            module.fail_json(
                msg="Error: could not find private subnet with CIDR {0} in region {1}".format(
                    private_subnet_cidr, region
                )
            )

        if id_private_subnet is not None:
            return id_private_subnet
    else:
        module.fail_json(msg="Error: private_subnet_list is empty")

    module.fail_json(
        msg="Error: could not find given subnet network {0} in {1}".format(
            private_subnet_cidr, private_subnet_list
        )
    )


def run_module():
    module = OvhClientModule(
        argument_spec=dict(
            project_id=dict(type="str", required=True),
            private_subnet_cidr=dict(type="str", required=True),
            region=dict(type="str", required=True),
            private_network_name=dict(type="str", required=True),
        ),
        required_by={},
    )

    if module.client is None:
        module.fail_json(msg="Unable to init OVH api Client")

    private_subnet_cidr = module.params.get("private_subnet_cidr")
    project_id = module.params.get("project_id")
    region = module.params.get("region")
    private_network_name = module.params.get("private_network_name")

    from ovh.exceptions import (
        APIError,
    )

    try:
        id_private_network_value, id_openstack_value = id_private_network(
            module, project_id, private_network_name, region
        )
        id_private_subnet = id_subnet_network(
            module, project_id, id_private_network_value, private_subnet_cidr, region
        )
    except APIError as api_error:
        module.fail_json(msg="Error: {0}".format(api_error))

    result = {
        "id_private_network": id_private_network_value,
        "id_openstack_value": id_openstack_value,
        "id_private_subnet": id_private_subnet,
    }

    module.exit_json(changed=False, **result)


def main():
    run_module()


if __name__ == "__main__":
    main()
