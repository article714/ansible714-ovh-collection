#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2022

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

from __future__ import absolute_import, division, print_function


__metaclass__ = type


ANSIBLE_METADATA = {"metadata_version": "1.1", "status": ["preview"], "supported_by": "community"}


DOCUMENTATION = r"""
---
module: iploadbalancer_frontend
short_description: Manage Frontend for OVH IpLoadBalancer
description:
  - Manage Frontend for OVH IpLoadBalancer
author:
  - Christophe Guychard (@xtof-osd)
requirements:
  - "ovh >=  1.0.0"
options:
  lb_name:
    required: true
    description:
      - Name of the IpLoadBalancer
    type: str
  allowed_source:
    required: false
    description:
      - |
        Restrict IP Load Balancing access to these ip block.
        No restriction if null.
        You cannot specify allowedSource and deniedSource both at the same time
    type: list
    elements: str
  dedicated_ip_fo:
    required: false
    description:
      - Only attach frontend on these ip. No restriction if null
    type: str
  default_farm_id:
    required: false
    description:
      - Default HTTP Farm of your frontend
    type: int
  default_ssl_id:
    required: false
    description:
      - Default ssl served to your customer
    type: int
  denied_source:
    required: false
    description:
      - |
         Deny IP Load Balancing access to these ip block.
         No restriction if null. You cannot specify allowedSource
         and deniedSource both at the same time
    type: list
    elements: str
  disabled:
    required: false
    description:
      - Is the fronted disabled?
    type: bool
    default: false
  frontend_type:
    required: true
    description:
      - Type of the frontend, must be ("http","tcp","udp")
    choices:
      - http
      - tcp
      - udp
    type: str
  name:
    required: true
    description:
      - |
        Human readable name for your frontend, this field is for you
        This value must be unique for this module to work.
    type: str
  hsts:
    required: false
    description:
      - |
         HTTP Strict Transport Security. Default: 'false'
    type: bool
    default: false
  http_header:
    required: false
    description:
      - |
        Add header to your frontend.
        Useful variables admitted : %ci <=> client_ip, %cp <=> client_port
    type: list
    elements: str
  port:
    required: true
    description:
      - |
        Port(s) attached to your frontend.
        Supports single port (numerical value),
        range (2 dash-delimited increasing ports) and
        comma-separated list of 'single port' and/or 'range'.

        Each port must be in the [1;49151] range.
    type: str
  redirect_location:
    required: false
    description:
      - |
         HTTP redirection (Ex: http://www.ovh.com)
    type: str
  ssl:
    required: false
    description:
      - SSL deciphering.
    type: bool
    default: false
  state:
    required: false
    description: Indicate the desired state of the Frontend
    choices:
      - present
      - absent
    default: present
    type: str
  zone:
    required: false
    description:
      - Zone of your frontend. Use "all" for all owned zone.
    type: str
    default: all

extends_documentation_fragment:
  - ansible714.ovh.ovh_client_module

"""

EXAMPLES = """
- name: Create a new frontend
  ansible714.ovh.iploadbalancer_frontend:
    lb_name: my-load-balancer
    name: http-90
    frontend_type: http
    port: 90
    state: absent
    ovh_api_endpoint: '{{ ovh_api_endpoint }}'
    ovh_api_application_key: '{{ ovh_api_application_key }}'
    ovh_api_application_secret: '{{ ovh_api_application_secret }}'
    ovh_api_consumer_key: '{{ ovh_api_consumer_key }}'
"""

RETURN = """
# TODO
"""


from ansible_collections.ansible714.ovh.plugins.module_utils.ovh_client_module import (  # noqa: E402,E501
    OvhClientModule,
)


def run_module():
    module = OvhClientModule(
        argument_spec=dict(
            lb_name=dict(type="str", required=True),
            allowed_source=dict(type="list", elements="str", required=False),
            dedicated_ip_fo=dict(type="str", required=False),
            default_farm_id=dict(type="int", required=False),
            default_ssl_id=dict(type="int", required=False),
            denied_source=dict(type="list", elements="str", required=False),
            disabled=dict(type="bool", required=False, default=False),
            frontend_type=dict(type="str", required=True, choices=("http", "tcp", "udp")),
            name=dict(type="str", required=True),
            hsts=dict(type="bool", required=False, default=False),
            http_header=dict(type="list", elements="str", required=False),
            port=dict(type="str", required=True),
            redirect_location=dict(type="str", required=False),
            ssl=dict(type="bool", required=False, default=False),
            state=dict(type="str", choices=["present", "absent"], default="present"),
            zone=dict(type="str", required=False, default="all"),
        ),
        mutually_exclusive=[
            ("allowed_source", "denied_source"),
        ],
        required_by={},
    )

    # Get parameters
    lb_name = module.params.get("lb_name")
    allowed_source = module.params.get("allowed_source")
    dedicated_ip_fo = module.params.get("dedicated_ip_fo")
    default_farm_id = module.params.get("default_farm_id")
    default_ssl_id = module.params.get("default_ssl_id")
    denied_source = module.params.get("denied_source")
    disabled = module.params.get("disabled")
    frontend_type = module.params.get("frontend_type")
    name = module.params.get("name")
    hsts = module.params.get("hsts")
    http_header = module.params.get("http_header")
    port = module.params.get("port")
    redirect_location = module.params.get("redirect_location")
    ssl = module.params.get("ssl")
    state = module.params.get("state")
    zone = module.params.get("zone")

    request_args = {
        "allowedSource": allowed_source,
        "dedicatedIpfo": dedicated_ip_fo,
        "defaultFarmId": default_farm_id,
        "defaultSslId": default_ssl_id,
        "deniedSource": denied_source,
        "disabled": disabled,
        "displayName": name,
        "hsts": hsts,
        "httpHeader": http_header,
        "port": port,
        "redirectLocation": redirect_location,
        "ssl": ssl,
        "zone": zone,
    }

    # ------------------
    # Adapt arguments to frontend_type
    if frontend_type == "tcp":
        request_args.pop("hsts")
        request_args.pop("httpHeader")
        request_args.pop("redirectLocation")
    elif frontend_type == "udp":
        request_args.pop("allowedSource")
        request_args.pop("defaultSslId")
        request_args.pop("deniedSource")
        request_args.pop("hsts")
        request_args.pop("httpHeader")
        request_args.pop("redirectLocation")
        request_args.pop("ssl")

    # Import exceptions (inside module to avoid earlier errors if ovh module is not present)
    from ovh.exceptions import (
        APIError,
    )

    existing_frontend = None
    # ------------------
    # Look for an existing Frontend
    # getting Frontend data by port to enforce uniqueness
    frontends_list = []
    try:
        frontends_list = module.client.get(
            "/ipLoadbalancing/{0}/{1}/frontend".format(lb_name, frontend_type),
            port=port,
            zone=zone,
        )
    except APIError as error:
        module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))
    for afrontendid in frontends_list:
        try:
            existing_frontend = module.client.get(
                "/ipLoadbalancing/{0}/{1}/frontend/{2}".format(
                    lb_name, frontend_type, afrontendid
                ),
            )
        except APIError as error:
            module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))
        if existing_frontend["displayName"] == name:
            break
        existing_frontend = None

    # ------------------
    # Depending on state value, process data
    if state == "present":
        if existing_frontend is None:
            # create Frontend
            try:
                result = module.client.post(
                    "/ipLoadbalancing/{0}/{1}/frontend".format(lb_name, frontend_type),
                    **request_args,
                )
                module.exit_json(changed=True, msg="Frontend has been created", **result)
            except APIError as error:
                module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))
        else:
            # should update Frontend
            module.client.put(
                "/ipLoadbalancing/{0}/{1}/frontend/{2}".format(
                    lb_name, frontend_type, existing_frontend["frontendId"]
                ),
                **request_args,
            )
            module.exit_json(changed=True, msg="Frontend has been updated")
    else:
        if existing_frontend is None:
            module.exit_json(changed=False, msg="Frontend already deleted {0}".format(name))
        else:
            # delete Frontend
            try:
                module.client.delete(
                    "/ipLoadbalancing/{0}/{1}/frontend/{2}".format(
                        lb_name, frontend_type, existing_frontend["frontendId"]
                    ),
                )
                module.exit_json(changed=True, msg="Frontend has been deleted")
            except APIError as error:
                module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

    module.exit_json(changed=False, msg="Nothing has been done")


def main():
    run_module()


if __name__ == "__main__":
    main()
