#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2023

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)


from __future__ import absolute_import, division, print_function


__metaclass__ = type


ANSIBLE_METADATA = {"metadata_version": "1.1", "status": ["preview"], "supported_by": "community"}


DOCUMENTATION = r"""
---
module: public_cloud_instance_private_network
short_description: Manage OVH API for public cloud attach private_network
description:
  - This module manage the attach of an private_network on OVH instance public Cloud
author:
  - Christophe Guychard (@xtof-osd)
  - Mathieu Piriou (@PiMath22)
requirements:
  - python >= 3.6
  - ovh >= 1.0.0
options:
  project_id:
    required: true
    type: str
    description:
      - |
        The OVH API Public cloud project Id, that can be obtained using
        public_cloud_project_info module
  instance_id:
    required: true
    type: str
    description:
      - The instance id
  private_network_id:
    required: true
    type: str
    description:
      - The id of the private_network
  static_ip:
    required: false
    type: str
    description:
      - The static IP to set on new interface
  state:
    required: false
    default: present
    type: str
    choices: ["present", "absent"]
    description: Indicate the desired state of private_network

extends_documentation_fragment:
  - ansible714.ovh.ovh_client_module

"""

EXAMPLES = """
- name: "Attach a private_network to instance {{ instance_id }} on public cloud OVH"
  ansible714.ovh.public_cloud_instance_private_network:
    ovh_api_endpoint: "{{ ovh_api_endpoint }}"
    ovh_api_application_key: "{{ ovh_api_application_key }}"
    ovh_api_application_secret: "{{ ovh_api_application_secret }}"
    ovh_api_consumer_key: "{{ ovh_api_consumer_key }}"
    instance_id: "{{ instance_id }}"
    project_id: "{{ project_id }}"
    private_network_id: "{{ private_network_id }}"
    static_ip: "{{ static_ip }}"
    state: present
"""

RETURN = """ # """

from ansible_collections.ansible714.ovh.plugins.module_utils.ovh_client_module import (  # noqa: E402,E501
    OvhClientModule,
)


def run_module():
    module = OvhClientModule(
        argument_spec=dict(
            project_id=dict(required=True),
            state=dict(choices=["present", "absent"], default="present"),
            instance_id=dict(required=True),
            private_network_id=dict(required=True),
            static_ip=dict(required=False, default=None),
        ),
        required_by={},
    )

    # Get parameters
    project_id = module.params["project_id"]
    private_network_id = module.params["private_network_id"]
    static_ip = module.params["static_ip"]
    instance_id = module.params["instance_id"]
    state = module.params["state"]

    # Import exceptions (inside module to avoid earlier errors if ovh module is not present)
    from ovh.exceptions import (
        APIError,
    )

    if module.check_mode:
        module.exit_json(
            msg="{0}/{1} successfully configured ({2}) on  private_network {3} (dry run)".format(
                instance_id, project_id, state, private_network_id
            ),
            changed=True,
        )

    is_already_registered = False
    private_network_if = None

    # list existing interfaces
    try:
        interfaces_list = module.client.get(
            "/cloud/project/{0}/instance/{1}/interface".format(project_id, instance_id)
        )

        for netif in interfaces_list:
            if netif["networkId"] == private_network_id:
                is_already_registered = True
                private_network_if = netif

    except APIError as error:
        module.fail_json(msg="Failed to get interfaces list: {0}".format(error))

    # Attach or detach
    if state == "present":
        if not is_already_registered:
            try:
                if static_ip:
                    attach_result = module.client.post(
                        "/cloud/project/{0}/instance/{1}/interface".format(
                            project_id, instance_id
                        ),
                        networkId=private_network_id,
                        ip=static_ip,
                    )
                    module.exit_json(changed=True, **attach_result)
                else:
                    attach_result = module.client.post(
                        "/cloud/project/{0}/instance/{1}/interface".format(
                            project_id, instance_id
                        ),
                        networkId=private_network_id,
                    )
                    module.exit_json(changed=True, **attach_result)

                module.exit_json(
                    msg="private_network {0} interface has been added to instance {1}".format(
                        private_network_id, instance_id
                    ),
                    result=attach_result,
                    changed=True,
                )

            except APIError as error:
                module.fail_json(msg="Failed to call OVH API: {0}".format(error))

        module.exit_json(
            msg="private_network {0} interface already exists on instance {1}".format(
                private_network_id, instance_id
            ),
            changed=False,
        )

    else:
        if is_already_registered:
            try:
                detach_result = module.client.delete(
                    "/cloud/project/{0}/instance/{1}/interface/{2}".format(
                        project_id, instance_id, private_network_if["id"]
                    )
                )

                module.exit_json(
                    msg="private_network {0} interface has been deleted from instance {1}".format(
                        private_network_id, instance_id
                    ),
                    result=detach_result,
                    changed=True,
                )

            except APIError as error:
                module.fail_json(
                    msg="Failed to remove private_network interface: {0}".format(error)
                )

        module.exit_json(
            msg="private_network {0} interface does not exist on instance {1}".format(
                private_network_id, instance_id
            ),
            changed=False,
        )

    module.fail_json(msg="do not know how to deal with private_network information", changed=False)


def main():
    run_module()


if __name__ == "__main__":
    main()
