#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2022

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

from __future__ import absolute_import, division, print_function


__metaclass__ = type


ANSIBLE_METADATA = {"metadata_version": "1.1", "status": ["preview"], "supported_by": "community"}


DOCUMENTATION = r"""
---
module: dns
short_description: Manage DNS records
description:
    - CRUD operations on DNS records at OVH-Cloud
author:
  - Christophe Guychard (@xtof-osd)
requirements:
    - ovh >= 1.0.0
options:
    zone:
        required: true
        description: The zone to modify
        type: str
    record_value:
        required: true
        description: Value to be set for the record
        type: str
    record_name:
        required: true
        description: Name of the record to manage
        type: str
    record_type:
        required: false
        description: The record type
        default: A
        type: str
        choices:
            - A
            - AAAA
            - CAA
            - CNAME
            - DKIM
            - DMARC
            - DNAME
            - LOC
            - MX
            - NAPTR
            - NS
            - PTR
            - SPF
            - SRV
            - SSHFP
            - TLSA
            - TXT
    record_ttl:
        required: false
        default: 0
        description: Record's custom Time To live.
        type: int
    state:
        required: false
        description: Indicate the desired state of the record
        choices:
        - present
        - absent
        default: present
        type: str

extends_documentation_fragment:
  - ansible714.ovh.ovh_client_module

"""

EXAMPLES = """
- name: Creates entry for developement webserver
  ansible714.ovh.dns:
    zone: article714.org
    record_name: "www.dev"
    record_ttl: 800
    record_value: "10.10.1.5"
    state: "present"
"""

RETURN = """
# Processing status of the record (success/failure and change indicator)
"""

from ansible_collections.ansible714.ovh.plugins.module_utils.ovh_client_module import (  # noqa: E402,E501
    OvhClientModule,
)


def setup_module():
    """Initialize client module"""

    return OvhClientModule(
        argument_spec=dict(
            zone=dict(type="str", required=True),
            record_value=dict(type="str", required=True),
            record_name=dict(type="str", required=True),
            record_type=dict(
                type="str",
                required=False,
                choices=[
                    "A",
                    "AAAA",
                    "CAA",
                    "CNAME",
                    "DKIM",
                    "DMARC",
                    "DNAME",
                    "LOC",
                    "MX",
                    "NAPTR",
                    "NS",
                    "PTR",
                    "SPF",
                    "SRV",
                    "SSHFP",
                    "TLSA",
                    "TXT",
                ],
                default="A",
            ),
            record_ttl=dict(type="int", required=False, default=0),
            state=dict(type="str", choices=["present", "absent"], default="present"),
        ),
        supports_check_mode=True,
        required_by={},
    )


def run_module(module):
    """Module main function"""

    zone = module.params["zone"]
    record_value = module.params["record_value"]
    record_name = module.params["record_name"]
    record_type = module.params["record_type"]
    record_ttl = module.params["record_ttl"]
    state = module.params["state"]

    # Import exceptions (inside module to avoid earlier errors if ovh module is not present)
    from ovh.exceptions import (
        APIError,
    )

    # ---
    # Check-Mode support
    if module.check_mode:
        if state == "present":
            module.exit_json(
                msg=" In Zone {0} record {1} ({2}) will be set to {3} - (check mode)".format(
                    zone, record_name, record_type, record_value
                ),
                change=True,
            )
        else:
            module.exit_json(
                msg=" In Zone {0} record {1} ({2}) will be removed if exists (check mode)".format(
                    zone, record_name, record_value
                ),
                change=True,
            )

    # ------------------
    # Looking for existing record

    try:
        existing_records = module.client.get(
            "/domain/zone/{0}/record".format(zone), fieldType=record_type, subDomain=record_name
        )
    except APIError as error:
        module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

    existing_recordid = None
    for some_recordid in existing_records:
        try:
            record = module.client.get("/domain/zone/{0}/record/{1}".format(zone, some_recordid))
            if record["subDomain"] == record_name and record["fieldType"] == record_type:
                if existing_recordid is not None:
                    # TODO: manage name with several records
                    module.fail_json(
                        msg="Found more than one record for record_name {0} in zone {1}".format(
                            record_name, zone
                        )
                    )
                existing_recordid = some_recordid
        except APIError as error:
            module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

    # ------------------
    # Depending on state value, process data
    if state == "present":
        if existing_recordid is not None:
            try:
                if record["target"] != record_value:
                    module.client.put(
                        "/domain/zone/{0}/record/{1}".format(zone, existing_recordid),
                        subDomain=record_name,
                        target=record_value,
                        ttl=record_ttl,
                    )
                    # Refresh DNS zone
                    module.client.post("/domain/zone/{0}/refresh".format(zone))
                    module.exit_json(
                        msg="Zone <{0}>: record has been updated: {1}  {2}  {3}".format(
                            zone,
                            record_name,
                            record_type,
                            record_value,
                        ),
                        changed=True,
                    )
                else:
                    module.exit_json(
                        msg="zone <{0}>: record is up to date: {1}  {2}  {3}".format(
                            zone,
                            record_name,
                            record_type,
                            record_value,
                        ),
                        changed=False,
                    )

            except APIError as error:
                module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))
        else:
            # Create a new record
            try:
                module.client.post(
                    "/domain/zone/{0}/record".format(zone),
                    fieldType=record_type,
                    subDomain=record_name,
                    target=record_value,
                    ttl=record_ttl,
                )
                # Refresh DNS zone
                module.client.post("/domain/zone/{0}/refresh".format(zone))
                module.exit_json(
                    msg="Zone {0}, record updated {1} {2} {3}".format(
                        zone,
                        record_name,
                        record_type,
                        record_value,
                    ),
                    changed=True,
                )
            except APIError as error:
                module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

    elif state == "absent":
        if existing_recordid is None:
            module.exit_json(
                msg="Record {0} does not exist in zone {1}".format(record_name, zone),
                changed=False,
            )

        try:
            module.client.delete("/domain/zone/{0}/record/{1}".format(zone, existing_recordid))
            # Refresh DNS zone
            module.client.post("/domain/zone/{0}/refresh".format(zone))

            module.exit_json(
                msg="Zone {0}: record {1} has been deleted".format(zone, record_name), changed=True
            )
        except APIError as error:
            module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))


def main():
    mod = setup_module()
    run_module(mod)


if __name__ == "__main__":
    main()
