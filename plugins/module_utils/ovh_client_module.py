# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2022

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

from __future__ import absolute_import, division, print_function

__metaclass__ = type


from ansible.module_utils.basic import (
    AnsibleModule,
)

# List of parameters used by module to connect to OVH-Api
OVH_API_CONFIG_CONFIGURATION = dict(
    ovh_api_endpoint=dict(
        required=True,
        type="str",
    ),
    ovh_api_application_key=dict(
        required=True,
        type="str",
        no_log=True,
    ),
    ovh_api_application_secret=dict(
        required=True,
        type="str",
        no_log=True,
    ),
    ovh_api_consumer_key=dict(
        required=True,
        type="str",
        no_log=True,
    ),
)


class OvhClientModule(AnsibleModule):
    """
    An AnsibleModule providing a connection to OVH-Api
    using python-ovh (https://github.com/ovh/python-ovh)
    """

    def __init__(
        self,
        argument_spec,
        bypass_checks=False,
        no_log=False,
        check_invalid_arguments=None,
        mutually_exclusive=None,
        required_together=None,
        required_one_of=None,
        add_file_common_args=False,
        supports_check_mode=False,
        required_if=None,
        required_by=None,
    ):
        # Init module
        argument_spec.update(OVH_API_CONFIG_CONFIGURATION)

        super(OvhClientModule, self).__init__(
            argument_spec,
            bypass_checks=bypass_checks,
            no_log=no_log,
            mutually_exclusive=mutually_exclusive,
            required_together=required_together,
            required_one_of=required_one_of,
            add_file_common_args=add_file_common_args,
            supports_check_mode=supports_check_mode,
            required_if=required_if,
            required_by=required_by,
        )

        # Import OVH module or fail
        try:
            import ovh
            from ovh.exceptions import (
                APIError,
            )
        except ImportError:
            self.fail_json(msg="python-ovh must be installed to use this collection")

        # Init OVH Client
        self.client = None
        try:
            self.client = ovh.Client(
                endpoint=self.params.get("ovh_api_endpoint"),
                application_key=self.params.get("ovh_api_application_key"),
                application_secret=self.params.get("ovh_api_application_secret"),
                consumer_key=self.params.get("ovh_api_consumer_key"),
            )
        except APIError as error:
            self.fail_json(msg="Failed to connect to OVH-Api: {0}".format(error))
        except Exception as error:
            self.fail_json(msg="Module Failed to connect to init: {0}".format(error))
