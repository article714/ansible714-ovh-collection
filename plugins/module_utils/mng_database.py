# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2023

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)


from __future__ import absolute_import, division, print_function

__metaclass__ = type


ANSIBLE_METADATA = {"metadata_version": "1.1", "status": ["preview"], "supported_by": "community"}

# Supported DBs for this collection
# @TODO: improve that part to use /cloud/project/{project_id}/database/availability endpoint
AVAILABLE_DB_ENGINES = (
    "cassandra",
    "m3db",
    "mongodb",
    "opensearch",
    "postgresql",
    "grafana",
    "kafka",
    "mysql",
    "redis",
)


def get_db_engine_from_module(module):
    db_engine = module.params["db_engine"]

    if not db_engine:
        module.fail_json(msg="Database was not specified.")

    if db_engine not in AVAILABLE_DB_ENGINES:
        module.fail_json(
            msg=f"An error has occurred while validating the database engine: {db_engine}"
        )

    return db_engine


def get_cluster_ids_for_module(module):
    if module.client is None:
        module.fail_json(msg="Unable to init OVH api Client")

    from ovh.exceptions import (
        APIError,
    )

    db_engine = get_db_engine_from_module(module)

    try:
        return module.client.get(
            "/cloud/project/{0}/database/{1}".format(module.params["project_id"], db_engine)
        )
    except APIError as api_error:
        module.fail_json(msg="Error retrieving cluster list: {0}".format(api_error))


def get_cluster_id_from_module(module):
    if module.client is None:
        module.fail_json(msg="Unable to init OVH api Client")

    from ovh.exceptions import (
        APIError,
    )

    cluster_id = None
    for cluster_id in get_cluster_ids_for_module(module):
        try:
            response = module.client.get(
                "/cloud/project/{0}/database/{1}/{2}".format(
                    module.params["project_id"], module.params["db_engine"], cluster_id
                ),
            )
            if response["description"] == module.params["db_instance_name"]:
                break

        except APIError as api_error:
            module.fail_json(msg="Error getting cluster: {0}".format(api_error))

    return cluster_id


def get_node_number_from_module(module):
    plan = module.params["plan"]
    try:
        return {"essential": 1, "business": 3, "enterprise": 6}[plan]
    except KeyError:
        module.fail_json(msg="Plan '{0}' is not a supported plan".format(plan))


def get_ip_restrictions_from_module(module):
    ip_restrictions = module.params["ip_restrictions"]

    for aIP in ip_restrictions:
        if "description" not in aIP:
            module.fail_json(msg="Missing name on IP restriction : {0}".format(aIP))
        if "ip" not in aIP:
            module.fail_json(msg="Missing field on IP restriction : {0}".format(aIP))

    return ip_restrictions
