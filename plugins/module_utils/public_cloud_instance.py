# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2023

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

from __future__ import absolute_import, division, print_function

__metaclass__ = type


ANSIBLE_METADATA = {"metadata_version": "1.1", "status": ["preview"], "supported_by": "community"}


def find_pci(module, bynameonly=False):
    """Retrieves Public Cloud Instance information using OVH-Apis

    module parameter should be instance of OvhClientModule

    Warning: This function might put an end to module execution in case of error
    """

    # Get parameters
    if bynameonly:
        instance_id = None
    else:
        instance_id = module.params["instance_id"]
    name = module.params["name"]
    project_id = module.params["project_id"]
    region = module.params["region"]
    pciData = None

    if not (instance_id or name):
        module.fail_json(msg="Missing Parameter:  You should provide either name or instance_id")

    # Import exceptions (inside module to avoid earlier errors if ovh module is not present)
    from ovh.exceptions import (
        APIError,
    )

    if instance_id:
        try:
            pciData = module.client.get(
                "/cloud/project/{0}/instance/{1}".format(project_id, instance_id)
            )

        except APIError as error:
            module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))
    else:
        try:
            if region:
                instances_list = module.client.get(
                    "/cloud/project/{0}/instance".format(project_id), region=region
                )
            else:
                instances_list = module.client.get(
                    "/cloud/project/{0}/instance".format(project_id)
                )

        except APIError as error:
            module.fail_json(msg="Error getting instances list: {0}".format(error))

        for aninstance in instances_list:
            if aninstance["name"] == name:
                pciData = aninstance

    return pciData
