# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2022-2023

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

from __future__ import absolute_import, division, print_function

__metaclass__ = type


ANSIBLE_METADATA = {"metadata_version": "1.1", "status": ["preview"], "supported_by": "community"}


def get_iplb_info(module):
    """Retrieves Ip LoadBalancer information using OVH-Apis

    module parameter should be instance of OvhClientModule

    Warning: This function might put an end to module execution in case of error
    """

    loadBalancerData = False

    if module.client is None:
        module.fail_json(msg="Unable to init OVH api Client")

    if "lb_name" in module.params:
        name = module.params.get(
            "lb_name",
        )

    else:
        name = module.params.get(
            "name",
        )

    from ovh.exceptions import (
        APIError,
    )

    # Check that the ip-load-balancer exists
    try:
        loadBalancers = module.client.get("/ipLoadbalancing")
    except APIError as error:
        module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

    if name not in loadBalancers:
        module.fail_json(msg="Load-Balancer {0} does not exist".format(name))
    else:
        loadBalancerData = module.client.get("/ipLoadbalancing/{0}".format(name))

    return loadBalancerData


def get_vracknetwork_info(module):
    """Retrieves Vrack Network information using OVH-Apis

    module parameter should be instance of OvhClientModule

    Warning: This function might put an end to module execution in case of error
    """

    if module.client is None:
        module.fail_json(msg="Unable to init OVH api Client")

    lb_name = module.params.get(
        "lb_name",
    )

    subnet = module.params.get("subnet")
    vlan = int(module.params.get("vlan"))
    vrackNetworkData = None

    from ovh.exceptions import (
        APIError,
    )

    # Check if network already exists:

    try:
        result = module.client.get(
            "/ipLoadbalancing/{0}/vrack/network".format(lb_name),
            subnet=subnet,
            vlan=vlan,
        )
    except APIError as error:
        module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

    if result:
        vrackNetworkData = module.client.get(
            "/ipLoadbalancing/{0}/vrack/network/{1}".format(lb_name, result[0])
        )

    return vrackNetworkData


def get_farms_info(client, loadBalancer):
    """
    returns data about farms for a loadBalancer, as a dict
    """

    farms = []

    farmtypeids = client.get("/ipLoadbalancing/{0}/definedFarms".format(loadBalancer))
    for farm in farmtypeids:
        farmdata = client.get(
            "/ipLoadbalancing/{0}/{1}/farm/{2}".format(loadBalancer, farm["type"], farm["id"])
        )
        # rename farmId key
        farmdata["farm_id"] = farmdata.get("farmId")
        farmdata.pop("farmId")
        # Servers
        servers = []
        serverids = client.get(
            "/ipLoadbalancing/{0}/{1}/farm/{2}/server".format(
                loadBalancer, farm["type"], farm["id"]
            )
        )
        for server in serverids:
            serverdata = client.get(
                "/ipLoadbalancing/{0}/{1}/farm/{2}/server/{3}".format(
                    loadBalancer, farm["type"], farm["id"], server
                )
            )
            servers.append(serverdata)

        farms.append({"farm_type": farm["type"], **farmdata, "servers": servers})

    return farms


def get_frontends_info(client, loadBalancer):
    """
    returns data about frontends for a loadBalancer, as a dict
    """

    frontends = []
    frontendids = client.get("/ipLoadbalancing/{0}/definedFrontends".format(loadBalancer))
    for frontend in frontendids:
        frontendata = client.get(
            "/ipLoadbalancing/{0}/{1}/frontend/{2}".format(
                loadBalancer, frontend["type"], frontend["id"]
            )
        )
        # Routes
        routes = []
        routeids = client.get(
            "/ipLoadbalancing/{0}/{1}/route".format(loadBalancer, frontend["type"]),
            frontendId=frontend["id"],
        )
        for route in routeids:
            routedata = client.get(
                "/ipLoadbalancing/{0}/{1}/route/{2}".format(loadBalancer, frontend["type"], route)
            )
            # @TODO: add rules if needed
            routes.append(routedata)

        frontends.append({"type": frontend["type"], **frontendata, "routes": routes})

    return frontends


def get_networks_info(client, loadBalancer):
    """
    returns data about vrack networks
    """

    networks = []
    networkids = client.get("/ipLoadbalancing/{0}/vrack/network".format(loadBalancer))
    for networkid in networkids:
        networkdata = client.get(
            "/ipLoadbalancing/{0}/vrack/network/{1}".format(loadBalancer, networkid)
        )

        networks.append(networkdata)

    return networks


def get_farm_byid(module, lb_name, farm_id, farm_type):
    """Returns Farm information

    Args:
        module (OvhClientModule): AnsibleModule with OVH-Api support
        farm_id (int): Farm id from OVH
        farm_type (str): Kind of Farm
    """

    from ovh.exceptions import (
        APIError,
    )

    existing_farm = None
    if farm_id:
        # getting farm data from farm_id
        try:
            existing_farm = module.client.get(
                "/ipLoadbalancing/{0}/{1}/farm/{2}".format(lb_name, farm_type, farm_id)
            )
        except APIError as error:
            if error.response is not None and error.response.status_code == 404:
                existing_farm = None
            else:
                module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))
    return existing_farm


def find_ssl_certificate(
    module,
    lb_name,
    cert_fingerprint=None,
    cert_serial_number=None,
    fqdn=None,
    cert_name="unkown",
    is_free_cert=False,
):
    """Search for an existing certificate with provided data

    Args:
        module (OvhClientModule): AnsibleModule with OVH-Api support
        cert_fingerprint (str): Certificate Fingerprint
        cert_serial_number (str): Certificate Serial Number
        fqdn (str): Subject FQDN for the certificate
    """

    # Import exceptions (inside module to avoid earlier errors if ovh module is not present)
    from ovh.exceptions import (
        APIError,
    )

    existing_certid, existing_certinfo = (None, None)
    results = []

    try:
        if cert_fingerprint:
            results = module.client.get(
                "/ipLoadbalancing/{0}/ssl".format(lb_name),
                fingerprint=cert_fingerprint,
            )
        if not results and cert_serial_number:
            results = module.client.get(
                "/ipLoadbalancing/{0}/ssl".format(lb_name),
                serial=cert_serial_number,
            )
        if not results and is_free_cert:
            results = module.client.get(
                "/ipLoadbalancing/{0}/ssl".format(lb_name),
                type="built",
            )
        if not results:
            results = module.client.get("/ipLoadbalancing/{0}/ssl".format(lb_name))

        if results:
            for existing_certid in results:
                # get existing cert info
                existing_certinfo = module.client.get(
                    "/ipLoadbalancing/{0}/ssl/{1}".format(lb_name, existing_certid)
                )

                # check that we found the right certificate
                if fqdn:
                    if fqdn in existing_certinfo["san"]:
                        break
                    else:
                        existing_certid, existing_certinfo = (None, None)
                else:
                    module.fail_json(
                        msg="Cannot verify that certificate is matching FQDN: no fqdn provided"
                    )

            # check we are not overriding free/Custom cert
            if existing_certinfo:
                if (is_free_cert and existing_certinfo["type"] == "custom") or (
                    existing_certinfo["type"] in ("built", "built_not_routed") and not is_free_cert
                ):
                    module.fail_json(
                        msg="Conflict between Certificate types (required and existing)"
                        " for: {0} -> {1}, {2}".format(
                            cert_name, is_free_cert, existing_certinfo["type"]
                        )
                    )

    except APIError as error:
        if error.response is not None and error.response.status_code == 404:
            existing_certid = None
        module.fail_json(msg="Failed to execute OVH-Api call : {0}".format(error))

    return (existing_certid, existing_certinfo)
