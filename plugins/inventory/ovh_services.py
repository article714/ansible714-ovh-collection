# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2023

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

DOCUMENTATION = r"""
---
name: ovh_services
author:
  - Christophe Guychard (@xtof-osd)
  - Damien Couppé (@Kontrolix)
short_description: Dynamic inventory for OVH services, through OVH Apis
description:
  - Gather service lists from OVH-cloud APIs.
options:
  iploadbalancers:
    required: false
    default: true
    description:
      - |
        Include IP load-balancing projects in inventory
        All load-balancers will be members of OVHIPLoadBalancers group
    type: bool
  projects:
    required: false
    default: true
    description:
      - |
        Include projects in inventory, Projects are added as groups which names
        are built using project Id => "Project_<projectId>".
        This is necesary to avoid invalid group names
    type: bool
  only_projects:
    required: false
    default: []
    description:
      - |
        Only Include selected projects in inventory.
        This option implies that project option is true.
    type: list
    elements: str
  mk8s:
    required: false
    default: true
    description:
      - |
        Include Managed Kubernetes instances to inventory.
        All Managed Kubernetes clusters will members of group OVHManagedK8S.
  instances:
    required: false
    default: true
    description:
      - |
        Include Public cloud instances in inventory, if projects are added to inventory,
        instance are placed is related group/project.
        All load-balancers will be members of OVHPublicCloudInstances group if not part of a K8s
        cluster, and OVHMk8sNodes if part of a K8s cluster
    type: bool
  pci_hostname:
    required: false
    default: id
    description:
      - |
        Wich data field to uses as hostnname for Ansible hosts.
        Default is id, but could be name.

        If pci_hostname=name, you are responsible to make sure that you do not have 2 identical
        instance names.
    type: str
    choices:
      - name
      - id
  pci_ansible_host:
    required: false
    default: name
    description:
      - |
        Wich data field to uses as hostnname for Ansible hosts.
        Default is name, but could be public/private IP(v4/v6)
    choices:
      - name
      - publicIPv6
      - publicIPv4
      - privateIPv4
  ovh_api_endpoint:
    required: false
    description:
      - |
        Which OVH Api endpoint to use.
        Might be provided through environment variable: OVH_API_ENDPOINT
    type: str

  ovh_api_application_key:
    required: false
    description:
      - |
        Application Key obtained from OVH
        Might be provided through environment variable: OVH_API_APPLICATION_KEY
    type: str

  ovh_api_application_secret:
    required: false
    description:
      - |
        Application Secret obtained from OVH
        Might be provided through environment variable: OVH_API_APPLICATION_SECRET
    type: str

  ovh_api_consumer_key:
    required: false
    description:
      - |
        Consumer key, created via OVH-Api and configured with the rights
        to use all the needed endoints (the one that you will be using)

        Might be provided through environment variable: OVH_API_CONSUMER_KEY
    type: str


"""

EXAMPLES = r"""
# Refer to official documentation.
# Create a file called ovh_services.yaml:
plugin: ansible714.ovh.ovh_services
# to test your configuration, run
# $> ansible-inventory --list -vvv -i ovh_services.yaml
"""


from ansible.errors import AnsibleParserError  # noqa: E402,E501
from ansible.plugins.inventory import BaseInventoryPlugin  # noqa: E402,E501
import os  # noqa: E402,E501


from ansible_collections.ansible714.ovh.plugins.module_utils.iploadbalancer import (  # noqa: E402,E501
    get_farms_info,
    get_frontends_info,
    get_networks_info,
)


# Import OVH module or fail
try:
    import ovh
    from ovh.exceptions import (
        APIError,
    )
except ImportError:
    pass


class InventoryModule(BaseInventoryPlugin):
    """
    Inventory module for OVH services
    """

    NAME = "ansible714.ovh.ovh_services"
    OBJECTS_IN_INVENTORY = ("projects", "mk8s", "iploadbalancers", "instances")

    def __init__(self):
        super(InventoryModule, self).__init__()
        self.ovh_projectnames = []
        self.ovh_k8scache = {}
        self.inventory_content = {}
        self.ovh_client = None
        self.config = None

    def parse(self, inventory, loader, path, cache=True):
        """
        Run the inventory.

        For inventory to be successful, you need to provide OVH-Apis credentials
        """

        super(InventoryModule, self).parse(inventory, loader, path, cache)

        # Process parameters
        self.config = self._read_config_data(path)
        for option in self.OBJECTS_IN_INVENTORY:
            self.inventory_content[option] = (
                self.get_option(option) if option in self.config else True
            )

        if "only_projects" in self.config:
            self.inventory_content["projects"] = True

        # connect to OVH Api
        self._init_ovh_client()

        # Add data to inventory
        if self.inventory_content["iploadbalancers"]:
            self._add_loadbalancers()
        if self.inventory_content["projects"]:
            self._add_projects()
        if self.inventory_content["mk8s"]:
            self._add_mk8s()
        if self.inventory_content["instances"]:
            self._add_instances()

    def _init_ovh_client(self):
        ovh_api_endpoint = self._get_ovh_secret("ovh_api_endpoint")
        ovh_api_application_key = self._get_ovh_secret("ovh_api_application_key")
        ovh_api_application_secret = self._get_ovh_secret("ovh_api_application_secret")
        ovh_api_consumer_key = self._get_ovh_secret("ovh_api_consumer_key")

        try:
            self.ovh_client = ovh.Client(
                endpoint=ovh_api_endpoint,
                application_key=ovh_api_application_key,
                application_secret=ovh_api_application_secret,
                consumer_key=ovh_api_consumer_key,
            )
            self.display.vvvv("Connected to OVH-APi: ok")
        except APIError as error:
            raise AnsibleParserError("Failed to connect to OVH-Api: {0}".format(error))
        except Exception as error:
            raise AnsibleParserError("Failed to init OVH-Api: {0}".format(error))

    def _get_ovh_secret(self, secret_name):
        ovh_secret = (
            self.get_option(secret_name)
            if secret_name in self.config
            else os.environ.get(secret_name.upper())
        )

        if not ovh_secret:
            raise AnsibleParserError(
                "Invalid OVH Dynamic inventory configuration"
                " missing '{0}' parameter.".format(secret_name)
            )

        return ovh_secret

    def verify_file(self, path):
        """
        Return true/false if this is possibly a valid file for this plugin to consume
        """
        valid = False
        if super(InventoryModule, self).verify_file(path):
            self.display.vvvv(
                "Validating OVH Dynamic inventory configuration file: {0}".format(path)
            )
            for filename in ("ovh_services.yaml", "ovh_services.yml", "ovh.yaml", "ovh.yml"):
                if path.endswith(filename):
                    self.display.vvvv(
                        "OVH Dynamic inventory configuration file found: {0}".format(path)
                    )
                    valid = True
        return valid

    def _find_instance_ip(self, instancedata, kind, version):
        """
        Finds relevant Ip in instance data
        """
        for ip in instancedata["ipAddresses"]:
            if ip["type"] == kind and ip["version"] == version:
                return ip["ip"]
        return None

    def _add_instances(self):
        """
        Adds Data about Public cloud instances to inventory
        """
        self.display.vvvv("OVH Dynamic inventory, processing Public Cloud instances")
        # Process parameters
        pci_hostname = self.get_option("pci_hostname") if "pci_hostname" in self.config else "id"
        pci_ansible_host = (
            self.get_option("pci_ansible_host") if "pci_ansible_host" in self.config else "name"
        )

        # create OVHPublicCloudInstances group
        self.inventory.add_group("OVHPublicCloudInstances")
        self.inventory.add_group("OVHMk8sNodes")

        self._get_selected_projects()

        for project in self.ovh_projectnames:
            try:
                instances = self.ovh_client.get("/cloud/project/{0}/instance".format(project))
                for instancedata in instances:
                    self.inventory.add_host(instancedata[pci_hostname])
                    if self.inventory_content["projects"]:
                        self.inventory.add_child(
                            "Project_{0}".format(project), instancedata[pci_hostname]
                        )
                    # Instances variables/facts including projectId
                    self.inventory.set_variable(instancedata[pci_hostname], "projectId", project)
                    for varname in instancedata:
                        self.inventory.set_variable(
                            instancedata[pci_hostname], varname, instancedata[varname]
                        )

                    # setup ansible_host
                    if pci_ansible_host == "name":
                        host_value = instancedata["name"]
                    elif pci_ansible_host == "publicIPv6":
                        host_value = self._find_instance_ip(instancedata, "public", 6)
                    elif pci_ansible_host == "publicIPv4":
                        host_value = self._find_instance_ip(instancedata, "public", 4)
                    elif pci_ansible_host == "privateIPv4":
                        host_value = self._find_instance_ip(instancedata, "private", 4)

                    self.inventory.set_variable(
                        instancedata[pci_hostname], "ansible_host", host_value
                    )

                    # Identify managed K8S nodes
                    is_k8s_instance = False
                    for kubeid in self._get_mk8s_ids(project):
                        for kubeinstance in self._get_mk8s_nodes(project, kubeid):
                            if kubeinstance["instanceId"] == instancedata["id"]:
                                is_k8s_instance = True
                                break
                        if is_k8s_instance:
                            break

                    self.inventory.add_child(
                        "OVHMk8sNodes" if is_k8s_instance else "OVHPublicCloudInstances",
                        instancedata[pci_hostname],
                    )

            except APIError as error:
                raise AnsibleParserError(
                    "Failed to execute OVH-Api call to retrieve instance Data : {0}".format(error)
                )

    def _get_selected_projects(self):
        """
        Retrieve projects that must figure in inventory
        """
        if not self.ovh_projectnames:
            only_projects = (
                self.get_option("only_projects") if "only_projects" in self.config else None
            )

            if only_projects is None:
                try:
                    self.ovh_projectnames = self.ovh_client.get("/cloud/project")
                except APIError as error:
                    raise AnsibleParserError("Failed to execute OVH-Api call : {0}".format(error))
            else:
                self.ovh_projectnames = only_projects

    def _add_projects(self):
        """
        Adds Data about Cloud projects to inventory
        Project information is added as group variables
        """
        self.display.vvvv("OVH Dynamic inventory, processing Public Cloud projects")
        self.inventory.add_group("ovh_cloud_projects")

        self._get_selected_projects()

        # collect projects data
        for project in self.ovh_projectnames:
            try:
                group_name = "Project_{0}".format(project)
                self.inventory.add_group(group_name)
                self.inventory.add_child("ovh_cloud_projects", group_name)
                projectdata = self.ovh_client.get("/cloud/project/{0}".format(project))
                for varname in projectdata:
                    self.inventory.set_variable(group_name, varname, projectdata[varname])
            except APIError as error:
                raise AnsibleParserError(
                    "Failed to execute OVH-Api call to retrieve project data : {0}".format(error)
                )

    def _get_mk8s_nodes(self, project, kubeid):
        """
        returns the lis of nodes for given Managed Kubernetes
        """

        if not self.ovh_k8scache.setdefault(project, {}):
            self._get_mk8s_ids(project)
        if self.ovh_k8scache[project][kubeid].get("nodes") is None:
            try:
                self.ovh_k8scache[project][kubeid]["nodes"] = self.ovh_client.get(
                    "/cloud/project/{0}/kube/{1}/node".format(project, kubeid)
                )
            except APIError as error:
                raise AnsibleParserError(
                    "Failed to execute OVH-Api call to retrieve MK8S nodes  data : {0}".format(
                        error
                    )
                )
        return self.ovh_k8scache[project][kubeid]["nodes"]

    def _get_mk8s_ids(self, project):
        """
        returns the lis of nodes for given Managed Kubernetes
        """
        if not self.ovh_k8scache.setdefault(project, {}):
            try:
                kubeids = self.ovh_client.get("/cloud/project/{0}/kube".format(project))
                for kubeid in kubeids:
                    self.ovh_k8scache[project].setdefault(kubeid, {})
            except APIError as error:
                raise AnsibleParserError(
                    "Failed to execute OVH-Api call to retrieve MK8S ids : {0}".format(error)
                )
        return self.ovh_k8scache[project].keys()

    def _add_mk8s(self):
        """
        Adds Data Managed Kubernetes services
        """
        self.display.vvvv("OVH Dynamic inventory, processing Managed Kubernetes")
        self.inventory.add_group("ovh_cloud_projects")

        self._get_selected_projects()

        self.inventory.add_group("OVHManagedK8S")

        # collect Managed Kubernetes data
        for project in self.ovh_projectnames:
            try:
                self.inventory.add_group("OVHManagedK8S")
                for kubeid in self._get_mk8s_ids(project):
                    self.display.vvvv(
                        "OVH Dynamic inventory, processing Managed Kubernetes: {0}".format(kubeid)
                    )
                    self.inventory.add_host(kubeid)
                    self.inventory.add_child("OVHManagedK8S", kubeid)
                    self.inventory.set_variable(kubeid, "service_kind", "managedKubernetes")
                    kubedata = self.ovh_client.get(
                        "/cloud/project/{0}/kube/{1}".format(project, kubeid)
                    )
                    self.inventory.set_variable(kubeid, "projectId", project)
                    for varname in kubedata:
                        self.inventory.set_variable(kubeid, varname, kubedata[varname])
                    kubeinstances = self._get_mk8s_nodes(project, kubeid)
                    self.inventory.set_variable(kubeid, "nodes", kubeinstances)

            except APIError as error:
                raise AnsibleParserError(
                    "Failed to execute OVH-Api call to retrieve Managed K8S data : {0}".format(
                        error
                    )
                )

    def _add_loadbalancers(self):
        """
        Adds Data about IP load-balancers to inventory
        """
        _variable_names = (
            "displayName",
            "ipLoadbalancing",
            "ipv4",
            "ipv6",
            "offer",
            "sslConfiguration",
            "state",
            "vrackEligibility",
            "vrackName",
            "zone",
        )

        # Looking for IP-Load-balancers
        self.inventory.add_group("OVHIPLoadBalancers")
        try:
            loadBalancers = self.ovh_client.get("/ipLoadbalancing")
        except APIError as error:
            raise AnsibleParserError("Failed to execute OVH-Api call : {0}".format(error))

        for loadBalancer in loadBalancers:
            try:
                loadBalancerData = self.ovh_client.get("/ipLoadbalancing/{0}".format(loadBalancer))
                self.inventory.add_host(loadBalancer)
                self.inventory.add_child("OVHIPLoadBalancers", loadBalancer)
                self.inventory.set_variable(loadBalancer, "service_kind", "ipLoadbalancing")
                self.inventory.set_variable(
                    loadBalancer, "ansible_host", loadBalancerData["serviceName"]
                )

                # Copy variables
                for varname in _variable_names:
                    self.inventory.set_variable(loadBalancer, varname, loadBalancerData[varname])

                # Frontends Data
                self.display.vvvv("OVH Dynamic inventory, processing IPLB frontends data")
                frontends = get_frontends_info(self.ovh_client, loadBalancer)
                self.inventory.set_variable(loadBalancer, "frontends", frontends)

                # Farms Data
                self.display.vvvv("OVH Dynamic inventory, processing IPLB farms data")
                farms = get_farms_info(self.ovh_client, loadBalancer)
                self.inventory.set_variable(loadBalancer, "farms", farms)

                # VrackNetworks Data
                self.display.vvvv("OVH Dynamic inventory, processing Vrack Networks data")
                networks = get_networks_info(self.ovh_client, loadBalancer)
                self.inventory.set_variable(loadBalancer, "networks", networks)

            except APIError as error:
                raise AnsibleParserError(
                    "Failed to execute OVH-Api call when updating LB data: {0}".format(error)
                )
            except Exception as error:
                raise AnsibleParserError("Failed to init update LB Data: {0}".format(error))
