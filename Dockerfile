ARG PYTHON_VERSION
ARG DEBIAN_VERSION
FROM python:${PYTHON_VERSION}-${DEBIAN_VERSION}
LABEL maintainer="C. Guychard<christophe@article714.org>"

# Used Ansible Version
ARG ANSIBLE_DEP

# Build container

COPY requirements.txt /tmp
COPY requirements_dev.txt /tmp
COPY requirements_docs.txt /tmp
COPY galaxy_requirements.yml /tmp

RUN python -m pip install --no-cache-dir --upgrade pip && \
python -m pip install --no-cache-dir -r /tmp/requirements.txt  && \
python -m pip install --no-cache-dir -r /tmp/requirements_dev.txt  && \
python -m pip install --no-cache-dir -r /tmp/requirements_docs.txt  && \
ansible-galaxy collection install -r /tmp/galaxy_requirements.yml
