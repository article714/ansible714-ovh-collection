# Release notes

## Version 0.0.13

- Provided a way to not gather facts for iploadblancer

## Version 0.0.12

- Fixed issue #25 about conflicting refresh tasks on load-balancer
- Updated iploadbalancer_route documentation (issue #20)
- Fixed issue #45, dns_reverse module raising python exception
- Improved documentation generation (issue #40)
- Fixed issue #41, inconsistent properties on ip-loadbalancer farms between facts & modules

## Version 0.0.11

- Optimized Dockerfile for toolings & tests
- Moved to branch main of build-tools (waiting for it to be versioned)
- Fixes issue with updating existing DNS records (issue #43)
- Documentation improvements

## Version 0.0.10

- Added module to setup DNS reverse records
- Added a Mnaged K8S and Public Cloud instances to dynamic inventory plugin for OVH-cloud services
- Added a inventory plugin for OVH-cloud services (iploadbalancing in priority) (issue #13)
- Added roles & modules to manage Public Cloud Instances (issue #34)
- Added roles & modules index and doc generation (issue #1)
- Added modules to manage ssh keys attached to profile or project (issue #33)
- Added support for pre-commit hooks (issue #30)
- Added a modules allowing to retrieve information on the OVH private networks and related subnets.
- Added a module to manage an Managed Db instances at OVH: create, update and delete (issue #28).

## Version 0.0.9

- Fixed direct link to documentation
- Added some unit-tests + fixed pytest dependency for ansible-test
- Fixed tooling for collection package upload to Gitlab

## Version 0.0.8

- SSL certs only updated if serial-number or fingerprint changes
- Fixed issue #24 on free cert creation
- Update build toolchain to use a docker image and latest `ansible-core` version
- Updated toolchain to newer python version (3.10) and debian bullseye
- Removed black support for python 2.7 (dev tools)
- Upgraded OVH module to 1.0.0
- Fixed issue #23 on renewal of certificates (fqdn now mandatory)

## Version 0.0.7

- Fixed issue #18 on vracknetwork update
- Fixed issue #22 about certificate chains not being uploaded to Load-Balancer
- Documentation for collection (plugins) is extracted and published on ansible714 website (issue #10)
- Fixed: it should be possible to ignore port option on a server definition
- Fixed issue with application of configuration on Load-Balancer (issue #17)

## Version 0.0.6

- Fix issue with ssl certificates => no key provided when using pem_file, update needs deletion + re-creation

## Version 0.0.5

- added support for DNS zone management (dns module, issue #14)
- Fixed issue with ssl_certificate module always failing when cert does not exist

## Version 0.0.4

- Role now takes into account the SSL Certificates management
- Certificate FQDN is now mandatory for Free Certificates to address issue #8, and enable searching for free certificates
- Better tests and validation process, preparing for unit-testing of collection
- Fixed warnings when importing collection to Galaxy

## Version 0.0.3

- Provide some some sensible defaults for `ip_load_balancer` role
- Fixed various issues with galaxy docs
- Fixed major issue in iploadbalancer_facts module

## Version 0.0.2

- Fixed many documentation issues preventing publishing to Galaxy
- Added modules and roles to manage Public Cloud Instances
- Published collection file (.tar.gz) only when built from tag

## Version 0.0.1

- First version of the tooling including mainly modules & roles to manage OVH IP Load-Balancer
