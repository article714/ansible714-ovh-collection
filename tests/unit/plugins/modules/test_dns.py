# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2022

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

from __future__ import absolute_import, division, print_function

__metaclass__ = type


import json

from ansible.module_utils import basic
from ansible.module_utils.common.text.converters import to_bytes
from ansible_collections.ansible714.ovh.plugins.modules import dns

from ....unit.tools import moduletest


def set_module_args(args):
    """
    Setup default arguments for unit tests
    """
    args = json.dumps(
        {
            "ANSIBLE_MODULE_ARGS": {
                "ovh_api_endpoint": "ovh-eu",
                "ovh_api_application_key": "some_data",
                "ovh_api_application_secret": "some_data",
                "ovh_api_consumer_key": "some_data",
                "zone": "some-dns.io",
                "record_value": "some-value",
                "record_name": "some-entry",
            }
        }
    )
    basic._ANSIBLE_ARGS = to_bytes(args)


class TestDNSModule(moduletest.AnsibleModuleTestCase):
    """Test module in check mode"""

    def test_check_mode(self):
        """Test module in check mode"""
        set_module_args({})
        # Creates module
        module = dns.setup_module()
        # Forces check mode
        module.check_mode = True
        # Runs module
        with self.assertRaises(moduletest.ModuleSucceeded) as result:
            dns.run_module(module)
        self.assertTrue(result.exception.args[0]["changed"])
