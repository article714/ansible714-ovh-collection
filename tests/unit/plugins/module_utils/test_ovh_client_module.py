# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2022

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

from __future__ import absolute_import, division, print_function

__metaclass__ = type

import json

from ansible.module_utils import basic
from ansible.module_utils.common.text.converters import to_bytes
from ansible_collections.ansible714.ovh.plugins.module_utils.ovh_client_module import (
    OvhClientModule,
)
from ....unit.tools import moduletest


def set_module_args(args):
    """prepare arguments so that they will be picked up during module creation"""
    args = json.dumps(
        {
            "ANSIBLE_MODULE_ARGS": {
                "ovh_api_endpoint": "ovh-eu",
                "ovh_api_application_key": "some_data",
                "ovh_api_application_secret": "some_data",
                "ovh_api_consumer_key": "some_data",
            }
        }
    )
    basic._ANSIBLE_ARGS = to_bytes(args)


class TestOvhClientModule(moduletest.AnsibleModuleTestCase):
    """Test module in check mode"""

    def test_init(self):
        set_module_args({})

        test_module = OvhClientModule({}, required_by={})

        self.assertIsNotNone(test_module)

        self.assertFalse(test_module.check_mode)
