# -*- coding: utf-8 -*-

# Copyright: ©Tekfor - 2022

# GNU Lesser General Public License v3.0+ (https://www.gnu.org/licenses/lgpl-3.0.txt)

from unittest import TestCase
from unittest.mock import patch

from ansible.module_utils.basic import AnsibleModule


class ModuleSucceeded(Exception):
    pass


class ModuleFailed(Exception):
    pass


def exit_json(*args, **kwargs):
    """exit_json function replacement for unit testing"""
    kwargs["changed"] = True if "changed" not in kwargs else kwargs["changed"]
    kwargs["failed"] = False if "failed" not in kwargs else kwargs["failed"]
    raise ModuleSucceeded(kwargs)


def fail_json(*args, **kwargs):
    """fail_json function replacement for unit testing"""
    kwargs["failed"] = True if "failed" not in kwargs else kwargs["failed"]
    raise ModuleFailed(kwargs)


class AnsibleModuleTestCase(TestCase):
    """Common base class for moodules tests"""

    def setUp(self):
        """patches module to prevent unittests fails"""
        self.patchedModule = patch.multiple(
            AnsibleModule,
            exit_json=exit_json,
            fail_json=fail_json,
        )
        self.patchedModule.start()
        self.addCleanup(self.patchedModule.stop)
