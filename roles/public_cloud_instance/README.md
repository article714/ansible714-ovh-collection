# public_cloud_instance

Manage OVH Public Cloud Instances

## Dependencies

None.

## License

LGPL-3.0-or-later

## Author

C. Guychard <christophe@article714.org>

## Role variables

### machine_type

Default value:

```YAML
machine_type: ovh-pci
```

### netplan_config_version

Default value:

```YAML
netplan_config_version: 2
```

### netplan_io

Default value:

```YAML
netplan_io: true
```

### netplan_packages

Default value:

```YAML
netplan_packages:
  - netplan.io
```

### netplan_reset_configuration

Default value:

```YAML
netplan_reset_configuration: true
```
