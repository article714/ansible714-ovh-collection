# managed_db

Manages an OVH-Cloud [Managed Database](https://www.ovhcloud.com/en/public-cloud/databases/) instance.

## Dependencies

None.

## License

LGPL-3.0-or-later

## Author

Mathieu Piriou (@PiMath22) <mathieu.piriou@tekfor.cloud>

## Role variables

### db_state

Default value:

```YAML
db_state: present
```

### ip_restrictions

List of IPs allowed to access Managed Db instance.

Default value:

```YAML
ip_restrictions: {}
```

### pci_project_name

The name of the Public Cloud project to which Managed Db belongs.

Default value:

```YAML
pci_project_name:
```
