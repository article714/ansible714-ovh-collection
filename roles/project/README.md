# project

A role to manage OVH-Cloud Projects and related data

## Dependencies

None.

## License

LGPL-3.0-or-later

## Author

C. Guychard (Tekfor) <christophe@tekfor.cloud>
