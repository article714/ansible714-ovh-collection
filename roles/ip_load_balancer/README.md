# ip_load_balancer

This role makes use of the ansible714.ovh collection modules to manage an
[IP Load-Balancer](https://www.ovhcloud.com/en/network/load-balancer/) (IPLB) from OVH-Cloud.

It does not yet manage Load-balancer&#39;s lifecycle, but only the configuration of an existing IPLB

## Dependencies

None.

## License

LGPL-3.0-or-later

## Author

C. Guychard (Tekfor) <christophe.guychard@tekfor.cloud>

## Role variables

### gather_iplb_facts

Only gather facts on load-balancer when true, else role will only use variables set in inventory.

Default value:

```YAML
gather_iplb_facts: true
```

### iplb_vrack

The name of the Vrack to which the IPLB will be connected.

### networks

The list of (sub)networks that the IPLB will use for outgoing traffic (to servers/farms).

Must contain a list of dictionaries defining network(s) properties: natIp, subnet, name, state
