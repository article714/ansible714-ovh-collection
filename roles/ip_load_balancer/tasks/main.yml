---
#
# @var iplb_vrack:description: The name of the Vrack to which the IPLB will be connected.
#
# @var networks:description: >
# The list of (sub)networks that the IPLB will use for outgoing traffic (to servers/farms).
#
# Must contain a list of dictionaries defining network(s) properties: natIp, subnet, name, state
# @end
#
#
# @todo improvement: Manage IP-Loadbalancer lifecyle (create/delete)

- name: " Gather facts on OVH Load-Balancer "
  ansible714.ovh.iploadbalancer_facts:
    name: "{{ ansible_host }}"
    ovh_api_endpoint: "{{ ovh_api_endpoint }}"
    ovh_api_application_key: "{{ ovh_api_application_key }}"
    ovh_api_application_secret: "{{ ovh_api_application_secret }}"
    ovh_api_consumer_key: "{{ ovh_api_consumer_key }}"
  when: gather_iplb_facts
  delegate_to: localhost
  become: false

- name: Attach Load-Balancer to VRack
  ansible714.ovh.iploadbalancer_set_vrack: # noqa: var-naming[no-role-prefix]
    name: "{{ ansible_host }}"
    vrack: "{{ iplb_vrack }}"
    skip_validation: true
    ovh_api_endpoint: "{{ ovh_api_endpoint }}"
    ovh_api_application_key: "{{ ovh_api_application_key }}"
    ovh_api_application_secret: "{{ ovh_api_application_secret }}"
    ovh_api_consumer_key: "{{ ovh_api_consumer_key }}"
  when: iplb_vrack | default(false) and vrackEligibility
  delegate_to: localhost
  become: false
  register: attachment_task
  notify: Apply IP-LB Configuration

- name: Wait for Load-Balancer to be attached to VRack
  ansible714.ovh.vrack_wait_for_task: # noqa no-handler
    name: "{{ iplb_vrack }}"
    task_id: "{{ attachment_task.id }}"
    ovh_api_endpoint: "{{ ovh_api_endpoint }}"
    ovh_api_application_key: "{{ ovh_api_application_key }}"
    ovh_api_application_secret: "{{ ovh_api_application_secret }}"
    ovh_api_consumer_key: "{{ ovh_api_consumer_key }}"
  when: iplb_vrack | default(false) and vrackEligibility and attachment_task.changed
  delegate_to: localhost
  become: false
  notify: Apply IP-LB Configuration

- name: Manage networks for vrack
  ansible714.ovh.iploadbalancer_vracknetwork: # noqa: var-naming[no-role-prefix]
    lb_name: "{{ ansible_host }}"
    natIp: "{{ item.natIp }}"
    subnet: "{{ item.subnet }}"
    vlan: "{{ item.vlan | default(0) }}"
    name: "{{ item.name }}"
    state: "{{ item.state }}"
    ovh_api_endpoint: "{{ ovh_api_endpoint }}"
    ovh_api_application_key: "{{ ovh_api_application_key }}"
    ovh_api_application_secret: "{{ ovh_api_application_secret }}"
    ovh_api_consumer_key: "{{ ovh_api_consumer_key }}"
  with_items: "{{ networks | default([]) }}"
  when: iplb_vrack | default(false) and vrackEligibility
  delegate_to: localhost
  become: false
  register: vracknetworks
  notify: Apply IP-LB Configuration

- name: Manage Servers farms
  ansible714.ovh.iploadbalancer_farm:
    lb_name: "{{ ansible_host }}"
    balancing_algorithm: "{{ item.balancing_algorithm | default(omit) }}"
    farm_id: "{{ item.farm_id | default(omit) }}"
    farm_type: "{{ item.farm_type }}"
    name: "{{ item.name }}"
    port: "{{ item.port | default(omit) }}"
    probe: "{{ item.probe | default(omit) }}"
    state: '{{ item.state | default("present") }}'
    stickiness: "{{ item.stickiness | default(omit) }}"
    vrackNetworkId: "{{ item.vrackNetworkId | default(omit) }}"
    zone: "{{ item.zone }}"
    ovh_api_endpoint: "{{ ovh_api_endpoint }}"
    ovh_api_application_key: "{{ ovh_api_application_key }}"
    ovh_api_application_secret: "{{ ovh_api_application_secret }}"
    ovh_api_consumer_key: "{{ ovh_api_consumer_key }}"
  with_items: "{{ farms | default([]) }}"
  delegate_to: localhost
  become: false
  notify: Apply IP-LB Configuration

- name: Manage Servers in farms
  ansible714.ovh.iploadbalancer_server:
    lb_name: "{{ ansible_host }}"
    farm_id: "{{ item.farm_id }}"
    farm_type: "{{ item.farm_type }}"
    address: "{{ item.address }}"
    backup: "{{ item.backup | default(false) }}"
    chain: "{{ item.chain | default(omit) }}"
    cookie: "{{ item.cookie | default(omit) }}"
    name: "{{ item.name }}"
    port: "{{ item.port | default(omit) }}"
    probe: "{{ item.probe | default(omit) }}"
    proxyProtocolVersion: "{{ item.proxyProtocolVersion | default(omit) }}"
    ssl: "{{ item.ssl | default(omit) }}"
    state: "{{ item.state | default(omit) }}"
    status: "{{ item.status | default(omit) }}"
    weight: "{{ item.weight | default(omit) }}"
    ovh_api_endpoint: "{{ ovh_api_endpoint }}"
    ovh_api_application_key: "{{ ovh_api_application_key }}"
    ovh_api_application_secret: "{{ ovh_api_application_secret }}"
    ovh_api_consumer_key: "{{ ovh_api_consumer_key }}"
  with_items: "{{ servers | default([]) }}"
  delegate_to: localhost
  become: false
  notify: Apply IP-LB Configuration

- name: Manage Frontends
  ansible714.ovh.iploadbalancer_frontend: # noqa: var-naming[no-role-prefix]
    lb_name: "{{ ansible_host }}"
    allowed_source: "{{ item.allowed_source | default(omit) }}"
    dedicated_ip_fo: "{{ item.dedicated_ip_fo | default(omit) }}"
    default_farm_id: "{{ item.default_farm_id | default(omit) }}"
    default_ssl_id: "{{ item.default_ssl_id | default(omit) }}"
    denied_source: "{{ item.denied_source | default(omit) }}"
    disabled: "{{ item.disabled | default(false) }}"
    frontend_type: "{{ item.frontend_type }}"
    name: "{{ item.name }}"
    hsts: "{{ item.hsts | default(omit) }}"
    http_header: "{{ item.http_header | default(omit) }}"
    port: "{{ item.port }}"
    redirect_location: "{{ item.redirect_location | default(omit) }}"
    ssl: "{{ item.ssl | default(omit) }}"
    state: "{{ item.state }}"
    zone: "{{ item.zone | default(omit) }}"
    ovh_api_endpoint: "{{ ovh_api_endpoint }}"
    ovh_api_application_key: "{{ ovh_api_application_key }}"
    ovh_api_application_secret: "{{ ovh_api_application_secret }}"
    ovh_api_consumer_key: "{{ ovh_api_consumer_key }}"
  with_items: "{{ frontends | default([]) }}"
  delegate_to: localhost
  become: false
  register: iplbfrontends
  notify: Apply IP-LB Configuration

- name: Manage Routes
  ansible714.ovh.iploadbalancer_route:
    lb_name: "{{ ansible_host }}"
    frontend_id: "{{ item.frontend_id }}"
    frontend_type: "{{ item.frontend_type }}"
    name: "{{ item.name }}"
    action_status: "{{ item.action_status | default(omit) }}"
    action_target: "{{ item.action_target | default(omit) }}"
    action_type: "{{ item.action_type | default(omit) }}"
    rules: "{{ item.rules | default([]) }}"
    state: "{{ item.state | default(omit) }}"
    weight: "{{ item.weight | default(omit) }}"
    ovh_api_endpoint: "{{ ovh_api_endpoint }}"
    ovh_api_application_key: "{{ ovh_api_application_key }}"
    ovh_api_application_secret: "{{ ovh_api_application_secret }}"
    ovh_api_consumer_key: "{{ ovh_api_consumer_key }}"
  with_items: "{{ routes | default([]) }}"
  delegate_to: localhost
  become: false
  notify: Apply IP-LB Configuration

- name: Manage SSL Certificates
  ansible714.ovh.iploadbalancer_ssl_certificate:
    lb_name: "{{ ansible_host }}"
    name: "{{ item.name }}"
    fqdn: "{{ item.fqdn | default(omit) }}"
    free_certificate: "{{ item.free_certificate | default(omit) }}"
    certificate: "{{ item.certificate | default(omit) }}"
    chain: "{{ item.chain | default(omit) }}"
    key: "{{ item.key | default(omit) }}"
    pem_file: "{{ item.pem_file | default(omit) }}"
    state: "{{ item.state | default(omit) }}"
    ovh_api_endpoint: "{{ ovh_api_endpoint }}"
    ovh_api_application_key: "{{ ovh_api_application_key }}"
    ovh_api_application_secret: "{{ ovh_api_application_secret }}"
    ovh_api_consumer_key: "{{ ovh_api_consumer_key }}"
  with_items: "{{ ssl_certificates | default([]) }}"
  delegate_to: localhost
  become: false
  notify: Apply IP-LB Configuration
