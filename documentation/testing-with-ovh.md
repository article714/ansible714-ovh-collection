# Testing collection in an OVH Environment

## Project setup

Define the following Gitlab CI/CD variables:

- `VAULT_BASE_PATH`: path for the kv-v2 engine where secrets are stored
- `VAULT_SERVER_URL`,  `VAULT_AUTH_PATH`, `VAULT_AUTH_ROLE`: see [Gitlab documentation](https://docs.gitlab.com/ee/ci/secrets/#configure-your-vault-server)

If `SKIP_OVH_TEST` is set, OVH-specific tests are not executed.

## Vault setup & needed credentials

[official documentation](https://docs.gitlab.com/ee/ci/secrets/) about using external secrets in Gitlab CI/CD pipelines.
NOTICE: engine used to store secrets must be kv v2

Credentials and configuration information needed to run tests are all stored in the Vault.

Secrets that must be available (i.e. a `vault kv get <secret path>` gives requestet information):

- `${VAULT_BASE_PATH}/ovh/api_endpoint`,
- `${VAULT_BASE_PATH}/ovh/application_key`
- `${VAULT_BASE_PATH}/ovh/application_secret`
- `${VAULT_BASE_PATH}/ovh/consumer_key`
- `${VAULT_BASE_PATH}/ovh/clouds_yaml`, a [clouds.yaml file](https://docs.openstack.org/python-openstackclient/pike/configuration/index.html#configuration-files)
    used to host the credentials for the test environment.
- `${VAULT_BASE_PATH}/ovh/inventory_testing`, a yaml file including Ansible variable definitions for TESTING group, used
   to build inventory. This inventory file muste define at least:
        - `pci_project_name`, name of the OVH-Cloud project to work with;
        - `pci_public_net_dns_zone`, the zone where will be created the DNS records during tests.
        - `default_openstack_cloud`, the name of the cloud in `clouds.yaml` to use for the tests.
        - `db_instance_name`, name of the Opensearch instance
        - `new_db_instance_name`, new name of the Opensearch instance
        - `db_state`, 'present' to create or update an instance, 'absent' to delete an instance
        - `db_flavor`, instance flavor
        - `db_plan`, OVH offer for the opensearch instance
        - `db_version`, opensearch version
        - `db_region`, opensearch instance region
        - `db_engine`, the name of the database engine
        - `pci_private_network_name`, the name of the private network
        - `pci_private_subnet_cidr`, the cidr of the OVH subnet
        - `pci_private_network_region`, private network region
        - `ip_restrictions`, description and ip authorized to connect to the database

IMPORTANT: for all these secrets, Value of the secret must be stored in the `value` field.
E.g.: `vault kv put ${VAULT_BASE_PATH}/ovh/api_endpoint value=ovh-eu`
