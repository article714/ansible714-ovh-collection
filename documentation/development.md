# Contribute to this collection

## How to setup a development environment

Following is a sample list of commands that you could use to initiate a development environment using Ansible 6.0.0:

```bash
# create a new virtualenv
mkvirtualenv ansible_600

# Install dependencies
export ANSIBLE_VERSION=6.0.0
export ANSIBLE_DEP="ansible==${ANSIBLE_VERSION}"
pip3 install --upgrade pip
# required deps to run collection
pip3 install -r requirements.txt
# recommended deps to contribute to collection
pip3 install -r requirements_dev.txt
# addition deps if you want to work on/generate documentation
pip3 install -r requirements_docs.txt

# clone repository
git clone git@gitlab.com:article714/ansible714-ovh-collection.git

# install pre-commit hooks
cd ansible714-ovh-collection
pre-commit install
```

## Manually run tests or checks

If you want to run manually the tests that will be run by ci pipeline, you can use the following commands:

```bash
# run pre-commit checks (code formating, ansible-linting)
pre-commit run

# run collection sanity tests
./tests/scripts/run_ansible_test.sh sanity

# run collection unit tests
./tests/scripts/run_ansible_test.sh unit
```

## Pre-commit hooks

This project uses [pre-commit](https://pre-commit.com/) to help maintain code standards. So before commiting code to this
repository, it is **strongly recommended** to setup `pre-commit` tooling in your development environment:

```bash
git clone git@gitlab.com:article714/ansible714-ovh-collection.git
cd ansible714-ovh-collection
pre-commit install
```

Known dependencies to run pre-commit hooks:

- Python and [pre-commit](https://pre-commit.com/)
- a Ruby interpreter, for [mardownlint](https://github.com/markdownlint/markdownlint)

## Code formatting

We use `black` (see [documentation](https://black.readthedocs.io/)) to format python code. A `pyproject.toml` file is
provided for configuration and so you can use it on your code in the following way:

```bash
black -c pyproject.toml plugins/module_utils/ovh_client_module.py
```

Using the same config file, code formatting can be verified using `flake8`:

```bash
flake8 --config pyproject.toml
```

## CI/CD

This project uses [Gitlab CI](https://docs.gitlab.com/ee/ci/) to build and test collection.

In order to run Ansible tests with a higher level of verbosity, you can define an environment variable with additional
ansible parameters: `ANSIBLE_VERBOSITY_OPTION="-v -v -v"`. It can be defined at project (default for all pipelines) or pipeline level.

Also some specific configuration is needed if you want to [run tests against actual OVH APIS](testing-with-ovh.md).
