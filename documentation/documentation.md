# Documenting the collection

The documentation for the collection is made of several parts:

- `README.md`, contains generic information about the collection;
- `CHANGELOG.md`, contains list of evolutions/contributions;
- `AUTHORS.md`contains list of individual contributors;
- `README.md` file;
- `documentation/` directory contains:
      - additional content on [how to contribute](development.md) or to [test](testing-with-ovh.md) the collection
      - generated documentation for roles and modules using [ansible-doc](https://docs.ansible.com/ansible/latest/cli/ansible-doc.html)
        and [ansible-doctor](https://ansible-doctor.geekdocs.de/)

**TODO**: we need to explain how collection documentation is integrated to Ansible714 website
