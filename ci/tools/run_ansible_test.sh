#!/bin/bash -eu

set -e -o pipefail

#************************************
# utility script to run ansible-test in a temporary directory.
#
# Usage:
#     run_ansible_test.sh sanity|unit
#
# depending on first parameter value will execute sanity checks or unit tests
#
# Default values can be overriden using environment variables:
#    *   workdir: directory where ansible_collections dir tree will be created
#
#************************************


# Prepare environment variables
workdir=${workdir:="/tmp"}
srcdir=$(pwd)

collections_dir="${workdir}/ansible_collections/ansible714"
ovh_collection_dir="${collections_dir}/ovh"

python_version=$(python3 --version | cut -d ' ' -f 2 | cut -d '.' -f 1,2)

# Create temp environment
mkdir -p "${collections_dir}"

if [ -d "${ovh_collection_dir}" ]; then
    rsync -avz --delete "${srcdir}/" "${ovh_collection_dir}"
else
    cp -a "${srcdir}" "${ovh_collection_dir}"
fi

# Runs test
cd "${ovh_collection_dir}"
ansible-test "$1" --python "$python_version" --requirements

# Clean up
cd -