#!/bin/bash -eu

# set -x

script_dir=$(dirname "$(realpath """$0""")")
source_dir=$(realpath "${script_dir}"/../..)
doc_dir="${source_dir}/documentation"
templates_dir="${doc_dir}/templates"
roles_dir="${source_dir}/roles"

collection_name=$(basename "$(pwd)")
collection_namespace=$(grep ^namespace: galaxy.yml | cut -d " " -f 2)
collection_shortname=$(grep ^name: galaxy.yml | cut -d " " -f 2)
collection_prefix="${collection_namespace}.${collection_shortname}"

modules=$(ansible-doc -t module --list | grep "${collection_prefix}" | cut -d ' ' -f 1);
# shellcheck disable=SC2086
modules_list=$(printf '"%s",' $modules | sed s/,\$//);
roles_list=$(find "${roles_dir}" -mindepth 1 -maxdepth 1 -type d -exec basename {} \;)
# shellcheck disable=SC2086
roles_list=$(printf '"%s",' $roles_list | sed s/,\$//);

echo "{\"collection_name\":\"$collection_name\", \"modules\":[$modules_list], \"roles\":[$roles_list]}" > "${doc_dir}/collection_data.json"
jinja -d "${doc_dir}/collection_data.json" "${templates_dir}/modules_index.md.j2"  > "${doc_dir}/modules/index.md"
jinja -d "${doc_dir}/collection_data.json" "${templates_dir}/roles_index.md.j2"  > "${doc_dir}/roles/index.md"

for module in ${modules}; do
    ansible-doc --type module --json "${module}" | sed -e "s/\"$module\":/\"data\":/" >  "${doc_dir}/modules/module_${module}.json"
    jinja -d  "${doc_dir}/modules//module_${module}.json" "${templates_dir}/module.md.j2" > "${doc_dir}/modules/module_${module}.md"
done;
