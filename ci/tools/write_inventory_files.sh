#!/bin/bash -eu

# writes TESTING.yml on disk

if [ -f "$1" ]; then
  cat "$1" >> tests/inventory/group_vars/TESTING.yaml
else
  echo "ERROR: $1 is not a file!";
fi