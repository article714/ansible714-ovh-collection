#!/bin/bash -eu

# set -x

script_dir=$(dirname "$(realpath """$0""")")
source_dir=$(realpath "${script_dir}/../..")
roles_dir="${source_dir}/roles"
doc_dir="${source_dir}/documentation"
base_output_dir="${doc_dir}/roles"
templates_dir="${doc_dir}/templates"

doc_file_name='README.md'


export ANSIBLE_DOCTOR_TEMPLATE_DIR=${templates_dir}
export ANSIBLE_DOCTOR_TEMPLATE="a714roles"


if [ ${#@} -gt 0 ]; then
  # generate doc for a single role
  roles_dir_list="${roles_dir}/${1}"
else
  # generate doc for all roles defined
  roles_dir_list=$(find "${roles_dir}" -mindepth 1 -maxdepth 1 -type d)
fi


for role_dir in ${roles_dir_list}; do
  role_name=$(basename "${role_dir}")
  export ANSIBLE_DOCTOR_OUTPUT_DIR=${role_dir}
  export ANSIBLE_DOCTOR_ROLE_NAME=${role_name}
  ansible-doctor -f -n  "${role_dir}"
  if [ -f "${role_dir}/${doc_file_name}" ]; then
    cp "${role_dir}/${doc_file_name}" "${base_output_dir}/${role_name}.md"
  fi
  echo "Generated doc for ${role_name}"
done